//
//  HomeSubView.swift
//  Yaanba
//
//  Created by Abhay on 07/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class HomeSubView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var collectionViewUpperProduct: UICollectionView!
    @IBOutlet weak var heightConstantUpperProduct: NSLayoutConstraint!
    @IBOutlet weak var collectionViewOffer: UICollectionView!
    @IBOutlet weak var heightConstantOffer: NSLayoutConstraint!
    @IBOutlet weak var tblProduct: UITableView!
    @IBOutlet weak var heightConstantProductTbl: NSLayoutConstraint!
    
    //MARK: - SetupUI
    
    func setupUI(delegate:HomeSubVc){
        
        collectionViewUpperProduct.register(UINib(nibName: "HomeProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeProductCollectionCell")
        
        collectionViewOffer.register(UINib(nibName: "OfferCollectioncell", bundle: nil), forCellWithReuseIdentifier: "OfferCollectioncell")
        
        tblProduct.register(UINib(nibName: "ProductTblCell", bundle: nil), forCellReuseIdentifier: "ProductTblCell")
        
    }
    
}
