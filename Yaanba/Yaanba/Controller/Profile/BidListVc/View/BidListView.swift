//
//  BidListView.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class BidListView: UIView {

    //MARK: - Outlet

    @IBOutlet weak var productImg: CustomImageView!
    @IBOutlet weak var tblBidList: UITableView!
    @IBOutlet weak var lblProductName: UILabel!    
    @IBOutlet weak var lblProductDetails: UILabel!
    //MARK: - setupUI
    
    func setupUI(){
        
        tblBidList.register(UINib(nibName: "BidListTblCell", bundle: nil), forCellReuseIdentifier: "BidListTblCell")
        
        lblProductName.textColor = UIColor.appThemeDarkBlueColor
        lblProductName.font = themeFont(size: 15, fontname: .semibold)
        
    }
    
}
