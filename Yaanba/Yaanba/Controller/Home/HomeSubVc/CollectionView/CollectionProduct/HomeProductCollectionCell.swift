//
//  HomeProductCollectionCell.swift
//  Yaanba
//
//  Created by Abhay on 07/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class HomeProductCollectionCell: UICollectionViewCell {

    //MARK: - Outlet
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProduct: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgProduct.layer.cornerRadius = 8
        imgProduct.layer.masksToBounds = true
        
        lblProduct.textColor = UIColor.appThemeDarkBlueColor
        lblProduct.font = themeFont(size: 14, fontname: .semibold)
        
        // Initialization code
    }

}
