//
//  ProfileViewModel.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
class ProfileViewModel{
    
    fileprivate weak var theController:ProfileVc!
    
    init(theController:ProfileVc) {
        self.theController = theController
    }
    
    var profileData:Logindata?
    var productListdata = [ProductListdata]()
}
//MARK: Api setup
extension ProfileViewModel {
    
    func profileApi(param:[String:Any],success:@escaping(_ token : LoginModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aUserProfile
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LoginModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
    func logoutApi(param:[String:Any],success:@escaping(_ token : LogoutModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aLogout
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LogoutModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    func myProductListApi(param:[String:Any],success:@escaping(_ token : ProductListModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aMyProducts
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakeGetAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let productListData = ProductListModel(JSON: data) {
                             success(productListData)
                             return
                         }
                     }
                     return
                     
                 }
                failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}

