//
//  HeaderView.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class HeaderViewForSingleSegment: UIView {

    //MARK: - Outlet
    @IBOutlet weak var vwTitle:UIView!
    @IBOutlet weak var lblTItle: UILabel!
    @IBOutlet weak var vwWriteReview:UIView!
    @IBOutlet weak var btnWriteReview: CustomButton!
    @IBOutlet weak var vwAddComment:UIView!
    @IBOutlet weak var lblAddComment: UILabel!
    @IBOutlet weak var btnAddComment: UIButton!
    @IBOutlet weak var imgArrow: UIImageView!
    
    override func awakeFromNib() {
        
        lblTItle.font = themeFont(size: 14, fontname: .regular)
        lblTItle.textColor = UIColor.white
        lblTItle.text = "Review_key".localized
        vwTitle.backgroundColor = UIColor.appThemeDarkBlueColor
        
        btnWriteReview.setupThemeButtonUI()
        btnWriteReview.setTitle("WRITE_REVIEW_key".localized, for: .normal)
        
        lblAddComment.text = "Add_Comment_key".localized
        lblAddComment.font = themeFont(size: 14, fontname: .regular)
        lblAddComment.textColor = UIColor.white
        
        imgArrow.image = imgArrow.image?.imageFlippedForRightToLeftLayoutDirection().withRenderingMode(.alwaysTemplate)
        imgArrow.tintColor = UIColor.white
        
    }
    
}
