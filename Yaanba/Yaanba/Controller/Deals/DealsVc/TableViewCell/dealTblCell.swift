//
//  dealTblCell.swift
//  Yaanba
//
//  Created by Abhay on 17/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import MZTimerLabel

class dealTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var imgPageCount: UIPageControl!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOriginalPrice: UILabel!
    @IBOutlet weak var lblOfferedPrice: UILabel!
    @IBOutlet weak var lblMobileNumber: MZTimerLabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var collectionUpperDeals: UICollectionView!
    @IBOutlet weak var constantTblDelasHeight: NSLayoutConstraint!
    @IBOutlet weak var headerPageControl: UIPageControl!
    
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblProductName.textColor = UIColor.appThemeDarkBlueColor
        lblProductName.font = themeFont(size: 17, fontname: .bold)
        
        lblDescription.textColor = UIColor.appThemeGrayColor
        lblDescription.font = themeFont(size: 13, fontname: .regular)
        
        lblOriginalPrice.textColor = UIColor.appThemeGrayColor
        lblOriginalPrice.font = themeFont(size: 13, fontname: .regular)
        
        lblOfferedPrice.textColor = UIColor.appThemeDarkBlueColor
        lblOfferedPrice.font = themeFont(size: 14, fontname: .bold)
        
        lblMobileNumber.textColor = UIColor.appThemeGrayColor
        lblMobileNumber.font = themeFont(size: 14, fontname: .regular)
        
        headerPageControl.currentPageIndicatorTintColor = UIColor.appThemeDarkBlueColor
        //        headerPageControl.tintColor = UIColor.blue
        
        if DeviceLanguage == "ar"{
            headerPageControl.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        collectionUpperDeals.register(UINib(nibName: "AddImagesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddImagesCollectionCell")
        
        // Initialization code
        //lblMobileNumber.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
extension dealTblCell: MZTimerLabelDelegate{
    
    func timerLabel(_ timerLabel: MZTimerLabel?, customTextToDisplayAtTime time: TimeInterval) -> String? {
        
        if timerLabel == lblMobileNumber {
            let second = Int(time) % 60
            let minute = (Int(time) / 60) % 60
            let hours = Int(time / 3600)
            return String(format: "%02dh %02dm %02ds", hours, minute, second)
        } else {
            return nil
        }
    }
}
