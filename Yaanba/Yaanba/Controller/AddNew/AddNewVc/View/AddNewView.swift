//
//  AddNewView.swift
//  Yaanba
//
//  Created by Abhay on 08/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class AddNewView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var lblWhatKindOfAddAreyoulisting: UILabel!
    @IBOutlet weak var txtSelectProductCategory: CustomTextField!
    @IBOutlet weak var txtChooseType: CustomTextField!
    @IBOutlet weak var vwSelectSubCategory: CustomView!
    @IBOutlet weak var txtSelectSubCategory: CustomTextField!
    @IBOutlet weak var vwItemTitle: CustomView!
    @IBOutlet weak var txtItemTitle: CustomTextField!
    @IBOutlet weak var lblDescribeYourItem: UILabel!
    @IBOutlet weak var txtvwDescribeYourItem: UITextView!
    @IBOutlet weak var lblUploadItemsPicture: UILabel!
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var txtChooseSellType: CustomTextField!
    @IBOutlet weak var txtItemPrice: CustomTextField!
    @IBOutlet weak var txtAlternativeMobileNumber: CustomTextField!
    @IBOutlet weak var vwBidding: UIView!
    @IBOutlet weak var txtBasicPrice: CustomTextField!
    @IBOutlet weak var txtStepPrice: CustomTextField!
    @IBOutlet weak var txtStartDate: CustomTextField!
    @IBOutlet weak var txtStartTime: CustomTextField!
    @IBOutlet weak var txtEndDate: CustomTextField!
    @IBOutlet weak var txtEndTime: CustomTextField!
    @IBOutlet weak var btnPostNow: CustomButton!
    @IBOutlet weak var vwProductDetail: UIView!
    @IBOutlet weak var vwItemPrice: CustomView!
    @IBOutlet weak var lblCountryCode: UITextField!
    @IBOutlet weak var imgCountryCode: CustomImageView!
    @IBOutlet weak var imgArrow: UIImageView!
    
    @IBOutlet weak var txtSubOfSubCategory: CustomTextField!
    
    
    func setupUI(theController:AddNewVc){
        
        vwBidding.isHidden = true
        vwProductDetail.isHidden = true
       // vwItemPrice.isHidden = true
        lblWhatKindOfAddAreyoulisting.font = themeFont(size: 13, fontname: .bold)
        lblWhatKindOfAddAreyoulisting.textColor = UIColor.appThemeDarkBlueColor
        lblWhatKindOfAddAreyoulisting.text = "What_kind_of_add_are_you_listing_key".localized
        
        lblUploadItemsPicture.font = themeFont(size: 13, fontname: .bold)
        lblUploadItemsPicture.textColor = UIColor.appThemeDarkBlueColor
        lblUploadItemsPicture.text = "Upload_item's_picture_key".localized
        [txtSelectProductCategory,txtChooseType,txtSelectSubCategory,txtItemTitle,txtChooseSellType,txtItemPrice,txtAlternativeMobileNumber,txtBasicPrice,txtStepPrice,txtStartDate,txtStartTime,txtEndDate,txtEndTime,txtSubOfSubCategory].forEach { (txt) in
            txt?.placeHolderColor = UIColor.white
            txt?.font = themeFont(size: 13, fontname: .regular)
            txt?.delegate = theController
            txt?.tintColor = UIColor.white
            txt?.textColor = .white
            txt?.text = ""
        }
        
        lblDescribeYourItem.textColor = UIColor.white
        lblDescribeYourItem.font = themeFont(size: 13, fontname: .regular)
        lblDescribeYourItem.text = "Describe_your_item_key".localized
        
        btnPostNow.setupThemeButtonUI()
        btnPostNow.setTitle("POST_NOW_key".localized, for: .normal)
        
        txtSelectProductCategory.placeholder = "Choose_your_product's_category_key".localized
        txtChooseType.placeholder = "Choose_type_key".localized
        txtSelectSubCategory.placeholder = "Select_sub_category_key".localized
        txtItemTitle.placeholder = "Your_item_title_key".localized
        txtAlternativeMobileNumber.placeholder = "Alternative_mobile_number_key".localized
        txtChooseSellType.placeholder = "Choose_sell_type_key".localized
        txtItemPrice.placeholder = "Your_item_price_key".localized
        txtBasicPrice.placeholder = "Basic_price_key".localized
        txtStepPrice.placeholder = "Step_price_key".localized
        txtStartDate.placeholder = "Start_date_key".localized
        txtStartTime.placeholder = "Start_time_key".localized
        txtEndDate.placeholder = "End_date_key".localized
        txtEndTime.placeholder = "End_time_key".localized
        
        txtSubOfSubCategory.placeholder = "Select Sub-subcategory".localized

        txtvwDescribeYourItem.font = themeFont(size: 13, fontname: .regular)
        txtvwDescribeYourItem.delegate = theController
        
        collectionImages.register(UINib(nibName: "AddImagesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddImagesCollectionCell")
        if #available(iOS 13.0, *) {
            imgArrow.image = imgArrow.image?.withTintColor(UIColor.white)
        }
        
    }
    
}
