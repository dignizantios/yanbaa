//
//  DiscountDetailsView.swift
//  Yaanba
//
//  Created by Abhay on 05/03/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class DiscountDetailsView: UIView {

    //MARK:- Outlets
    
    @IBOutlet weak var discountCollectionView: UICollectionView!
    @IBOutlet weak var dtView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var companyNameView: UIView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnCall: CustomButton!
    @IBOutlet weak var btnEmail: CustomButton!
    
    //MARK: - SetupUI
    
    func setupUI(){
        lblDate.textColor = UIColor.appThemeGrayColor
        lblDate.font = themeFont(size: 13, fontname: .regular)
        
        lblCompanyName.textColor = UIColor.black
        lblCompanyName.font = themeFont(size: 16, fontname: .regular)
        
        lblDiscount.textColor = UIColor.black
        lblDiscount.font = themeFont(size: 10,  fontname: .regular)
        
        lblDescription.textColor = UIColor.black
        lblDescription.font = themeFont(size: 14, fontname: .regular)

        btnCall.setupThemeButtonUI()
        btnEmail.setupThemeButtonUI()
        btnCall.titleLabel?.font = themeFont(size: 12, fontname: .bold)
        btnEmail.titleLabel?.font = themeFont(size: 12, fontname: .bold)
        
        discountCollectionView.register(UINib(nibName: "AddImagesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddImagesCollectionCell")
        
        btnCall.setImage(UIImage(named: "ic_telephone")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        btnEmail.setImage(UIImage(named: "ic_email")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        
        if DeviceLanguage == "ar"{
            btnCall.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -50)
            btnEmail.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -50)
        }else{
            btnCall.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 50)
            btnEmail.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 50)
        }    }
}
