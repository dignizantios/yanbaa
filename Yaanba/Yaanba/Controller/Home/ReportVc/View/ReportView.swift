//
//  ReportView.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ReportView: UIView {

    @IBOutlet weak var lblWhyYouLikeReport: UILabel!
    @IBOutlet weak var lblReportDescription: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnSend: CustomButton!
    
    
    func setupUI(thedelegate:ReportVc){
        
        lblWhyYouLikeReport.textColor = UIColor.appThemeDarkBlueColor
        lblWhyYouLikeReport.font = themeFont(size: 18, fontname: .bold)
        print(thedelegate.mainModelView.productId)
        lblWhyYouLikeReport.text = "Why_would_you_like_to_report_item_key".localized+" #\(thedelegate.mainModelView.productId)"
        lblReportDescription.textColor = UIColor.appThemeDarkBlueColor
        lblReportDescription.font = themeFont(size: 14, fontname: .bold)
        lblReportDescription.text = "Report_Description_key".localized
        
        txtDescription.textColor = UIColor.black
        txtDescription.font = themeFont(size: 16, fontname: .regular)
        
        btnSend.setupThemeButtonUI()
        btnSend.setTitle("SEND_key".localized, for: .normal)
    
    }
    
}
