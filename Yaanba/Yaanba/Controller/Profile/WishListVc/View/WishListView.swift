//
//  WishListView.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class WishListView: UIView {

    //MARK: - Outlet
    @IBOutlet weak var tblWishlist: UITableView!
    
    //MARK: - SetupUI

    func setupUI(){
        
        tblWishlist.register(UINib(nibName: "ProductTblCell", bundle: nil), forCellReuseIdentifier: "ProductTblCell")
        tblWishlist.tableFooterView = UIView()
        
    }
    
}
