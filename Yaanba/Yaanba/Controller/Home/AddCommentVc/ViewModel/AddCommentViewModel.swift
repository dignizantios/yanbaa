
//
//  AddCommentViewModel.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
class AddCommentViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:AddCommentVc!
    
    init(theController:AddCommentVc) {
        self.theController = theController
    }
    var productId = String()
    weak var ProductAdddelegate: ReloadAddProductDelegate?
}
//MARK: Api setup
extension AddCommentViewModel {
    func ValidateDetails() {
        
         if (self.theController.theCurrentView.txtComment.text?.trimmingCharacters(in: .whitespaces))!.isEmpty{
            makeToast(strMessage: "Please_Enter_Some_Comment_Key".localized)
        }
        else if (self.theController.theCurrentView.txtPrice.text?.trimmingCharacters(in: .whitespaces))!.isEmpty{
            makeToast(strMessage: "Please_Enter_price_Key".localized)
        }
        else {
            self.theController.addCommentApi()
        }
    }
    func addCommnetApi(param:[String:Any],success:@escaping(_ token : LogoutModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aAddComment
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let AddCommmentData = LogoutModel(JSON: data) {
                             success(AddCommmentData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}

