//
//  LoginVc.swift
//  Yaanba
//
//  Created by Abhay on 01/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
import FirebaseAuth

class LoginVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var constantTopLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblWelcomeBack: UILabel!
    @IBOutlet weak var lblSigninToYaanba: UILabel!
    @IBOutlet weak var lblEmailID: UILabel!
    @IBOutlet weak var txtEmailID: CustomTextField!
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: CustomTextField!
    
    @IBOutlet weak var lblCountryCode: UITextField!
    @IBOutlet weak var imgCountryCode: CustomImageView!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignIn: CustomButton!
    @IBOutlet weak var lblDonthaveAccount: UILabel!
    @IBOutlet weak var continueAsAGuestTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblContinueasaGuest: UILabel!
    
    //MARK: - Variable
       
       lazy var mainModelView: LoginViewModel = {
           return LoginViewModel(theController: self)
       }()
    var countryDialCode: String = "+61"
    var countryName: String = "Australia"
    var countryCode: String = "AU"
    
    //MARK: - Viewlife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode
    }
    
}

//MARK: - SetupUI
extension LoginVc{
    
    func setupUI(){
        
        if UIScreen.main.bounds.width > 320
        {
            self.constantTopLogoConstraint.constant = 50
            self.continueAsAGuestTopConstraint.constant = 60
        }
        
        lblWelcomeBack.text = "WELCOME_BACK_key".localized
        lblSigninToYaanba.text = "Sign_In_to_Continue_to_Yanbaa_key".localized
        
        lblEmailID.text = "MOBILE_NUMBER_key".localized
        lblPassword.text = "PASSWORD_key".localized
        
        lblEmail.text = "EMAIL_ID_key".localized
        btnForgotPassword.setTitle("Forgot_Password_?_key".localized, for: .normal)
        
        btnSignIn.setTitle("Login_key".localized, for: .normal)
        
        lblWelcomeBack.font = themeFont(size: 12, fontname: .regular)
        lblSigninToYaanba.font = themeFont(size: 16, fontname: .bold)
        
        [lblEmailID,lblPassword,lblEmail].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkBlueColor
            lbl?.font = themeFont(size: 11, fontname: .regular)
        }
        
        [txtEmailID,txtPassword,txtEmail].forEach { (txt) in
            txt?.font = themeFont(size: 13, fontname: .regular)
            txt?.textColor = UIColor.black
            txt?.delegate = self
        }
        lblCountryCode.font = themeFont(size: 13.0, fontname: .light)
        lblCountryCode.textColor = UIColor.black
        
        txtEmailID.placeholder = "MOBILE_NUMBER_key".localized
//        txtPassword.placeholder = "PASSWORD_key".localized
        
        txtEmail.placeholder = "Enter Email here".localized
        txtPassword.placeholder = "Enter password here".localized
        
        btnSignIn.setupThemeButtonUI()
        
        btnForgotPassword.setTitleColor(UIColor.black, for: .normal)
        btnForgotPassword.titleLabel?.font = themeFont(size: 15, fontname: .semibold)
   
        lblDonthaveAccount.attributedText = setUIForUnderline(normal:"Dont_have_an_account_key".localized,underLine:"Register_Now_key".localized)
        lblDonthaveAccount.textColor = UIColor.appThemeDarkBlueColor
       
        let registerTapGeture = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
        lblDonthaveAccount.addGestureRecognizer(registerTapGeture)

        
        lblContinueasaGuest.attributedText = setUIForUnderline(normal:"Continue_as_a_key".localized,underLine:"Guest_key".localized)
        lblContinueasaGuest.textColor = UIColor.appThemeDarkBlueColor
        let guestTapGeture = UITapGestureRecognizer(target: self, action: #selector(tapGuestLabel))
        lblContinueasaGuest.addGestureRecognizer(guestTapGeture)
        
    }
    
}


//MARK:- Other Method
extension LoginVc
{
    
    @objc func tapLabel(_ gesture: UITapGestureRecognizer)
    {
        
        let str1 = "Dont_have_an_account_key".localized
        let str2 = "Register_Now_key".localized
        let str = str1 + " " + str2
        
        let rangeRegister = (str as NSString).range(of: str2, options: .caseInsensitive)

        let checkRegisterTapped = gesture.didTapAttributedTextInLabel(lblDonthaveAccount,targetRange:rangeRegister)
        
        if(checkRegisterTapped)
        {
            print("Register")
           
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVc") as! RegisterVc
            self.navigationController?.pushViewController(obj, animated: true)
         
        }
        
    }
    
    @objc func tapGuestLabel(_ gesture: UITapGestureRecognizer)
        {
            
            let str1 = "Continue_as_a_key".localized
            let str2 = "Guest_key".localized
            let str = str1 + " " + str2
            
            let rangeGuest = (str as NSString).range(of: str2, options: .caseInsensitive)

            let checkGuestTapped = gesture.didTapAttributedTextInLabel(lblContinueasaGuest,targetRange:rangeGuest)
            
            if(checkGuestTapped)
            {
                print("Guest")
                Defaults.removeObject(forKey: remember_accesstoken_key)
                Defaults.synchronize()
                appDelegate.objCustomTabBar = TabbarController()
                self.navigationController?.pushViewController(appDelegate.objCustomTabBar, animated: false)
             
            }
            
        }
}

//MARK: -IBAction
extension LoginVc{
    
    @IBAction func btnSignInTapped(_ sender: UIButton) {
        self.mainModelView.ValidateDetails()
//        self.mainModelView.ValidateOTPSend()
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVc") as! ForgotPasswordVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnCountryTapped(_ sender: UIButton) {
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.delegate = self
        self.present(obj, animated:  false, completion: nil)
    }
    
}
extension LoginVc:CountryCodeDelegate{
    func CountryCodeDidFinish(data: JSON) {
        self.countryDialCode = data["dial_code"].string ?? ""
        self.countryName = data["name"].string ?? ""
        self.countryCode = data["code"].string ?? ""
        
        self.imgCountryCode.image = UIImage(named : countryCode)
         self.lblCountryCode.text = countryDialCode
    }
}
//MARK:- TestFeild Delegate
extension LoginVc : UITextFieldDelegate {
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
}
//MARK: ApiSetUp
extension LoginVc {
    
    ///---- LOgin
    func loginApi() {
        let parameters : [String : Any] =   [ APIKey.key_email:self.txtEmail.text!,
            APIKey.key_password:self.txtPassword.text!,
            APIKey.key_device_token:Defaults.object(forKey: "device_token") ?? "1234",
            APIKey.key_register_id:Defaults.object(forKey: "fcm_token") ?? "1234",
            APIKey.key_device_type:strDeviceType,
        ]
        print("parameters:", parameters)
        
        mainModelView.loginApi(param: parameters, success: { (loginData) in
            
            //Defaults.removeObject(forKey: remember_email_key)
           // Defaults.removeObject(forKey: remember_password_key)
            Defaults.removeObject(forKey: remember_accesstoken_key)
            Defaults.removeObject(forKey: remember_userid_key)
            Defaults.removeObject(forKey: token_expire_date_key)
            //Defaults.set(self.txtEmailID.text!, forKey: remember_email_key)
           // Defaults.set(self.txtPassword.text!, forKey: remember_password_key)
            Defaults.set(loginData.accessToken, forKey: remember_accesstoken_key)
            Defaults.set("\(loginData.logindata?.id ?? 0)", forKey: remember_userid_key)
            Defaults.set(Date(), forKey: token_expire_date_key)            
            Defaults.synchronize()
            appDelegate.loadHomeScreen()
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    ///---- Login //registerApi
    func checkMobileApi() {
        
        var contryCode = self.lblCountryCode.text!
        contryCode.remove(at: contryCode.startIndex)
        let parameters : [String : Any] =   [ APIKey.key_mobile_no:self.txtEmailID.text!,
                                               APIKey.key_country_code:contryCode
        ]
        print("parameters:", parameters)
        showLoader()
        mainModelView.checkMobileApi(param: parameters, success: { (checkMobileData) in
            if checkMobileData.flag == 1{
                self.veryfyOTP()
            }else{
                makeToast(strMessage: checkMobileData.msg ?? "")
            }
        }) { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        }
    }
    func veryfyOTP()
    {
        var contryCode = self.lblCountryCode.text!
        contryCode.remove(at: contryCode.startIndex)
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        let param : [String : Any] =   [APIKey.key_country_code:contryCode,
                                           APIKey.key_mobile_no:self.txtEmailID.text!,
                                           APIKey.key_device_token:Defaults.object(forKey: "device_token") ?? "1234",
                                           APIKey.key_register_id:Defaults.object(forKey: "fcm_token") ?? "1234",
                                           APIKey.key_device_type:strDeviceType,
        ]
        print(param)
        
        print("\(self.lblCountryCode.text!)\(self.txtEmailID.text!)")
        PhoneAuthProvider.provider().verifyPhoneNumber("\(self.lblCountryCode.text!)\(self.txtEmailID.text!)", uiDelegate: nil)
        { (verificationID, error) in
            hideLoader()
            if ((error) != nil)
            {
                // Verification code not sent.
                print(error?.localizedDescription ?? "")
//                makeToast(strMessage: "Something_went_wrong_key".localized)
                makeToast(strMessage: error?.localizedDescription ?? "")
            } else {
                // Successful. User gets verification code
                // Save verificationID in UserDefaults
                print("verificationId: ",verificationID)
                Defaults.set(verificationID, forKey: "firebase_verification")
                Defaults.synchronize()
                

                //And show the Screen to enter the Code.
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                obj.mainModelView.signupDataDict = JSON(param)
                obj.mainModelView.isComeFromLogin = true
                obj.mainModelView.enterPhoneNum = "\(self.lblCountryCode.text!)\(self.txtEmailID.text!)"
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
    }
    
    
}

