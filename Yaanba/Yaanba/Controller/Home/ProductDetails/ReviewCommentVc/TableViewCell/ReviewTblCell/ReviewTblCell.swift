//
//  ReviewTblCell.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ReviewTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCommentValue: UILabel!
    @IBOutlet weak var lblReviewRatting: UILabel!
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.layer.masksToBounds = true
        
        lblName.textColor = UIColor.appThemeDarkBlueColor
        lblName.font = themeFont(size: 14, fontname: .regular)
        
        lblDate.textColor = UIColor.appThemeGrayColor
        lblDate.font = themeFont(size: 9, fontname: .regular)
        
        lblCommentValue.textColor = UIColor.appThemeGrayColor
        lblCommentValue.font = themeFont(size: 13, fontname: .regular)
        
        lblReviewRatting.textColor = UIColor.appThemeDarkBlueColor
        lblReviewRatting.font = themeFont(size: 10, fontname: .regular)
        lblReviewRatting.addImageToLabel(imageName: "ic_favorite_black", strText: "(4.5)", aboveString: "")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
