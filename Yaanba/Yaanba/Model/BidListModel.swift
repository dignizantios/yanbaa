import Foundation 
import ObjectMapper 

class BidListModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var bidListdata: BidListdata?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		bidListdata <- map["data"]
        
	}
} 

class BidListdata: Mappable {

	
    var bids: [Bids]?
    var productMultiImage: [ProductMultiImage]?
    var description: String?
    
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		
        productMultiImage <- map["ProductMultiImage"]
        description <- map["description"]
        bids <- map["bids"]
	}
} 

class Bids: Mappable {
    var id: NSNumber?
    var productId: NSNumber?
    var bidUserId: NSNumber?
    var bid: NSNumber?
    var createdAt: String?
    var userData: UserData?

    required init?(map: Map){
        
    }

    func mapping(map: Map) {
        id <- map["id"]
        productId <- map["product_id"]
        bidUserId <- map["bid_user_id"]
        bid <- map["bid"]
        createdAt <- map["created_at"]
        userData <- map["user_data"]
    }
}
class UserData: Mappable { 

	var id: NSNumber? 
	var name: String? 
	var mobileNo: String? 
	var profileImage: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		name <- map["name"] 
		mobileNo <- map["mobile_no"] 
		profileImage <- map["profile_image"] 
	}
} 

