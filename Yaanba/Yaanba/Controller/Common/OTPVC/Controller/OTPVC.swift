//
//  OTPVC.swift
//  Yaanba
//
//  Created by Abhay on 02/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SwiftyJSON

class OTPVC: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblPhoneVerification: UILabel!
    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var lblEnterOTPVerification: UILabel!
    @IBOutlet weak var lblDidntYoureceiveAnyCode: UILabel!
    @IBOutlet weak var lblResendANewCode: UILabel!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    //MARK: - Variable
    var enteredOtp = String()
    //MARK: - Varaible
    lazy var mainModelView: OTPViewModel = {
        return OTPViewModel(theController: self)
    }()
    
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        transperentNavigationBar()
    }
    
}
//MARK: - SetupUI

extension OTPVC{
    
    func setupUI(){
        
        lblPhoneVerification.font = themeFont(size: 28, fontname: .bold)
        lblEnterOTPVerification.font = themeFont(size: 16, fontname: .regular)
        lblDidntYoureceiveAnyCode.font = themeFont(size: 17, fontname: .semibold)
        
        [lblEnterOTPVerification,lblDidntYoureceiveAnyCode,lblResendANewCode].forEach { (lbl) in
            lbl?.textColor = UIColor.black
        }
        
        btnSubmit.setupThemeButtonUI()
        btnSubmit.setTitle("Submit_key".localized, for: .normal)
        
        lblPhoneVerification.text = "Phone_Verification_key".localized
        lblEnterOTPVerification.text = "Enter_your_OTP_code_here_key".localized
        lblDidntYoureceiveAnyCode.text = "Didnt_you_received_any_code_key".localized
        
        lblResendANewCode.attributedText = setUIForUnderline(normal:"Resend_key".localized,underLine:"a_new_code_key".localized)
        
        let loginTapGeture = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
        lblResendANewCode.addGestureRecognizer(loginTapGeture)
        lblResendANewCode.textColor = .black
        lblResendANewCode.isUserInteractionEnabled = true
        OTPsetup()
    }
    //OTPSetup
    func OTPsetup()
    {
        otpView.otpFieldsCount = 6
        otpView.otpFieldSize = 40
        otpView.otpFieldSeparatorSpace = 8
        otpView.otpFieldDefaultBackgroundColor = UIColor.lightGray
        otpView.otpFieldEnteredBackgroundColor = UIColor.appThemeYellowColor
        otpView.otpFieldFont = themeFont(size: 20, fontname: .bold)
        otpView.otpFieldDisplayType = .circular
        otpView.otpFieldBorderWidth = 0
        otpView.shouldAllowIntermediateEditing = false
        otpView.delegate = self
        otpView.initalizeUI()
        
    }
    
}

//MARK: - IBAction method

extension OTPVC{
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        guard enteredOtp.count == 6 else {
            makeToast(strMessage: "Please enter valid otp".localized)
            return
        }
        let verificationID = UserDefaults.standard.value(forKey: "firebase_verification")
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID! as! String, verificationCode: enteredOtp)
        showLoader()
        Auth.auth().signIn(with: credential) { (_ user:AuthDataResult?, _ error: Error?) in
            
            if error != nil {
                // Error
                hideLoader()
                print(error?.localizedDescription ?? "")
                makeToast(strMessage: error?.localizedDescription ?? "")
                self.lblResendANewCode.textColor = .black
                self.lblResendANewCode.isUserInteractionEnabled = true
                hideLoader()
                return
            }
            else
            {
                print("Phone number: \(user?.user.phoneNumber ?? "")")
                print("userInfo: \(user?.user.providerData[0].displayName)")
                if user != nil {
                    
                    user?.user.getIDToken(completion: { (data, error) in
                        if error == nil {
                            let token : String = data ?? ""
                            Defaults.set(token, forKey: "idToken")
                            Defaults.synchronize()
                            idToken = Defaults.value(forKey: "idToken") as! String
                            
                            print("IdToken:", token)
                            if self.mainModelView.isComeFromLogin{
                                self.loginApi()
                            }else{
                                self.registerApi()
                            }
                            
                        }
                    })
                    
                    print("Verify Successfully")
                    
                    let firebaseAuth = Auth.auth()
                    do {
                        try firebaseAuth.signOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                }
                hideLoader()
            }
        }
    }
    
    @objc func tapLabel(_ gesture: UITapGestureRecognizer)
    {
        let str1 = "Resend_key".localized
        let str2 = "a_new_code_key".localized
        let str = str1 + " " + str2
        let rangeRegister = (str as NSString).range(of: str2, options: .caseInsensitive)
        let newcode = gesture.didTapAttributedTextInLabel(lblResendANewCode,targetRange:rangeRegister)
        
        if(newcode)
        {
            print("New code")
            showLoader()
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            let parameters : [String : Any] = self.mainModelView.signupDataDict.dictionaryObject!
            PhoneAuthProvider.provider().verifyPhoneNumber(mainModelView.enterPhoneNum, uiDelegate: nil)
            { (verificationID, error) in
                hideLoader()
                if ((error) != nil)
                {
                    // Verification code not sent.
                    print(error?.localizedDescription ?? "")
                    makeToast(strMessage: error?.localizedDescription ?? "")
                    self.lblResendANewCode.textColor = .black
                    self.lblResendANewCode.isUserInteractionEnabled = true
                } else {
                    // Successful. User gets verification code
                    // Save verificationID in UserDefaults
                    print("verificationId: ",verificationID)
                    
                    UserDefaults.standard.set(verificationID, forKey: "firebase_verification")
                    UserDefaults.standard.synchronize()
                    //And show the Screen to enter the Code.
                    makeToast(strMessage: "Code resend sucessfully on your mobile number".localized)
                    self.lblResendANewCode.textColor = .lightGray
                    self.lblResendANewCode.isUserInteractionEnabled = false
                }
            }
        }
    }
    
    ///---- Register
    func registerApi() {
        showLoader()
        var parameters : [String : Any] = self.mainModelView.signupDataDict.dictionaryObject!
        parameters[APIKey.key_verification_id] = idToken
        parameters["device_type"] = "1"
        parameters["device_token"] = Defaults.object(forKey: "device_token") ?? "1234"
        parameters["register_id"] = Defaults.object(forKey: "fcm_token") ?? "1234"
        print("parameters:", parameters)
        
        mainModelView.registerApi(param: parameters, image: self.mainModelView.dataArray, NameArray: self.mainModelView.nameArray, FileNameArray: self.mainModelView.fileNameArray, success: { (loginData) in
            hideLoader()
            makeToast(strMessage: loginData.msg ?? "")
            Defaults.removeObject(forKey: remember_accesstoken_key)
            Defaults.removeObject(forKey: remember_userid_key)
            Defaults.removeObject(forKey: token_expire_date_key)
            Defaults.set(loginData.accessToken, forKey: remember_accesstoken_key)
            Defaults.set("\(loginData.logindata?.id ?? 0)", forKey: remember_userid_key)
            Defaults.set(Date(), forKey: token_expire_date_key)
            Defaults.synchronize()
            appDelegate.loadHomeScreen()
            
        }, failed: { (error) in
            hideLoader()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
            hideLoader()
        })
    }
    ///---- Login
    func loginApi() {
        var parameters : [String : Any] =   self.mainModelView.signupDataDict.dictionaryObject!
        parameters[APIKey.key_verification_id] = idToken
        print("parameters:", parameters)
        showLoader()
        mainModelView.loginApi(param: parameters, success: { (loginData) in
            hideLoader()
            //  Defaults.removeObject(forKey: remember_email_key)
            // Defaults.removeObject(forKey: remember_password_key)
            Defaults.removeObject(forKey: remember_accesstoken_key)
            Defaults.removeObject(forKey: remember_userid_key)
            Defaults.removeObject(forKey: token_expire_date_key)
            // Defaults.set(self.txtEmailID.text!, forKey: remember_email_key)
            // Defaults.set(self.txtPassword.text!, forKey: remember_password_key)
            Defaults.set(loginData.accessToken, forKey: remember_accesstoken_key)
            Defaults.set("\(loginData.logindata?.id ?? 0)", forKey: remember_userid_key)
            Defaults.set(Date(), forKey: token_expire_date_key)
            Defaults.synchronize()
            appDelegate.loadHomeScreen()
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
            hideLoader()
        })
    }
}

//MARK: - OTP Delegate

extension OTPVC: VPMOTPViewDelegate {
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        return true
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        enteredOtp = otpString
        print("OTPString: \(otpString)")
        
    }
    
}
