//
//  PlusMinusBiddingPopupView.swift
//  Yaanba
//
//  Created by Abhay on 15/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class PlusMinusBiddingPopupView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var lblPlaceBid: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var btnPlaceNow: CustomButton!
    
    //MARK: - SetupUI
    func setupUI(thedelegate:PlusMinusBiddingPopupVc){
        
        lblPlaceBid.text = "Place_a_Bid_key".localized
        lblPlaceBid.font = themeFont(size: 14, fontname: .regular)
        lblPlaceBid.textColor = UIColor.appThemeDarkBlueColor
        
        lblValue.textColor = UIColor.appThemeDarkBlueColor
        lblValue.font = themeFont(size: 18, fontname: .bold)
        lblValue.text = String(thedelegate.mainModelView.BasicPrice)
        
        btnPlaceNow.setupThemeButtonUI()
        btnPlaceNow.setTitle("PLACE_NOW_key".localized, for: .normal)
        btnPlaceNow.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        
    }
    
}
