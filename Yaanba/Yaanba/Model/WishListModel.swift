import Foundation
import ObjectMapper

class WishListModel: Mappable {

    var flag: NSNumber?
    var msg: String?
   // var wishlistdata: [wishlistproductData]?
    var wishlistproductData: [WishlistproductData]?
    required init?(map: Map){
    }

    func mapping(map: Map) {
        flag <- map["flag"]
        msg <- map["msg"]
        //wishlistproductData <- map["data"]
        wishlistproductData <- map["data"]
    }
}

class Wishlistdata: Mappable {

    var id: NSNumber?
    var productId: NSNumber?
    var userId: NSNumber?
    var wishlistproductData: WishlistproductData?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        productId <- map["product_id"]
        userId <- map["user_id"]
        wishlistproductData <- map["data"]
    }
}

class WishlistproductData: Mappable {

    var id: NSNumber?
    var productTitle: String?
    var price: NSNumber?
    var priceBidBarg: String?
    var basicPrice: Any?
    var stepPrice: Any?
    var productImage: String?
    var productType: String?
    var mostPopular: NSNumber?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        productTitle <- map["product_title"]
        price <- map["price"]
        priceBidBarg <- map["price_bid_barg"]
        basicPrice <- map["basic_price"]
        stepPrice <- map["step_price"]
        productImage <- map["product_image"]
        mostPopular <- map["most_popular"]
        productType <- map["product_type"]
    }
}
