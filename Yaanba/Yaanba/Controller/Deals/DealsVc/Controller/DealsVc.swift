//
//  DealsVc.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage
import MZTimerLabel

protocol ReloadDealImgCountDelegate: class {
    func reloadDealImg()
}

class DealsVc: UIViewController, MZTimerLabelDelegate {

    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:DealsView = { [unowned self] in
        return self.view as! DealsView
    }()
    lazy var mainModelView: DealsViewModel = {
        return DealsViewModel(theController: self)
    }()
    var dealTblCel = dealTblCell()
    var currentPageNum = 0
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI()
        setupNavigationbarCenterText(titleText: "DEALS_key".localized)
        // Do any additional setup after loading the view.
        mainModelView.refreshControl.attributedTitle = NSAttributedString(string: "")
        mainModelView.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        mainModelView.refreshControl.attributedTitle = NSAttributedString(string: "")
        theCurrentView.tblDeals.addSubview(mainModelView.refreshControl)
        
        getDealsApi()
    }
    @objc func refresh(sender:AnyObject) {
        self.mainModelView.dealData?.removeAll()
        getDealsApi()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        theCurrentView.tblDeals.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        theCurrentView.tblDeals.removeObserver(self, forKeyPath: "contentSize")
    }

    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if object is UITableView {
            theCurrentView.constantTblDelasHeight.constant = theCurrentView.tblDeals.contentSize.height
        }
        
    }

}
extension DealsVc: ReloadDealImgCountDelegate{
    func reloadDealImg() {
        //let cell = self.theCurrentView.tblDeals.dequeueReusableCell(withIdentifier: "dealTblCell") as! dealTblCell
        dealTblCel.headerPageControl.currentPage = 1
    }
}
//MARK: - CollectionView Delegate

extension DealsVc:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mainModelView.dealImgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImagesCollectionCell", for: indexPath) as! AddImagesCollectionCell
        cell.btnDelete.isHidden = true
        if self.mainModelView.dealImgArray[indexPath.row] !=  "" {
            cell.imgSelected.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgSelected.sd_setImage(with: self.mainModelView.dealImgArray[indexPath.row].toURL(), placeholderImage: UIImage(named: "ic_upload_picture"), options: .lowPriority, context: nil)
        } else {
            cell.imgSelected.image = UIImage(named: "ic_upload_picture")
        }
        cell.DealImgCounterDelegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImagesCollectionCell", for: indexPath) as! AddImagesCollectionCell
       // cell.DealImgCounterDelegate?.reloadDealImg()
        //cell.headerPageControl.currentPage = Int((collectionView.contentOffset.x)/(collectionView.bounds.width))
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
         
        //reloadDealImg()
        //if scrollView == cell.collectionUpperDeals{
       /* let visibleCells = dealTblCel.collectionUpperDeals.visibleCells
        if let firstCell = visibleCells.first {
            if let indexPath = dealTblCel.collectionUpperDeals.indexPath(for: collectionCell as UICollectionViewCell) {
                // use indexPath to delete the cell
            }
        }
           */
        guard let indexPath = dealTblCel.collectionUpperDeals.indexPathsForVisibleItems.first else {
            return
        }
        print(indexPath.row)
          //  dealTblCel.headerPageControl.currentPage = Int((dealTblCel.collectionUpperDeals.contentOffset.x)/(dealTblCel.collectionUpperDeals.bounds.width))
        
        
        let pageWidth = scrollView.frame.width
        let currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        dealTblCel.headerPageControl.currentPage = currentPage
        print(dealTblCel.headerPageControl.currentPage)
       // }
    }
    /*
    func updateCellIMgCounter(indexPath: IndexPath) {


     tableView.beginUpdates()
     tableView.reloadRows(at: indexPathsForVisibleRows ?? [], with: .none)
     tableView.endUpdates()

    }*/
}

//MARK: - TableView

extension DealsVc:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mainModelView.dealData?.count == 0
        {
            let lbl = UILabel()
            lbl.text = "No_list_yet".localized
            lbl.numberOfLines = 5
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeGrayColor
            lbl.font = themeFont(size: 16.0, fontname: .regular)
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return self.mainModelView.dealData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        dealTblCel = tableView.dequeueReusableCell(withIdentifier: "dealTblCell") as! dealTblCell
        let dict  = self.mainModelView.dealData?[indexPath.row]
        //cell.imgProduct.image = UIImage(named: "sale_img.jpeg")
        dealTblCel.lblProductName.text = dict?.title ?? ""
        dealTblCel.tag = indexPath.row
        if let description = dict?.description
        {
            if let htmlData = description.data(using: .unicode) {
              do {
                dealTblCel.lblDescription.attributedText = try NSAttributedString(data: htmlData,
                                                             options: [.documentType: NSAttributedString.DocumentType.html],
                                                             documentAttributes: nil)
              } catch let e as NSError {
                print("Couldn't parse \(description): \(e.localizedDescription)")
              }
            }
        }
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(dict?.price ?? "") KWD")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        dealTblCel.lblOriginalPrice.attributedText = attributeString
        dealTblCel.lblOfferedPrice.text = "\(dict?.specialPrice ?? "") KWD"
        dealTblCel.lblMobileNumber.textColor = UIColor.appThemeYellowColor
        dealTblCel.lblMobileNumber.font = themeFont(size: 15, fontname: .bold)
        self.mainModelView.dealImgArray = dict?.images ?? [String]()
        dealTblCel.collectionUpperDeals.dataSource = self
        dealTblCel.collectionUpperDeals.delegate = self
        dealTblCel.collectionUpperDeals.reloadData()
        dealTblCel.headerPageControl.numberOfPages = dict?.images?.count ?? 0
        dealTblCel.headerPageControl.currentPage = currentPageNum
        dealTblCel.headerPageControl.isHidden = true
                
        let endDate = StringToDate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dict?.endTime ?? "")
        print(endDate)
        let timeInterval = endDate.timeIntervalSince(Date() as Date)
        print(timeInterval)
        dealTblCel.lblMobileNumber.setCountDownTo(endDate)//setCountDownTime(3600*24*240)
        dealTblCel.lblMobileNumber.delegate = self
        dealTblCel.lblMobileNumber.start()
        
        //setCountDownTime(fromDate: NSDate(), minutes: timeInterval)
        //setCountDownTime(minutes: TimeInterval(timeInterval))//setCountDownTime(minutes: timeInterval)
        /*dealTblCel.lblMobileNumber.animationType = .Evaporate
        dealTblCel.lblMobileNumber.setCountDownDate(fromDate: NSDate(), targetDate: endDate as NSDate)
        dealTblCel.lblMobileNumber.start()*/

        /*if cell.lblMobileNumber.text != ""{
         let array = cell.lblMobileNumber.text!.split(separator: ":")
         print("array-->",array.count)
         if array.count >= 2{
         cell.lblMobileNumber.text = "\(array[0])h:\(array[1])m:\(array[1])s"
         }
         print("lblMobileNumber-->",cell.lblMobileNumber.text)
         }*/
        
        return dealTblCel
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}


//MARK: - API calling

extension DealsVc {
    ///---- Get Deals
    
    func getDealsApi() {
        var parameters : [String : Any] =  [String : Any]()
        parameters[APIKey.key_verification_id] = idToken
        print("parameters:", parameters)
        
        mainModelView.getDealsApi(param: parameters, success: { (dealData) in
            hideLoader()
            if dealData.flag == 1
            {
                self.mainModelView.dealData = dealData.dealdata ?? [Dealdata]()
                self.theCurrentView.tblDeals.reloadData()
                return
            }
            makeToast(strMessage: dealData.msg ?? "")
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
//                makeToast(strMessage: error)
            }
        })
    }
}
