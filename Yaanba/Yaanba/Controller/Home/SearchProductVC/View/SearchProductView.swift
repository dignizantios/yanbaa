//
//  SearchProductView.swift
//  Yaanba
//
//  Created by Abhay on 29/02/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class SearchProductView: UIView {

    //MARK:- Outlets
    
    @IBOutlet weak var txtSearch: CustomTextField!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var tblSearch: UITableView!
    
    
}
