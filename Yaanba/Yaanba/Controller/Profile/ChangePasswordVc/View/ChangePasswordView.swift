//
//  ChangePasswordView.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ChangePasswordView: UIView {

    @IBOutlet weak var lblOldPassword: UILabel!
    @IBOutlet weak var txtOldPassword: CustomTextField!
    
    @IBOutlet weak var lblNewPassword: UILabel!
    @IBOutlet weak var txtNewPassword: CustomTextField!
    
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    
    @IBOutlet weak var btnSubmit: CustomButton!


    //MARK: - SerupUI
    
    func setupUI(){
        
        [lblOldPassword,lblNewPassword,lblConfirmPassword].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkBlueColor
            lbl?.font = themeFont(size: 11, fontname: .regular)
        }
        [txtOldPassword,txtNewPassword,txtConfirmPassword].forEach { (txt) in
            txt?.font = themeFont(size: 13, fontname: .regular)
            txt?.textColor = UIColor.black
        }
        txtOldPassword.placeholder = "Old_Password_key".localized
        txtNewPassword.placeholder = "New_Password_key".localized
        txtConfirmPassword.placeholder = "Confirm_Password_key".localized
        
        lblOldPassword.text = "Old_Password_key".localized
        lblNewPassword.text = "New_Password_key".localized
        lblConfirmPassword.text = "Confirm_Password_key".localized
        
        btnSubmit.setupThemeButtonUI()
        btnSubmit.setTitle("SUBMIT_key".localized, for: .normal)
        
    }
    
}
