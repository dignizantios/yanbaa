import Foundation 
import ObjectMapper 

class LogoutModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
	}
} 

