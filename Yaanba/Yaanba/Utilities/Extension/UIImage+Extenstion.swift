//
//  UIImage+Extenstion.swift
//  Yaanba
//
//  Created by Abhay on 13/02/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImage().jpegData(compressionQuality: quality.rawValue)
//        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
    
    func png(_ quality: JPEGQuality) -> Data? {
        return self.pngData()
    }
}
