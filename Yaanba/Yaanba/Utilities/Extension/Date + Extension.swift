//
//  Date + Extension.swift
//  Liber
//
//  Created by YASH on 22/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation

extension Date{
    
    func generateDatesArrayBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date) ->[Date]
    {
        var datesArray: [Date] =  [Date]()
        var startDate = startDate
        let calendar = Calendar.current
        
        let fmt = DateFormatter()
        fmt.dateFormat = OrinalDtFormater
        
        while startDate <= endDate {
            datesArray.append(startDate)
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
            
        }
        return datesArray
    }
    
    func DatesAvailableBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date,SelectedDate:Date) ->Bool
    {
        var startDate = startDate
        let calendar = Calendar.current
        let fmt = DateFormatter()
        fmt.dateFormat = OrinalDtFormater
        while startDate <= endDate
        {
            if(startDate == SelectedDate)
            {
                return true
            }
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
            
        }
        return false
    }
    
    func daysTo(_ date: Date) -> Int? {
        let calendar = Calendar.current

        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: date)

        //let components = calendar.dateComponents([.day], from: date1, to: date2)
        // return components.day  // This will return the number of day(s) between dates
        
        let components = calendar.dateComponents([.hour], from: date1, to: date2)
        return components.hour  // This will return the number of hour(s) between dates
    }
    
    
    
}


