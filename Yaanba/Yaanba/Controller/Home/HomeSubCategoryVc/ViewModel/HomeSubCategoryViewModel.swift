//
//  HomeSubCategoryViewModel.swift
//  Yaanba
//
//  Created by Abhay on 08/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class HomeSubCategoryViewModel {
    
    //MARK: - view life cycle
    fileprivate weak var theController:HomeSubCategoryVc!
    
    init(theController:HomeSubCategoryVc) {
        self.theController = theController
    }
    var subCategorys: [SubCategorydata] = []
    var selectedCategoryId = String()
    var selectedCategoryName = String()
    
}
//MARK: Api setup
extension HomeSubCategoryViewModel {
    
    func SubCategoryApi(param:[String:Any],success:@escaping(_ token : SubCategoryModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aSubCategory
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let SubCategoryData = SubCategoryModel(JSON: data) {
                             success(SubCategoryData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}

