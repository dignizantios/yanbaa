//
//  NotificationVc.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class NotificationVc: UIViewController {

    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:NotificationView = { [unowned self] in
        return self.view as! NotificationView
    }()
    lazy var mainModelView: NotificationViewModel = {
        return NotificationViewModel(theController: self)
    }()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI()
        // Do any additional setup after loading the view.
        if Defaults.object(forKey: remember_accesstoken_key) as? String != nil{
            mainModelView.refreshControl.attributedTitle = NSAttributedString(string: "")
            mainModelView.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
            mainModelView.refreshControl.attributedTitle = NSAttributedString(string: "")
            theCurrentView.tblNotification.addSubview(mainModelView.refreshControl)
        }
        self.mainModelView.isNewDataLoading = false
        self.theCurrentView.indicater.isHidden = true
        self.theCurrentView.vwIndicatorHeight.constant = 0
        
    }

    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarCenterText(titleText: "NOTIFICATION_key".localized)
        getNotificationCountApi()
    }
    @objc func refresh(sender:AnyObject) {
       // Code to refresh table view
        self.mainModelView.isNewDataLoading = false
        self.mainModelView.notificationListdata.removeAll()
        getNotificationListtApi()
        
    }
}

//MARK: - Table Delegate

extension NotificationVc:  UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mainModelView.notificationListdata.count == 0
        {
            let lbl = UILabel()
            lbl.text = "No_list_yet".localized
            lbl.numberOfLines = 5
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeGrayColor
            lbl.font = themeFont(size: 16.0, fontname: .regular)
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return self.mainModelView.notificationListdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTblCell") as!  NotificationTblCell
        if self.mainModelView.notificationListdata.count > 0
        {
            let dict = self.mainModelView.notificationListdata[indexPath.row]
            cell.setData(dic:dict)
            if (self.mainModelView.notificationdata?.total ?? 0 == self.mainModelView.notificationListdata.count)
            {
                mainModelView.isNewDataLoading = true
                //            allListDataAPI(offset: mainModelView.pageOffset)
            }
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let dict = self.mainModelView.notificationListdata[indexPath.row]
        if dict.notificationType == 1 && dict.userId != (Defaults.object(forKey: remember_userid_key) as! String){
            let obj = profileStoryboard.instantiateViewController(withIdentifier: "BidListVc") as! BidListVc
            obj.mainModelView.productId = "\(dict.productId ?? 0)"
            obj.mainModelView.productProfile = dict.productImage ?? ""
            obj.mainModelView.productName = dict.productTitle ?? ""
          //  obj.mainModelView.productDetails = dict. ?? ""
            self.navigationController?.pushViewController(obj, animated: true)
            
        }else{
            if dict.priceBidBarg == nil && dict.productType?.lowercased() == "Service".localized.lowercased(){
                let obj = homeStoryboard.instantiateViewController(withIdentifier: "ServiceDetailVc") as! ServiceDetailVc
                obj.mainModelView.productId = "\(dict.productId ?? 0)"
                self.navigationController?.pushViewController(obj, animated: true)
            }else{
                let obj = homeStoryboard.instantiateViewController(withIdentifier: "MainProductDetailsVc") as! MainProductDetailsVc
                obj.mainModelView.productId = "\(dict.productId ?? 0)"
                if dict.priceBidBarg?.lowercased() == "bid".lowercased(){
                    obj.selectedController = .singleSegment
                }else if dict.priceBidBarg?.lowercased() == "price".lowercased(){
                    obj.selectedController = .singleSegment
                }else if dict.priceBidBarg?.lowercased() == "bargeding".lowercased(){
                    obj.selectedController = .multipleSegment
                }else{
                   obj.selectedController = .singleSegment
                }
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
        
    }
    //MARK: ScrollView Delegate
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == theCurrentView.tblNotification {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height-80)
            {
                if !(mainModelView.isNewDataLoading) {
                    mainModelView.isNewDataLoading = true
                    theCurrentView.vwIndicatorHeight.constant = 40
                    theCurrentView.indicater.isHidden = false
                    getNotificationListtApi()
                }
            }
        }
    }
    
}
//MARK: ApiSetUp
extension NotificationVc {
    
    ///---- Get Notification list
    func getNotificationListtApi() {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            withoutLoginPopup()
            return
        }
        let parameters = [String : Any]()
        print("parameters:", parameters)
        var url = String()
        if self.mainModelView.isNewDataLoading{
            guard self.mainModelView.notificationdata?.nextPageUrl != "" else {
                return
            }
            url = self.mainModelView.notificationdata?.nextPageUrl ?? ""
        }else{
            url = aBaseURL+aNotificationList
        }
        
        mainModelView.notificationListApi(urls: url, param: parameters, success: { (notificationListData) in
            self.mainModelView.refreshControl.endRefreshing()
            if self.mainModelView.notificationListdata.count == 0{
                self.mainModelView.notificationListdata = notificationListData.notificationListdata?.notificationdata ??  [Notificationdata]()
            }else{
                self.mainModelView.notificationListdata += notificationListData.notificationListdata?.notificationdata ??  [Notificationdata]()
            }
            if self.mainModelView.notificationListdata.count > 0{
                self.mainModelView.notificationdata = notificationListData.notificationListdata!
            }
            self.theCurrentView.tblNotification.reloadData()
            self.mainModelView.isNewDataLoading = false
            self.theCurrentView.indicater.isHidden = true
            self.theCurrentView.vwIndicatorHeight.constant = 0
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
               // makeToast(strMessage: error)
            }
            self.theCurrentView.indicater.isHidden = true
            self.theCurrentView.vwIndicatorHeight.constant = 0
             self.theCurrentView.tblNotification.reloadData()
        })
    }
    ///---- Get Notification Count
       func getNotificationCountApi() {
           let parameters = [String : Any]()
           print("parameters:", parameters)
           var url = String()
           if self.mainModelView.isNewDataLoading{
               guard self.mainModelView.notificationdata?.nextPageUrl != "" else {
                   return
               }
               url = self.mainModelView.notificationdata?.nextPageUrl ?? ""
           }else{
               url = aBaseURL+aNotificationList
           }
           
           mainModelView.notificationCountApi(urls: url, param: parameters, success: { (notificationCountData) in
            if notificationCountData.flag == 1{
                let badgeCnt:Int = Int(truncating: notificationCountData.logindata?.count ?? 0)
                Defaults.removeObject(forKey: "badgeCnt")
                Defaults.set(badgeCnt, forKey: "badgeCnt")
                UIApplication.shared.applicationIconBadgeNumber = badgeCnt
                if badgeCnt > 0{
                    self.tabBarController?.tabBar.items![3].badgeValue = "\(badgeCnt)"
                }else{
                    self.tabBarController?.tabBar.items![3].badgeValue = nil
                }
                self.getNotificationListtApi()
            }else{
                UIApplication.shared.applicationIconBadgeNumber = 0
                self.tabBarController?.tabBar.items![3].badgeValue = nil
            }
           }, failed: { (error) in
               //Utility.shared.stopActivityIndicator()
               if error == "token_expired"{
                   self.logoutApiCall()
               }else{
                   //makeToast(strMessage: error)
               }
               self.getNotificationListtApi()
           })
       }
}

