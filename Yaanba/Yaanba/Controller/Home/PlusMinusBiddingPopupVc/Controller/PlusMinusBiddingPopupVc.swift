//
//  PlusMinusBiddingPopupVc.swift
//  Yaanba
//
//  Created by Abhay on 15/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class PlusMinusBiddingPopupVc: UIViewController {

    //MARK: - Variable
    
    
    lazy var theCurrentView:PlusMinusBiddingPopupView = { [unowned self] in
        return self.view as! PlusMinusBiddingPopupView
    }()
    lazy var mainModelView: PlusMinusBiddingPopupViewModel = {
        return PlusMinusBiddingPopupViewModel(theController: self)
    }()
  //  var viewModel = PlusMinusBiddingPopupViewModel()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI(thedelegate:self)
        mainModelView.fixPrice = mainModelView.BasicPrice
        // Do any additional setup after loading the view.
        
    }
    
    
}

//MARK: - IBAction

extension PlusMinusBiddingPopupVc{
    
    @IBAction func btnMinusTapped(_ sender: UIButton) {
        theCurrentView.lblValue.text = String(mainModelView.decrementStep(stepValue: mainModelView.stepPrice))
    }
    
    @IBAction func btnPlusTapped(_ sender: UIButton) {
        theCurrentView.lblValue.text = String(mainModelView.incrementStep(stepValue: mainModelView.stepPrice))
    }
    
    @IBAction func btnPlaceNowTapped(_ sender: UIButton) {
        self.mainModelView.ValidateDetails()
    }
    
    
    @IBAction func btnClosePopupTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
//MARK: ApiSetUp
extension PlusMinusBiddingPopupVc {
    
    ///---- Add comment
    func addBidApi() {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else{
            withoutLoginPopup()
            return
        }
        let parameters : [String : Any] =   [
           APIKey.key_product_id:self.mainModelView.productId,
           APIKey.key_bid:self.theCurrentView.lblValue.text!
        ]
        print("parameters:", parameters)
        
        mainModelView.addBidApi(param: parameters, success: { (addCoomentData) in
            
            if addCoomentData.flag == 1{
                self.mainModelView.BidAddedelegate?.reloadAddProduct()
                self.dismiss(animated: true, completion: nil)
            }
           // makeToast(strMessage: addCoomentData.msg ?? "")
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    
    
}

