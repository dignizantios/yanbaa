//
//  SearchProductVC.swift
//  Yaanba
//
//  Created by Abhay on 29/02/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class SearchProductVC: UIViewController {

    //MARK: - Variable
    
    lazy var mainModelView: SearchProductViewModel = {
        return SearchProductViewModel(theController: self)
    }()
    
    lazy var theCurrentView:SearchProductView = { [unowned self] in
        return self.view as! SearchProductView
    }()
    
    //MARK:- Viewlife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "Search Product".localized)
    }
    

}
//MARK: - SetupUI
extension SearchProductVC{
    
    func setupUI(){
        [theCurrentView.txtSearch].forEach { (txt) in
            txt?.font = themeFont(size: 13, fontname: .regular)
            txt?.textColor = UIColor.black
            txt?.delegate = self
            txt?.clearButtonMode = .whileEditing
            txt?.placeholder = "Search_here_key".localized
            
        }
        theCurrentView.tblSearch.register(UINib(nibName: "SearchProductCell", bundle: nil), forCellReuseIdentifier: "SearchProductCell")
        theCurrentView.txtSearch.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
      
        //theCurrentView.txtSearch.modifyClearButtonWithImage(image: UIImage(named: "ic_close_gray")!)
    }
    
}
//MARK:- TestFeild Delegate
extension SearchProductVC : UITextFieldDelegate {
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //theCurrentView.imgSearch.isHidden = true
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
            /*if theCurrentView.txtSearch.text == ""{
                theCurrentView.imgSearch.isHidden = false
            }else{
                theCurrentView.imgSearch.isHidden = true
            }*/
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
    @objc func textFieldDidChange(_ textField: UITextField)
    {
        mainModelView.searchTxt = textField.text!
        /*if textField.text?.count == 0
        {
            theCurrentView.imgSearch.isHidden = false
        }else
        {
            theCurrentView.imgSearch.isHidden = true
        }*/
        searchApi()
    }
}
//MARK: - Table Delegate

extension SearchProductVC:  UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if self.mainModelView.searchListdata.count == 0
         {
             let lbl = UILabel()
             lbl.text = "No_list_yet".localized
             lbl.numberOfLines = 5
             lbl.textAlignment = NSTextAlignment.center
             lbl.textColor = UIColor.appThemeGrayColor
             lbl.font = themeFont(size: 16.0, fontname: .regular)
             lbl.center = tableView.center
             tableView.backgroundView = lbl
             return 0
         }
         tableView.backgroundView = nil
         return self.mainModelView.searchListdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchProductCell") as!  SearchProductCell
        let dict = self.mainModelView.searchListdata[indexPath.row]
        if dict.productImage !=  nil
        {
            cell.imgSearch.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgSearch.sd_setImage(with: dict.productImage?.toURL(), placeholderImage: UIImage(named: "ic_search_icon"), options: .lowPriority, context: nil)
        } else {
            cell.imgSearch.image = UIImage(named: "ic_search_icon")
        }
        cell.lblTitle.text = dict.productTitle ?? ""
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.mainModelView.searchListdata[indexPath.row]
        if dict.priceBidBarg == nil && dict.productType?.lowercased() == "Service".localized.lowercased(){
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "ServiceDetailVc") as! ServiceDetailVc
            obj.mainModelView.productId = "\(self.mainModelView.searchListdata[indexPath.row].id ?? 0)"
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "MainProductDetailsVc") as! MainProductDetailsVc
            obj.mainModelView.productId = "\(dict.id ?? 0)"
            if dict.priceBidBarg?.lowercased() == "bid".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "price".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "bargeding".lowercased(){
                obj.selectedController = .multipleSegment
            }else{
               obj.selectedController = .singleSegment
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
}
//MARK: ApiSetUp
extension SearchProductVC {
    
    ///---- LOgin
    func searchApi() {
        let parameters : [String : Any] =   [ APIKey.key_search:mainModelView.searchTxt
        ]
        print("parameters:", parameters)
        
        mainModelView.searchApi(param: parameters, success: { (SearchData) in
            
            if SearchData.flag == 1{
                self.mainModelView.searchListdata = SearchData.searchdata ?? [Searchdata]()
            }else{
                self.mainModelView.searchListdata.removeAll()
            }
            self.theCurrentView.tblSearch.reloadData()
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
//                makeToast(strMessage: error)
            }
            self.mainModelView.searchListdata.removeAll()
             self.theCurrentView.tblSearch.reloadData()
        })
    }
    
    
}
