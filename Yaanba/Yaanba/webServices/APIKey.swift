//
//  APIKey.swift
//  Yaanba
//
//  Created by Abhay on 13/02/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

struct APIKey {
    
    static let key_success = "success"
    static let key_msg = "msg"
    static let key_fields = "fields"
    static let key_api_key = "api_key"
    static let key_flag = "flag"
    
    
    ///----- Login
    
    static let key_mobile_no = "mobile_no"
    static let key_name = "name"
    static let key_username = "username"
    static let key_email = "email"    
    static let key_password = "password"
    static let key_password_verify = "password_verify"
    static let key_old_password = "old_password"
    static let key_new_password = "new_password"
    static let key_device_token = "device_token"
    static let key_register_id = "register_id"
    static let key_device_type = "device_type"
    static let key_country_code = "country_code"
    static let key_verification_id = "verification_id"
    
     ///----- Home
    static let key_category_id = "category_id"
    static let key_search = "search"
    static let key_id = "id"
    
    ///----- Add Product
    static let key_product_type = "product_type"
    static let key_sub_category_id = "sub_category_id"
    static let key_sub_subcategory_id = "sub_subcategory_id"
    static let key_product_title = "product_title"
    static let key_description = "description"
    static let key_price = "price"
    static let key_alternative_mobile_no = "alternative_mobile_no"
    static let key_price_bid_barg = "price_bid_barg"
    static let key_product_image = "product_image"
    static let key_basic_price = "basic_price"
    static let key_step_price = "step_price"
    static let key_start_date = "start_date"
    static let key_end_date = "end_date"
    static let key_bid = "bid"
    ///----- Product
    static let key_product_id = "product_id"
    static let key_comment_price = "comment_price"
    static let key_comment = "comment"
    static let key_user_id = "user_id"
    static let key_rate = "rate"
    static let key_action = "action"
    
}
