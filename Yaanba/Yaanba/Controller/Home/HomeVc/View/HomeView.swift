
//
//  HomeView.swift
//  Yaanba
//
//  Created by Abhay on 07/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class HomeView: UIView {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var collectionheader: UICollectionView!
    @IBOutlet weak var txtSearch: CustomTextField!
    
    func setupUI(theController:HomeVc){
        
        btnViewAll.setTitle("VIEW_ALL_key".localized, for: .normal)
        btnViewAll.setTitleColor(UIColor.appThemeYellowColor, for: .normal)
        btnViewAll.titleLabel?.font = themeFont(size: 8, fontname: .semibold)
        btnViewAll.layer.cornerRadius = btnViewAll.bounds.height/2
        btnViewAll.layer.masksToBounds = true
        
        collectionheader.register(UINib(nibName: "homeHeaderCollectionCell", bundle: nil), forCellWithReuseIdentifier: "homeHeaderCollectionCell")
        
        txtSearch.delegate = theController
        txtSearch.placeholder = "Search_here_key".localized
        txtSearch.font = themeFont(size: 15, fontname: .regular)
        
    }
    
}
