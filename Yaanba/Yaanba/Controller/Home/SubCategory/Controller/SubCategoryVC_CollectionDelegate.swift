//
//  SubCategoryVC_CollectionDelegate.swift
//  Yaanba
//
//  Created by vishal on 27/11/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension SubCategoryVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.mainVM.dictSubCategoryData?.subCategorydata?.count ?? 0 == 0 {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height))
            messageLabel.text = "No list yet".localized
            messageLabel.textColor = UIColor.appThemeLightGrayColor
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.font = themeFont(size: 14, fontname: .regular)
            messageLabel.sizeToFit()
            collectionView.backgroundView = messageLabel
            return 0
        }
        
        collectionView.backgroundView = nil
        return self.mainVM.dictSubCategoryData?.subCategorydata?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeProductCollectionCell", for: indexPath) as! HomeProductCollectionCell
        
        if let dict = self.mainVM.dictSubCategoryData?.subCategorydata?[indexPath.row] {
            cell.imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgProduct.sd_setImage(with: dict.categoryImage?.toURL(), placeholderImage: UIImage(named: "ic_detail_view_big_splash_holder"), options: .lowPriority, context: nil)
            cell.lblProduct.text = dict.subSubcategory
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let dict = self.mainVM.dictSubCategoryData?.subCategorydata?[indexPath.row] {
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "SpecificCategoryVc") as! SpecificCategoryVc
            obj.mainModelView.subCategoryId = "\(dict.id ?? 0)"
            obj.mainModelView.subCategoryName = dict.subSubcategory
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width/3, height: collectionView.bounds.height/4+15) // Product size
    }
    
}
