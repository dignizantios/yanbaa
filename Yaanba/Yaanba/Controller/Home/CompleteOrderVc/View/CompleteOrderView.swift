//
//  CompleteOrderView.swift
//  Yaanba
//
//  Created by Abhay on 18/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class CompleteOrderView: UIView {

    //MARK:-  Oultet
    
    @IBOutlet weak var imgThumsup: UIImageView!
    @IBOutlet weak var lblSuccessFailure: UILabel!
    @IBOutlet weak var btnContinue: CustomButton!
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        lblSuccessFailure.textColor = UIColor.appThemeDarkBlueColor
        lblSuccessFailure.font = themeFont(size: 15, fontname: .regular)
        
        btnContinue.setupThemeButtonUI()
        btnContinue.setTitle("CONTINUE_key".localized, for: .normal)
        
    }
    
}
