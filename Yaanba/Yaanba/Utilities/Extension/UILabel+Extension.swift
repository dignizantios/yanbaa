//
//  UILabel+Extension.swift
//  Liber
//
//  Created by YASH on 24/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

extension UILabel
{
    
  
    func addImageToLabel(imageName: String, strText : String, aboveString : String)
    {
        
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:imageName)
        //Set bound to reposition
        let imageOffsetY:CGFloat = 0.0;
        
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        
        let str = NSMutableAttributedString(string: aboveString)
        
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        
        str.append(attachmentString)
        
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(str)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: strText)
        completeText.append(textAfterIcon)
        
        self.attributedText = completeText
       
    }
    
}
