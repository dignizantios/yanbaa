import Foundation 
import ObjectMapper 

class LoginModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var accessToken: String? 
	var logindata: Logindata? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		accessToken <- map["access_token"] 
		logindata <- map["data"]
	}
} 

class Logindata: Mappable { 

	var username: String? 
	var email: String? 
    var id: NSNumber?
    var name: String?
    var mobileNo: String?
    var isVerified: NSNumber?
    var ProfileImage: String?
    var count: NSNumber?
    var Status:String?
    var countryCode:NSNumber?
    
	required init?(map: Map){ 
	}
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        username <- map["username"]
        email <- map["email"]
        mobileNo <- map["mobile_no"]
        isVerified <- map["is_verified"]
        ProfileImage <- map["profile_image"]
        count <- map["count"]
        Status <- map["status"]
        countryCode <- map["country_code"]
    }
} 

