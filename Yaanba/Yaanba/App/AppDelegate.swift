//
//  AppDelegate.swift
//  Yaanba
//
//  Created by Abhay on 01/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import UserNotifications
import SwiftyJSON
import FirebaseMessaging


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    static let shared = UIApplication.shared.delegate as! AppDelegate
    var objCustomTabBar = TabbarController()
    var navControler:UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Auth.auth().languageCode = "er";
        /*FirebaseApp.configure()
        Auth.auth().languageCode = "er";
        Auth.auth().settings?.isAppVerificationDisabledForTesting = true
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        let phoneNumber = "+917874322023"
        // This test verification code is specified for the given test phone number in the developer console.
        let testVerificationCode = "123456"
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID ?? "",
        verificationCode: testVerificationCode)
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
          if let error = error {
            // ...
            print(error)
            return
          }
          // User is signed in
          // ...
            print(authResult)
        }
       PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
         if let error = error {
           //self.showMessagePrompt(error.localizedDescription)
            print(error.localizedDescription)
           return
         }
         // Sign in using the verificationID and the code sent to the user
         // ...
       }*/
        let fcmToken = Messaging.messaging().fcmToken
        print("fcmToken:\(fcmToken)")
        isNotSetupTabBar = true
        Messaging.messaging().delegate = self
        //removeTopNotification()
        registerForPushNotifications()
        let count =  UIApplication.shared.applicationIconBadgeNumber
        Defaults.removeObject(forKey: "badgeCnt")
        Defaults.set(count, forKey: "badgeCnt")
        if count > 0{
            UIApplication.shared.applicationIconBadgeNumber = count
            objCustomTabBar.tabBar.items![3].badgeValue = "\(count)"
        }else{
            UIApplication.shared.applicationIconBadgeNumber = 0
            objCustomTabBar.tabBar.items![3].badgeValue = nil
        }
        return true
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        let count =  UIApplication.shared.applicationIconBadgeNumber
        Defaults.removeObject(forKey: "badgeCnt")
        Defaults.set(count, forKey: "badgeCnt")
        if count > 0{
            UIApplication.shared.applicationIconBadgeNumber = count
            objCustomTabBar.tabBar.items![3].badgeValue = "\(count)"
        }else{
            UIApplication.shared.applicationIconBadgeNumber = 0
            objCustomTabBar.tabBar.items![3].badgeValue = nil
        }
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        let count =  UIApplication.shared.applicationIconBadgeNumber
        Defaults.removeObject(forKey: "badgeCnt")
        Defaults.set(count, forKey: "badgeCnt")
        if count > 0{
            UIApplication.shared.applicationIconBadgeNumber = count
            objCustomTabBar.tabBar.items![3].badgeValue = "\(count)"
        }else{
            UIApplication.shared.applicationIconBadgeNumber = 0
            objCustomTabBar.tabBar.items![3].badgeValue = nil
        }
    }
    // For iOS 9+
    func application(_ application: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      if Auth.auth().canHandle(url) {
        return true
      }
      // URL not auth related, developer should handle it.
        return false
    }

    // For iOS 8-
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool {
      if Auth.auth().canHandle(url) {
        return true
      }
      // URL not auth related, developer should handle it.
        return false
    }
   /* @available(iOS 13.0, *)
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
      for urlContext in URLContexts {
          let url = urlContext.url
          Auth.auth().canHandle(url)
      }
      // URL not auth related, developer should handle it.
        
    }
 */
    //MARK: Push Notification
    func removeTopNotification() {
           if #available(iOS 10.0, *) {
              let center = UNUserNotificationCenter.current()
              center.removeAllDeliveredNotifications()
              center.removeAllPendingNotificationRequests()
           }
           else {
               UIApplication.shared.cancelAllLocalNotifications()
           }
       }
    func registerForPushNotifications()
    {
        UNUserNotificationCenter.current().delegate = self
       
        if #available(iOS 10.0, *) {
           
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                // 1. Check if permission granted
                guard granted else { return }
                // 2. Attempt registration for remote notifications on the main thread
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        print("Device Token = ", token)
        
        Defaults.set(token, forKey: "device_token")
       
        // Pass device token to auth.
        Messaging.messaging().apnsToken = deviceToken
        
        print("FCM Token = \(Messaging.messaging().fcmToken ?? "")")
        Defaults.set("\(Messaging.messaging().fcmToken ?? "")", forKey: "fcm_token")
        Defaults.synchronize()
        let firebaseAuth = Auth.auth()
        firebaseAuth.setAPNSToken(deviceToken, type: .unknown)
        //At development time we use .sandbox
        //firebaseAuth.setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
        
        //At time of production it will be set to .prod
        //Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for remote notifications with error: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo);
    }
    
    func application(_ application: UIApplication,
    didReceiveRemoteNotification notification: [AnyHashable : Any],
    fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
       let userInfo = JSON(notification)
        print("info:=\(JSON(userInfo))")
        
        let firebaseAuth = Auth.auth()
        
        if (firebaseAuth.canHandleNotification(notification)){
            print("Notification",userInfo)
            completionHandler(.noData)
            return
        }
        print("Notification",userInfo)
        
        
        /*if let data = userInfo["payload"] as? NSDictionary
        {
            self.HandleSurvayNotification(data: data)
        }*/
        completionHandler(.noData)
    }
    // While Banner Tap and App in Background mode.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("%@",response.notification.request.content.userInfo);
       let dict = response.notification.request.content.userInfo
       
       print("JSON DICt - ",JSON(dict))
       
       var dictPush = JSON(dict)
       print("dictPush - ",dictPush)
       let badgeCnt = dictPush["count"].intValue
       Defaults.removeObject(forKey: "badgeCnt")
       Defaults.set(badgeCnt, forKey: "badgeCnt")
      
       UIApplication.shared.applicationIconBadgeNumber = badgeCnt
       if badgeCnt > 0{
           objCustomTabBar.tabBar.items![3].badgeValue = "\(badgeCnt)"
           
       }else{
           objCustomTabBar.tabBar.items![3].badgeValue = nil
       }
        //1= bid 2 = review 3= comment
        if Defaults.object(forKey: remember_userid_key) != nil{
            if isNotSetupTabBar{
                 pushData = dictPush
                /*
//                objCustomTabBar  = TabbarController()
//                let navigationController = UINavigationController(rootViewController: objCustomTabBar)
//                //navigationController.popToRootViewController(animated: true)
//                pushData = dictPush
//                objCustomTabBar.selectedIndex = 0
//                self.window?.rootViewController = navigationController
                */
            }else{
                pushData = dictPush
                if pushData["type"].stringValue == "1" && pushData["user_id"].stringValue == Defaults.object(forKey: remember_userid_key) as! String{
                    if objCustomTabBar.selectedIndex == 4{
                          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isBidOnMyProduct"), object: nil)
                    }else{
                        objCustomTabBar.selectedIndex = 4
                    }
                }else{
                    if objCustomTabBar.selectedIndex == 0{
                          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isNewPushArrived"), object: nil)
                    }else{
                        objCustomTabBar.selectedIndex = 0
                    }
                }
            }
        }
        
        completionHandler()
    }
    // While App on Foreground mode......
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("%@",notification.request.content.userInfo);
        let userInfo = notification.request.content.userInfo
        let dictPush = JSON(userInfo)
        print("dictNotificationData \(dictPush)" )
        let badgeCnt = dictPush["count"].intValue
        Defaults.removeObject(forKey: "badgeCnt")
        Defaults.set(badgeCnt, forKey: "badgeCnt")
        Defaults.synchronize()
        UIApplication.shared.applicationIconBadgeNumber = badgeCnt
        
        if badgeCnt > 0{
            objCustomTabBar.tabBar.items![3].badgeValue = "\(badgeCnt)"
            
        }else{
            objCustomTabBar.tabBar.items![3].badgeValue = nil
        }
        if isProductDetailsOpen == true{
            pushData = dictPush
            if pushData["type"].stringValue != "1" && pushData["user_id"].stringValue != Defaults.object(forKey: remember_userid_key) as! String{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshProductDetails"), object: nil)
                
            }
        }
        completionHandler([.alert, .sound, .badge])
    }
    
    func loadHomeScreen(){
      self.objCustomTabBar = TabbarController()
        //self.navControler = UINavigationController.init(rootViewController: self.objCustomTabBar)
        self.window?.rootViewController = self.objCustomTabBar
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if (Defaults.string(forKey: "badgeCnt") != nil)
        {
            let badgeCnt:Int = Defaults.object(forKey: "badgeCnt") as? Int ?? 0
            print("badgeCnt-->",badgeCnt)
            if badgeCnt > 0{
                let isVerify = Defaults.object(forKey: remember_accesstoken_key) as? String
               
                if isVerify != ""
                {
                    UIApplication.shared.applicationIconBadgeNumber = badgeCnt
                    objCustomTabBar.tabBar.items![3].badgeValue = "\(badgeCnt)"
                }else{
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    objCustomTabBar.tabBar.items![3].badgeValue = nil
                }
               
            }else{
                UIApplication.shared.applicationIconBadgeNumber = 0
                 objCustomTabBar.tabBar.items![3].badgeValue = nil
            }
            
        }else{
            UIApplication.shared.applicationIconBadgeNumber = 0
            objCustomTabBar.tabBar.items![3].badgeValue = nil
            
        }
        let count =  UIApplication.shared.applicationIconBadgeNumber
        Defaults.removeObject(forKey: "badgeCnt")
        Defaults.set(count, forKey: "badgeCnt")
        if count > 0{
            UIApplication.shared.applicationIconBadgeNumber = count
            objCustomTabBar.tabBar.items![3].badgeValue = "\(count)"
        }else{
            UIApplication.shared.applicationIconBadgeNumber = 0
            objCustomTabBar.tabBar.items![3].badgeValue = nil
        }
        //connectToFcm()
    }

}
//MARK: - Firebase Messeging delegate methods

extension AppDelegate : MessagingDelegate
{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        //print("Firebase registration token: \(fcmToken)")
        //print("==== FCM Token:  ",fcmToken)
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        /*InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //  self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
        Messaging.messaging().shouldEstablishDirectChannel = false*/
        let instance = InstanceID.instanceID()
        instance.deleteID { (error) in
//          print(error.debugDescription)
        }

        instance.instanceID { (result, error) in
          if let error = error {
            print("Error fetching remote instange ID: \(error)")
          } else {
           // print("Remote instance ID token: \(String(describing: result?.token))")
          }
        }
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
}
