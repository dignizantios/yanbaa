//
//  homeHeaderCollectionCell.swift
//  Yaanba
//
//  Created by Abhay on 07/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class homeHeaderCollectionCell: UICollectionViewCell {

    //MARK: - Outlet
    @IBOutlet weak var btnTitle: UIButton!
    
    //MARK: - view life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if DeviceLanguage == "ar"{
            btnTitle.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        btnTitle.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
        btnTitle.setTitleColor(UIColor.black, for: .selected)
        
        // Initialization code
    }

}
