//
//  HomeSubVc.swift
//  Yaanba
//
//  Created by Abhay on 07/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class HomeSubVc: UIViewController {

    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:HomeSubView = { [unowned self] in
        return self.view as! HomeSubView
    }()
    lazy var mainModelView: HomeSubViewModel = {
        return HomeSubViewModel(theController: self)
    }()
    
    //MARKL - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI(delegate:self)
        // Do any additional setup after loading the view.
        getSubCategoryApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        theCurrentView.collectionViewUpperProduct.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        theCurrentView.collectionViewOffer.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        theCurrentView.tblProduct.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        /*
        if self.mainModelView.subCategorys.count == 0{
            theCurrentView.heightConstantUpperProduct.constant = 0
        }*/
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        theCurrentView.collectionViewUpperProduct.removeObserver(self, forKeyPath: "contentSize")
        theCurrentView.collectionViewOffer.removeObserver(self, forKeyPath: "contentSize")
        theCurrentView.tblProduct.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if object is UICollectionView {
            theCurrentView.heightConstantUpperProduct.constant = theCurrentView.collectionViewUpperProduct.contentSize.height
        }
        
        if object is UICollectionView {
            theCurrentView.heightConstantOffer.constant = theCurrentView.collectionViewOffer.contentSize.height
        }
        
        if object is UITableView {
            theCurrentView.heightConstantProductTbl.constant = theCurrentView.tblProduct.contentSize.height
        }
        
    }
}

//MARK: - CollectionView

extension HomeSubVc:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == theCurrentView.collectionViewUpperProduct{
            return self.mainModelView.subCategorys.count
        }
        return self.mainModelView.discountArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == theCurrentView.collectionViewUpperProduct{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeProductCollectionCell", for: indexPath) as! HomeProductCollectionCell
            let dic = self.mainModelView.subCategorys[indexPath.row]
            if dic.categoryImage !=  "" {
                cell.imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.imgProduct.sd_setImage(with: dic.categoryImage?.toURL(), placeholderImage: UIImage(named: "ic_detail_view_big_splash_holder"), options: .lowPriority, context: nil)
            } else {
                cell.imgProduct.image = UIImage(named: "ic_detail_view_big_splash_holder")
            }
            cell.lblProduct.text = dic.subCategory ?? ""
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OfferCollectioncell", for: indexPath) as! OfferCollectioncell
        cell.lblExtra.text = "Extra_key".localized
        cell.lblDiscount.text = "\(self.mainModelView.discountArray[indexPath.row].discount ?? 0)% "+"Off_key".localized
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == theCurrentView.collectionViewUpperProduct{
            
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            obj.mainVM.subCategoryId = "\(self.mainModelView.subCategorys[indexPath.row].id ?? 0)"
            obj.mainVM.subCategoryName = self.mainModelView.subCategorys[indexPath.row].subCategory ?? ""
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: true)
            
            
//            let obj = homeStoryboard.instantiateViewController(withIdentifier: "SpecificCategoryVc") as! SpecificCategoryVc
//            obj.mainModelView.subCategoryId = "\(self.mainModelView.subCategorys[indexPath.row].id ?? 0)"
//            obj.mainModelView.subCategoryName = self.mainModelView.subCategorys[indexPath.row].subCategory ?? ""
//            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "DiscountDetailsVC") as! DiscountDetailsVC 
            obj.mainModelView.discountId = Int(truncating: self.mainModelView.discountArray[indexPath.row].id ?? 0)
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == theCurrentView.collectionViewUpperProduct{
            return CGSize(width: collectionView.bounds.width/3, height: 180) // Product size
        }
        return CGSize(width: collectionView.bounds.width/2, height: 100) // Offer size
    }
    
}

//MARK: - TableView

extension HomeSubVc:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mainModelView.mostPopularCategorys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTblCell") as! ProductTblCell
        let dict = self.mainModelView.mostPopularCategorys[indexPath.row]
        if dict.productImage !=  "" {
            cell.imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgProduct.sd_setImage(with: dict.productImage?.toURL(), placeholderImage: UIImage(named: "ic_home_futter_splash_holder"), options: .lowPriority, context: nil)
        } else {
            cell.imgProduct.image = UIImage(named: "ic_home_futter_splash_holder")
        }
        cell.lblProductName.text = dict.productTitle ?? ""
        if dict.priceBidBarg?.lowercased() == "bid".lowercased(){
            cell.lblPrice.text = "\("KWD".localized.uppercased()) \(dict.basicPrice ?? 0) "
        }else{
            cell.lblPrice.text = "\("KWD".localized.uppercased()) \(dict.price ?? 0) "
        }
        //cell.lblPrice.text = "\("KWD".localized) \(dict.price ?? 0)"
        cell.lblDiscount.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.mainModelView.mostPopularCategorys[indexPath.row]
        if dict.priceBidBarg == nil{
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "ServiceDetailVc") as! ServiceDetailVc
            obj.mainModelView.productId = "\(dict.id ?? 0)"
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
           let obj = homeStoryboard.instantiateViewController(withIdentifier: "MainProductDetailsVc") as! MainProductDetailsVc
            obj.mainModelView.productId = "\(dict.id ?? 0)"
            if dict.priceBidBarg?.lowercased() == "bid".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "price".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "bargeding".lowercased(){
                obj.selectedController = .multipleSegment
            }else{
               obj.selectedController = .singleSegment
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
        

    }
}
//MARK: ApiSetUp
extension HomeSubVc {
    
    ///---- Get CategoryList
    func getSubCategoryApi() {
        let parameters : [String : Any] =   [ APIKey.key_category_id:self.mainModelView.selectedCategoryId
        ]
        print("parameters",parameters)
        mainModelView.SubCategoryApi(param: parameters, success: { (categoryData) in
            if let data = categoryData.homeSubCategorydata{
                self.mainModelView.subCategorys = []
                self.mainModelView.subCategorys = data.subCategory ?? [SubCategory]()
                self.mainModelView.mostPopularCategorys = []
                self.mainModelView.mostPopularCategorys = data.mostPopular ?? [MostPopular]()
                self.theCurrentView.collectionViewUpperProduct.reloadData()
                self.mainModelView.discountArray = []
                self.mainModelView.discountArray = data.discount ?? [Discount]()
                self.theCurrentView.collectionViewOffer.reloadData()
                if self.mainModelView.discountArray.count == 0{
                    self.theCurrentView.heightConstantOffer.constant = 0
                }
                self.theCurrentView.tblProduct.reloadData()
            }
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
//                makeToast(strMessage: error)
            }
        })
    }
    
    
}

