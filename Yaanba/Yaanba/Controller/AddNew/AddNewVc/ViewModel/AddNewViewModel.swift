//
//  AddNewViewModel.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class AddNewViewModel  {
    
    //MARK: - view life cycle
    fileprivate weak var theController:AddNewVc!
    
    init(theController:AddNewVc) {
        self.theController = theController
    }
    
    //MARK: - Varaible
    var arrayCategoryData : [JSON] = []
    var arrayOfImages = [UIImage]()
    var selectedCategoryId = String()
    var subCategorys: [SubCategorydata] = []
    var selectedSubCategoryId = String()
    var subofSubCategorys: [SubCategorydata] = []
    var selectedSubofSubCategoryId = String()
    var dictSubCategoryData: SubCategoryModel? = nil
    
}
//MARK: Api setup
extension AddNewViewModel {
    
    func ValidateDetails() {
        
        if ((self.theController.view as? AddNewView)!.txtSelectProductCategory.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage:"Please select product category".localized)
        }
        else if ((self.theController.view as? AddNewView)!.txtChooseType.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please select type".localized)
        }
        else if ((self.theController.view as? AddNewView)!.txtSelectSubCategory.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please select product sub category".localized)
        }
        else if ((self.theController.view as? AddNewView)!.txtSubOfSubCategory.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please select product Sub-subcategory".localized)
        }
        else if ((self.theController.view as? AddNewView)!.txtItemTitle.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please enter item title".localized)
        }
        else if ((self.theController.view as? AddNewView)!.txtAlternativeMobileNumber.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please enter alternative mobile number".localized)
        }else if (self.theController.view as? AddNewView)!.txtAlternativeMobileNumber.text!.count <= 7 {
           makeToast(strMessage: "Mobile_number_should_be_greater_than_7_digits".localized)
        }
        else if ((self.theController.view as? AddNewView)!.txtvwDescribeYourItem.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please enter some item description".localized)
        }
        else if self.arrayOfImages.count <= 0 {
            makeToast(strMessage: "Please select atleast one item image".localized)
        }else{
            if (self.theController.view as? AddNewView)!.txtChooseType.text!.localized.lowercased() == "Service".localized.lowercased(){
                //API calling
                self.theController.addProductApi()
                
            }else{
                if ((self.theController.view as? AddNewView)!.txtChooseSellType.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
                    makeToast(strMessage: "Please select sell type".localized)
                }
                else if (self.theController.view as? AddNewView)!.txtChooseSellType.text!.localized.lowercased() == "Bidding".localized.lowercased(){
                    if ((self.theController.view as? AddNewView)!.txtBasicPrice.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
                        makeToast(strMessage: "Please enter basic price".localized)
                    }else if ((self.theController.view as? AddNewView)!.txtStepPrice.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
                        makeToast(strMessage: "Please enter step price".localized)
                    }else if ((self.theController.view as? AddNewView)!.txtStartDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
                        makeToast(strMessage: "Please select start date".localized)
                    }else if ((self.theController.view as? AddNewView)!.txtStartTime.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
                        makeToast(strMessage: "Please select start time".localized)
                    }else if ((self.theController.view as? AddNewView)!.txtEndDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
                        makeToast(strMessage: "Please select end date".localized)
                    }else if ((self.theController.view as? AddNewView)!.txtEndDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
                        makeToast(strMessage: "Please select end time".localized)
                    }else{
                        //API calling
                        self.theController.addProductApi()
                    }
                }
                else if (self.theController.view as? AddNewView)!.txtChooseSellType.text!.localized.lowercased() == "Auction".localized.lowercased() || (self.theController.view as? AddNewView)!.txtChooseSellType.text!.localized.lowercased() == "Price".localized.lowercased(){
                    if ((self.theController.view as? AddNewView)!.txtItemPrice.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
                        makeToast(strMessage: "Please enter item price".localized)
                    }else{
                        //API calling
                        self.theController.addProductApi()
                    }
                }
            }
        }
    }
    
    func CategoryListApi(param:[String:Any],success:@escaping(_ token : CategoryModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aHomeCategory
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        
                        if let CategoryData = CategoryModel(JSON: data) {
                            success(CategoryData)
                            return
                        }
                    }
                    return
                    
                }
                failed(value["msg"].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed("No_internet_connection_key".localized)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed("Request_time_out_Please_try_again".localized)
                } else {
                    // other failures
                    failed("Something_went_wrong_key".localized)
                }
                // Utility.shared.stopActivityIndicator()
                break
            }
        }
    }
    
    func SubCategoryApi(param:[String:Any],success:@escaping(_ token : SubCategoryModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aSubCategory
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        // showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            // hideLoader()
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        
                        if let SubCategoryData = SubCategoryModel(JSON: data) {
                            success(SubCategoryData)
                            return
                        }
                    }
                    return
                    
                }
                failed(value["msg"].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed("No_internet_connection_key".localized)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed("Request_time_out_Please_try_again".localized)
                } else {
                    // other failures
                    failed("Something_went_wrong_key".localized)
                }
                // Utility.shared.stopActivityIndicator()
                break
            }
        }
    }
    
    func subOfSubCategoryApi(param:[String:Any],success:@escaping() -> Void, failed:@escaping(String) -> Void) {
        
        let url = sub_subcategoryURL
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        print("PARAM:- \(param)")
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        let arr = JSON(data["data"]).arrayValue
                        self.subofSubCategorys = []
                        let arrSubofSubCategory = arr.map{$0["sub_subcategory"].stringValue}
                        print("Array:-\(arrSubofSubCategory)")
                        self.theController.chooseSubOfSubCategoryDD.dataSource = arrSubofSubCategory
                        for i in 0..<arr.count {
                            let dict = arr[i].dictionaryObject
                            self.subofSubCategorys.append(SubCategorydata(JSON: dict!)!)
                        }
                        success()
                        return
                    }
                    return
                }
                failed(value["msg"].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed("No_internet_connection_key".localized)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed("Request_time_out_Please_try_again".localized)
                } else {
                    failed("Something_went_wrong_key".localized)
                }
                break
            }
        }
    }
    
    
    func AddProductApi(param:[String:Any],image:[Data],NameArray:[String],FileNameArray:[String],success:@escaping(_ token : AddProductModel) -> Void, failed:@escaping(String) -> Void) {
        let boundary = "\(UUID().uuidString).png"
        let url = aAddProduct
        var header:[String:String] = [:]
        if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
             header["Authorization"] = "Bearer \(auth)"
        }
        // header["Version"] = iosVersion
        header["Content-Type"] = "multipart/form-data; boundary=--------------------------503146432196697590930355"
        //header["X-localization"] = DeviceLanguage
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().postUsingMultiPartData(name: url, parameter: param, files: image, withName: NameArray, withFileName: FileNameArray, mimeType: ["image/png"]) { (result) in
            print(result["msg"].stringValue)
            hideLoader()
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result.dictionaryObject {
                    
                    if let AddProductData = AddProductModel(JSON: data) {
                        success(AddProductData)
                        return
                    }
                }
                return
                
            }
            failed(result["msg"].stringValue)
        }
       /*WebServices().MakePostWithImageAPI(name: url, params: param, images: image,imageName:"product_image", header: header, completionHandler: { (result) in
            hideLoader()
            switch result {
            case .success(let value):
                
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        
                        if let AddProductData = AddProductModel(JSON: data) {
                            success(AddProductData)
                            return
                        }
                    }
                    return
                    
                }
                failed(value["msg"].stringValue)
                break
            case .failure(let error):
                
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed("No_internet_connection_key".localized)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed("Request_time_out_Please_try_again".localized)
                } else {
                    // other failures
                    failed("Something_went_wrong_key".localized)
                }
                // Utility.shared.stopActivityIndicator()
                break
            }
        }) { (error) in
            hideLoader()
            failed(error.localizedDescription)
             //makeToast(strMessage: error.localizedDescription)
        }*/
       
    }
}

