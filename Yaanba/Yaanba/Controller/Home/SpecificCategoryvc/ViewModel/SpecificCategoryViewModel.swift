//
//  SpecificCategoryViewModel.swift
//  Yaanba
//
//  Created by Abhay on 08/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class SpecificCategoryViewModel{
    
    //MARK: - View life cycle
    fileprivate weak var theController:SpecificCategoryVc!
    
    init(theController:SpecificCategoryVc) {
        self.theController = theController
    }
    var subCategoryId = String()
     var subCategoryName = String()
    var productListdata = [ProductListdata]()
}
//MARK: Api setup
extension SpecificCategoryViewModel {
    
    
    func productListApi(param:[String:Any],success:@escaping(_ token : ProductListModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aProductList
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let productListData = ProductListModel(JSON: data) {
                             success(productListData)
                             return
                         }
                     }
                     return
                     
                 }
                failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}

