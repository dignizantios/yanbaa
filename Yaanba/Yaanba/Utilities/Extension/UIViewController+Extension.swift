//
//  UIViewController+Extension.swift
//  Liber
//
//  Created by om on 10/3/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import AVFoundation
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire
import DropDown
import RxCocoa
import RxSwift
import Firebase
import FirebaseAuth

extension UIViewController
{
    
    //MARKL - Fonts
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    
    //MARK: - Images with String
    
    
    func getAttributedString(imgAttachment:UIImage,strPostText:String) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = imgAttachment
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(image1String)
        fullString.append(NSAttributedString(string: strPostText))
        return fullString
    }
    
    
    func getAttributedString(imgAttachment:UIImage,strPreText:String) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = imgAttachment
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(NSAttributedString(string: strPreText))
        fullString.append(image1String)
        return fullString
    }
    
    
    //MARK: - Call Method
    
    
    func callUser(strPhoneNumber:String)
    {
        
        let phone = strPhoneNumber
        
        var phoneStr: String = "telprompt://\(phone)"
        phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
        
        let urlPhone = URL(string: phoneStr)
        if UIApplication.shared.canOpenURL(urlPhone!)
        {
            //UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(urlPhone!)
            }
        
        }
        else
        {
            // KSToastView.ks_showToast("Call facility is not available!!!", duration: ToastDuration)
        }
        
    }
    
    //MARK: - navigation with sideMenu
    
    func setupNavigationbarBackButton(titleText:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = UIColor.appThemeDarkBlueColor
        HeaderView.font = themeFont(size: 15, fontname: .bold)
        
        self.navigationItem.titleView = HeaderView
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow")?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = .appThemeDarkBlueColor
        self.navigationItem.leftBarButtonItem = leftButton
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.appThemeYellowColor
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeYellowColor
    }
    
    func setupNavigationbarCenterText(titleText:String)
    {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = UIColor.appThemeDarkBlueColor
        HeaderView.font = themeFont(size: 15, fontname: .bold)
        
        self.navigationItem.titleView = HeaderView
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.appThemeYellowColor
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeYellowColor
    }
    
    @objc func backButtonTapped()
    {
        if isPushArrivedProductDetailsOpen == true{
            isPushArrivedProductDetailsOpen = false
            appDelegate.loadHomeScreen()
            return
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func transperentNavigationBar(){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow")?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = .appThemeDarkBlueColor
        self.navigationItem.leftBarButtonItem = leftButton
        
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }   
   
    //MARK: - Configure Dropdown
    func configureDropdown(dropdown : DropDown,sender:UIControl)
    {
        dropdown.clearSelection()
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .automatic
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        //        dropdown.topOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.size.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.semanticContentAttribute = .unspecified //Get for sysytem content (.unspecified)
        dropdown.cancelAction = { [unowned self] in
            print("Drop down dismissed")
        }
    }
    
    //MARK: - Textfield Done button (NumberPad)
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appThemeDarkBlueColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done_key".localized, style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.view.endEditing(true)
    }
    
    
    //MARK: - TableView Error Msg
    
    func showErrorMessageInTableView(strMessage:String) {
    
        let lbl = UILabel()
        lbl.text = strMessage
        lbl.font = themeFont(size: 16, fontname: .semibold)
        lbl.textAlignment = NSTextAlignment.center
        lbl.textColor = UIColor.appThemeGrayColor
        lbl.center = UITableView().center
        UITableView().backgroundView = lbl
        
    }
    func withoutLoginPopup() {
        
        let alertController = UIAlertController(title: "Alert_key".localized, message: "You_are_not_login_please_login_first_key".localized, preferredStyle: UIAlertController.Style.alert)
        
     let okAction = UIAlertAction(title: "Yes_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
        // Defaults.removeObject(forKey: remember_email_key)
      //   Defaults.removeObject(forKey: remember_password_key)
         Defaults.removeObject(forKey: remember_accesstoken_key)
         Defaults.removeObject(forKey: "idToken")
         Defaults.removeObject(forKey: "firebase_verification")
         Defaults.removeObject(forKey: "badgeCnt")
         Defaults.synchronize()
        UIApplication.shared.applicationIconBadgeNumber = 0
        appDelegate.objCustomTabBar.tabBar.items![3].badgeValue = nil
         let vc  = mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
          let rearNavigation = UINavigationController(rootViewController: vc)
          appDelegate.window?.rootViewController = rearNavigation
        }
     
     let cancelAction = UIAlertAction(title: "No_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK: - Label with underline
    
    func setUIForUnderline(normal:String,underLine:String) -> NSAttributedString
    {
        
        let str1 = normal
        let str2 = underLine
        
        let str = str1 + " " + str2
        let interactableText = NSMutableAttributedString(string:str)
        
        let rangeAccount = (str as NSString).range(of: str1, options: .caseInsensitive)
        let rangeRegister = (str as NSString).range(of: str2, options: .caseInsensitive)
        
        interactableText.addAttribute(NSAttributedString.Key.font,
                                      value: themeFont(size: 14, fontname: .regular),
                                      range: NSRange(location: 0, length: interactableText.length))
        
        interactableText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: rangeAccount.length+1, length: rangeRegister.length))
        
        return interactableText
    }
   
    ///---- Logout
    func logoutApiCall() {
        let parameters =   [String : Any]()
         
        self.logout(param: parameters, success: { (loginData) in
           // Defaults.removeObject(forKey: remember_email_key)
           // Defaults.removeObject(forKey: remember_password_key)
            Defaults.removeObject(forKey: remember_accesstoken_key)
            Defaults.removeObject(forKey: "idToken")
            Defaults.removeObject(forKey: "firebase_verification")
            Defaults.removeObject(forKey: "badgeCnt")
            Defaults.synchronize()
            let vc  = mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
             let rearNavigation = UINavigationController(rootViewController: vc)
             appDelegate.window?.rootViewController = rearNavigation
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            makeToast(strMessage: error)
        })
    }
    
    func logout(param:[String:Any],success:@escaping(_ token : LogoutModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aLogout
       
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LogoutModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value[APIKey.key_msg].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }

}
func randomString(length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0...length-1).map{ _ in letters.randomElement()! })
}
