//
//  Global.swift
//  Yaanba
//
//  Created by YASH on 22/11/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import MaterialComponents
import SwiftyJSON
import SVProgressHUD


//MARK: - Set Toaster
func makeToast(strMessage : String){
    
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
    
}

//MARK:- Set defaults value

func getUserDetail(_ forKey: String) -> String{
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return "" }
    let data = JSON(userDetail)
    return data[forKey].stringValue
}

//MARK:- Get current timezone

func getCurrentTimeZone() -> String
{
    var localTimeZoneName: String
    {
        return TimeZone.current.identifier
    }
    return localTimeZoneName // "America/Sao_Paulo"
}

//MARK:- Loader

func showLoader() {
    AppDelegate.shared.window?.isUserInteractionEnabled = false
    SVProgressHUD.show()
 
}

func hideLoader(){
    AppDelegate.shared.window?.isUserInteractionEnabled = true
    SVProgressHUD.dismiss()
}


//MARK:- Authentication

var basic_username = ""
var basic_password = ""
var lang = "0"
var guestPassword = ""
let Defaults = UserDefaults.standard
let strDeviceType = "1"
let strSuccessResponse = "1"
let strAccessDenied = "-1"
var dictSideMenu = JSON()
var ACCEPTABLE_CHARACTERS = " "
var NUMERIC_CHARACTERS = ""
var passwordLegth = 8
var mobileLength = 10
var expireTokenDaysHour = 7


let appDelegate = UIApplication.shared.delegate as! AppDelegate

let DeviceLanguage = NSLocale.current.languageCode
//let remember_email_key = "remember_email"
//let remember_password_key = "remember_password"
let remember_accesstoken_key = "remember_accesstoken"
let remember_userid_key = "remember_userid"
let token_expire_date_key = "token_expire_date"
var idToken  = "idToken"
//MARK: - Storyboard

let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
let dealsStoryboard = UIStoryboard(name: "Deals", bundle: nil)
let addNewStoryboard = UIStoryboard(name: "AddNew", bundle: nil)
let notificationStoryboard = UIStoryboard(name: "Notification", bundle: nil)
let profileStoryboard = UIStoryboard(name: "Profile", bundle: nil)

//MARK:- Push Notification handle
var isProductDetailsOpen = Bool()
var pushData = JSON()
var isNotSetupTabBar = Bool()
var isPushArrivedProductDetailsOpen = Bool()

//MARK: - Date
func StringToDate(Formatter : String,strDate : String) -> Date
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    
    guard let convertedDate = dateformatter.date(from: strDate) else {
        let str = dateformatter.string(from: Date())
        return dateformatter.date(from: str)!
    }
    print("convertedDate - ",convertedDate)
    return convertedDate
}

func DateToString(Formatter : String,date : Date) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
    let convertedString = dateformatter.string(from: date)
    return convertedString
}

func stringTodate(OrignalFormatter : String,YouWantFormatter : String,strDate:String) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = OrignalFormatter
    guard let convertedDate = dateformatter.date(from: strDate) else {
        return ""
    }
    dateformatter.dateFormat = YouWantFormatter
    let convertedString = dateformatter.string(from: convertedDate)
    return convertedString
    
}

func localToUTC(date:String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.calendar = NSCalendar.current
    dateFormatter.timeZone = TimeZone.current

    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

    return dateFormatter.string(from: dt!)
}

func UTCToLocal(date:String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

    return dateFormatter.string(from: dt!)
}
