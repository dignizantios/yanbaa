import Foundation 
import ObjectMapper 

class TourModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var tourdata: [Tourdata]? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		tourdata <- map["data"]
	}
} 

class Tourdata: Mappable { 

	var id: NSNumber? 
	var image: String? 
	var createdAt: String? 
	var title: String? 
	var description: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		image <- map["image"] 
		createdAt <- map["created_at"] 
		title <- map["title"] 
		description <- map["description"] 
	}
} 

