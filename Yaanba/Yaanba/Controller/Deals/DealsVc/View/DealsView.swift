//  DealsView.swift
//  Yaanba
//
//  Created by Abhay on 17/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class DealsView: UIView {

    //MARK: - Outlet
    //@IBOutlet weak var collectionUpperDeals: UICollectionView!
    @IBOutlet weak var tblDeals: UITableView!
    @IBOutlet weak var constantTblDelasHeight: NSLayoutConstraint!
    //@IBOutlet weak var headerPageControl: UIPageControl!
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        tblDeals.register(UINib(nibName: "dealTblCell", bundle: nil), forCellReuseIdentifier: "dealTblCell")
        
    }
    
}
