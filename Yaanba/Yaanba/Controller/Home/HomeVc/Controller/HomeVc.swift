//
//  HomeVc.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import CarbonKit
import SwiftyJSON

class HomeVc: UIViewController {

    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:HomeView = { [unowned self] in
        return self.view as! HomeView
    }()
    lazy var mainModelView: HomeViewModel = {
        return HomeViewModel(theController: self)
    }()
    
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    
  //  var viewModel = HomeViewModel(theController: self)
    
    var currentSelectedIndexInCollection = 0
    var SelectedIndexInCollection = 0
    
    //MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("language : \(DeviceLanguage)")
        
        if DeviceLanguage == "ar"{
            theCurrentView.collectionheader.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            self.theCurrentView.collectionheader.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: false)
        }
        
        theCurrentView.setupUI(theController: self)
        
        let indexpath = IndexPath(row: Int(0), section: 0)
        //theCurrentView.collectionheader.selectItem(at: indexpath, animated: false, scrollPosition: .centeredHorizontally)
        
        categoryListApi()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.movetoProductDetails(notification:)), name: NSNotification.Name(rawValue: "isNewPushArrived"), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarCenterText(titleText: "HOME_key".localized)
        getNotificationCountApi()
        if pushData["productId"].stringValue != ""{
            movetoProductDetail()
        }
    }
    @objc func movetoProductDetails(notification: NSNotification) {
      //  DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
        if pushData["productId"].stringValue != "" {
            movetoProductDetail()
        }
          
       // }
        
    }
    func movetoProductDetail()
    {
        if pushData["productId"].stringValue != ""{
            if pushData["type"].stringValue == "1" && pushData["user_id"].stringValue == Defaults.object(forKey: remember_userid_key) as! String
            {
                 appDelegate.objCustomTabBar.selectedIndex = 4
            }else if pushData["price_bid_barg"].stringValue == "" && pushData["product_type"].stringValue.lowercased() == "Service".localized.lowercased(){
                    let obj = homeStoryboard.instantiateViewController(withIdentifier: "ServiceDetailVc") as! ServiceDetailVc
                    obj.mainModelView.productId = pushData["productId"].stringValue
                    isPushArrivedProductDetailsOpen = true
                    let nav = UINavigationController(rootViewController: obj)
                    appDelegate.window?.rootViewController = nav
                
            }else{
                    let obj = homeStoryboard.instantiateViewController(withIdentifier: "MainProductDetailsVc") as! MainProductDetailsVc
                    obj.mainModelView.productId = pushData["productId"].stringValue
                    let nav = UINavigationController(rootViewController: obj)
                    isPushArrivedProductDetailsOpen = true
                    if pushData["price_bid_barg"].stringValue.lowercased() == "bid".lowercased(){
                        obj.selectedController = .singleSegment
                    }else if pushData["price_bid_barg"].stringValue.lowercased() == "price".lowercased(){
                        obj.selectedController = .singleSegment
                    }else if pushData["price_bid_barg"].stringValue.lowercased() == "bargeding".lowercased(){
                        obj.selectedController = .multipleSegment
                    }else{
                       obj.selectedController = .singleSegment
                    }
                    appDelegate.window?.rootViewController = nav
            }
        }
    }
    
}

//MARK: - IBAction method

extension HomeVc{
    
    @IBAction func btnViweAllTapped(_ sender: Any) {
        if self.mainModelView.arrayData.count > 0{
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "HomeSubCategoryVc") as! HomeSubCategoryVc
            print(SelectedIndexInCollection)
             obj.mainModelView.selectedCategoryId = self.mainModelView.arrayData[SelectedIndexInCollection]["id"].stringValue
             obj.mainModelView.selectedCategoryName = self.mainModelView.arrayData[SelectedIndexInCollection]["name"].stringValue
             self.navigationController?.pushViewController(obj, animated: true)
        }
        
        
    }
    
    func pastCollectionReload(){
        
        var dict = self.mainModelView.arrayData[currentSelectedIndexInCollection]
        dict["selected"].stringValue = "0"
        self.mainModelView.arrayData[currentSelectedIndexInCollection] = dict
        
        let indexPath = IndexPath(item: currentSelectedIndexInCollection, section: 0)
        UIView.performWithoutAnimation {
            theCurrentView.collectionheader.reloadItems(at: [indexPath])
        }
        
    }
    
    func selectedCollectionReload(index:Int){
        
        var dict = self.mainModelView.arrayData[index]
        dict["selected"].stringValue = "1"
        self.mainModelView.arrayData[index] = dict
        
        let indexPath = IndexPath(item: index, section: 0)
        
        UIView.performWithoutAnimation {
            theCurrentView.collectionheader.reloadItems(at: [indexPath])
        }
        
    }
    
}

//MARK: - Carbon kit
extension HomeVc:CarbonTabSwipeNavigationDelegate
{
    func carbonSetupStyle(){
        
        //print("arrayData : \(self.mainModelView.arrayData)")
        
        let array = self.mainModelView.arrayData.map({$0["name"].stringValue})
        print("array : \(array)")
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: array, delegate: self)
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.clear)
        carbonTabSwipeNavigation.toolbarHeight.constant = 0.0
        carbonTabSwipeNavigation.setIndicatorHeight(0)
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: theCurrentView.mainView)
        carbonTabSwipeNavigation.view.layoutIfNeeded()
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
            let vc = homeStoryboard.instantiateViewController(withIdentifier: "HomeSubVc") as! HomeSubVc
        
        SelectedIndexInCollection = Int(index)
        print(SelectedIndexInCollection)
        vc.mainModelView.selectedCategoryId = self.mainModelView.arrayData[SelectedIndexInCollection]["id"].stringValue
            return vc
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt)
    {
        let indexpath = IndexPath(row: Int(index), section: 0)
        
        pastCollectionReload()
        selectedCollectionReload(index:Int(index))
        SelectedIndexInCollection = Int(index)
        print(SelectedIndexInCollection)
        theCurrentView.collectionheader.selectItem(at: indexpath, animated: false, scrollPosition: .centeredHorizontally)
        
    }
}

//MARK: - CollectionView DataSource

extension HomeVc:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mainModelView.arrayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeHeaderCollectionCell", for: indexPath) as! homeHeaderCollectionCell
        cell.tag = indexPath.row
        
        let dict = self.mainModelView.arrayData[indexPath.row]
        cell.btnTitle.setTitle(dict["name"].stringValue, for: .normal)
        cell.btnTitle.isSelected = dict["selected"].stringValue == "1" ? true : false
        
        if cell.btnTitle.isSelected{
            self.currentSelectedIndexInCollection = indexPath.row
        }
        
        cell.btnTitle.titleLabel?.font = cell.btnTitle.isSelected ? themeFont(size: 15, fontname: .bold) : themeFont(size: 13, fontname: .regular)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        SelectedIndexInCollection=indexPath.row
        print(SelectedIndexInCollection)
        carbonTabSwipeNavigation.currentTabIndex = UInt(indexPath.row)
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.mainModelView.arrayData[indexPath.row]["name"].stringValue.width(withConstrainedHeight: 40.0, font: themeFont(size: 15, fontname: .bold)) + 15
        return CGSize(width:width , height: 40)
        
    }
}

//MARK: - TextField Delegate

extension HomeVc:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let obj = homeStoryboard.instantiateViewController(withIdentifier: "SearchProductVC") as! SearchProductVC
        self.navigationController?.pushViewController(obj, animated: false)
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
}
//MARK: ApiSetUp
extension HomeVc {
    
    ///---- Get CategoryList
    func categoryListApi() {
        let parameters =   [String : Any]()
        
        mainModelView.CategoryListApi(param: parameters, success: { (categoryData) in
            if let data = categoryData.categorydata{
                self.mainModelView.arrayData = [JSON]()
                for (i, category) in data.enumerated(){
                    var dict = JSON()
                    dict["id"].intValue = category.id as! Int
                    dict["name"].stringValue = category.category ?? ""
                    if i == 0{
                        dict["selected"].stringValue = "1"
                    }else{
                        dict["selected"].stringValue = "0"
                    }
                    self.mainModelView.arrayData.append(dict)
                }
                //print(self.mainModelView.arrayData)
                 self.theCurrentView.collectionheader.reloadData()
                self.carbonSetupStyle()
               
            }
            
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    ///---- Get Notification Count
    
    func getNotificationCountApi()
    {
        let parameters = [String : Any]()
        print("parameters:", parameters)
        let url = String()
        
        mainModelView.notificationCountApi(urls: url, param: parameters, success: { (notificationCountData) in
            
         if notificationCountData.flag == 1{
             let badgeCnt:Int = Int(truncating: notificationCountData.logindata?.count ?? 0)
             Defaults.removeObject(forKey: "badgeCnt")
             Defaults.set(badgeCnt, forKey: "badgeCnt")
             UIApplication.shared.applicationIconBadgeNumber = badgeCnt
             if badgeCnt > 0{
                 self.tabBarController?.tabBar.items![3].badgeValue = "\(badgeCnt)"
             }else{
                 self.tabBarController?.tabBar.items![3].badgeValue = nil
             }
            }else{
                UIApplication.shared.applicationIconBadgeNumber = 0
                self.tabBarController?.tabBar.items![3].badgeValue = nil
            }
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                //makeToast(strMessage: error)
            }
        })
    }
    
}

