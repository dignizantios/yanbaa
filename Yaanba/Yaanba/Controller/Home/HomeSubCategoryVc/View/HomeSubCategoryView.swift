//
//  HomeSubCategoryView.swift
//  Yaanba
//
//  Created by Abhay on 08/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class HomeSubCategoryView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var lblExplore: UILabel!
    @IBOutlet weak var lblQuntity: UILabel!
    @IBOutlet weak var collectionSubCategory: UICollectionView!
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        lblExplore.text = "Explore_key".localized
        lblExplore.font = themeFont(size: 14, fontname: .regular)
        lblExplore.textColor = UIColor.appThemeDarkBlueColor
        
        lblQuntity.textColor = UIColor.white
        lblQuntity.backgroundColor = UIColor.appThemeGrayColor
        lblQuntity.font = themeFont(size: 9, fontname: .regular)
        lblQuntity.layer.cornerRadius = lblQuntity.bounds.height/2
        lblQuntity.layer.masksToBounds = true
        
        collectionSubCategory.register(UINib(nibName: "HomeProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeProductCollectionCell")

        
    }
    
}
