import Foundation 
import ObjectMapper 

class SubCategoryModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var subCategorydata: [SubCategorydata]? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		subCategorydata <- map["data"]
	}
} 

class SubCategorydata: Mappable { 

	var id: NSNumber? 
	var categoryId: NSNumber? 
	var categoryImage: String? 
	var subCategory: String?
    
    var subCategoryId: NSNumber = 0
    var subSubcategory: String = ""
    var status: String = ""
    var createdAt: String = ""
    var updatedAt: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		categoryId <- map["category_id"] 
		categoryImage <- map["category_image"] 
		subCategory <- map["sub_category"]
        
        subCategoryId <- map["sub_category_id"]
        subSubcategory <- map["sub_subcategory"]
        status <- map["status"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]

	}
} 

