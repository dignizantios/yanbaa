//
//  AddCommentView.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class AddCommentView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var lblAddComment: UILabel!
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var lblEnterDescription: UILabel!
    @IBOutlet weak var txtPrice: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    func setupUI(theController:AddCommentVc){
        
        lblAddComment.text = "Add_Comment_key".localized
        lblAddComment.textColor = UIColor.appThemeDarkBlueColor
        lblAddComment.font = themeFont(size: 15, fontname: .semibold)
        
        lblEnterDescription.textColor = UIColor.white
        lblEnterDescription.font = themeFont(size: 13, fontname: .regular)
        lblEnterDescription.text = "Enter_Description_key".localized
        
        txtPrice.placeHolderColor = UIColor.white
        txtPrice.font = themeFont(size: 13, fontname: .regular)
        txtPrice.delegate = theController
        txtPrice.tintColor = UIColor.white
        
        txtComment.font = themeFont(size: 13, fontname: .regular)
        txtComment.delegate = theController

        btnSubmit.setupThemeButtonUI()
        btnSubmit.setTitle("SUBMIT_key".localized, for: .normal)
        
    }
}
