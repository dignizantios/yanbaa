//
//  DiscountDetailsViewModel.swift
//  Yaanba
//
//  Created by Abhay on 05/03/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
class DiscountDetailsViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:DiscountDetailsVC!
    
    init(theController:DiscountDetailsVC) {
        self.theController = theController
    }
    var discountId = Int()
    var discountDetaildata: DiscountDetaildata? 
}
//MARK: Api setup
extension DiscountDetailsViewModel {
    
    func discountDetailApi(param:[String:Any],success:@escaping(_ token : DiscountDetailsModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aDiscountDetail
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let SubCategoryData = DiscountDetailsModel(JSON: data) {
                             success(SubCategoryData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
   
}

