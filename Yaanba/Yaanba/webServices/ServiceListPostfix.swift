//
//  ServiceListPostfix.swift
//  Hand2Home
//
//  Created by ABHAY on 03/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation

//let aBaseURL = "http://52.66.128.99/yanbaa/public/api/"    ///Test
let aBaseURL = "http://yenbaa.com/yanbaa/public/api/"   //Live

let aGenerateAccessToken = "generate_access_token"
let aLogin = "login"
let aLoginWithMobile = "loginWithMobile"
let aCheckOnlyMobile = "CheckMobile"
let aRegister = "register"
let aCheckMobile = "check-mobile"
let aLogout = "logout"
let aRefreshToken = "refresh"
let aForgotPwd = "forgot-password"
let aUserProfile = "user-detail"
let aUpdateProfile = "update-profile"
let aChnagePassword = "change-password"

let aHomeCategory = "category"
let aSubCategory = "sub-category"
let sub_subcategoryURL = "sub-subcategory"
let aAddProduct = "add-product"
let aHomeSubCategory = "home"
let aProductList = "product-list"
let aProductDetails = "product-detail"
let aAddComment = "add-comment"
let aAddReview = "review"
let aAddReport = "report"
let aMyProducts = "myproduct-list"
let aAddRemoveWishList = "add-wishlist"
let aWishList = "wish-list"
let aTourList = "homepage"
let aBidNow = "bid-now"
let aBidList = "bid-list"
let aNotificationList = "notification-list"
let aNotificationCount = "notification-count"
let aSearchProduct = "search"
let aDiscountDetail = "get-discount"
let aGetDeals = "getDeal"
