//
//  CommentTblCell.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class CommentTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCommentValue: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblName.textColor = UIColor.appThemeDarkBlueColor
        lblName.font = themeFont(size: 14, fontname: .regular)
        
        lblDate.textColor = UIColor.appThemeGrayColor
        lblDate.font = themeFont(size: 9, fontname: .regular)
        
        lblCommentValue.textColor = UIColor.appThemeGrayColor
        lblCommentValue.font = themeFont(size: 13, fontname: .regular)
        
        lblPrice.textColor = UIColor.black
        lblPrice.font = themeFont(size: 12, fontname: .regular)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
