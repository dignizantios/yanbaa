//
//  ForgotPasswordViewModel.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class ForgotPasswordViewModel {
    
    fileprivate weak var theController:ForgotPasswordVc!
    
    init(theController:ForgotPasswordVc) {
        self.theController = theController
    }
    
}
//MARK: Api setup
extension ForgotPasswordViewModel {
    
    func ValidateDetails() {
        
         if (self.theController.txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Enter_EmailId_Key".localized)
        }
         else if (self.theController!.txtEmail.text!.isValidEmail() == false)  {
            
            makeToast(strMessage: "Please_Enter_Valid_EmailId_Key".localized)
        }
        else {
            
            self.theController.forgotApi()
            
        }
    }
    
    func forgotPwdApi(param:[String:Any],success:@escaping(_ token : LogoutModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aForgotPwd
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LogoutModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}

