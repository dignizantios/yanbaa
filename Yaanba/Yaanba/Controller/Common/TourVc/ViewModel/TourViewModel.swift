//
//  TourViewModel.swift
//  Yaanba
//
//  Created by Abhay on 22/02/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class TourViewModel{
    
    //MARK: - View life cycle
    fileprivate weak var theController:ViewController!
    
    init(theController:ViewController) {
        self.theController = theController
    }
    var tourData = [Tourdata]()
}
//MARK: Api setup
extension TourViewModel {
    
    func getTourApi(param:[String:Any],success:@escaping(_ token : TourModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aTourList
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakeGetAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let TourData = TourModel(JSON: data) {
                             success(TourData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
   
}
