//
//  NotificationView.swift
//  Yaanba
//
//  Created by Abhay on 06/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class NotificationView: UIView {

    //MARK: - Outlet

    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet var vwIndicatorHeight: NSLayoutConstraint!
    @IBOutlet var indicater: UIActivityIndicatorView!
    
    func setupUI(){
        
        tblNotification.register(UINib(nibName: "NotificationTblCell", bundle: nil), forCellReuseIdentifier: "NotificationTblCell")
        tblNotification.tableFooterView = UIView()
        
    }

}
