//
//  CountryCodeVC.swift
//  Berry
//
//  Created by Haresh Bhai on 24/01/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol CountryCodeDelegate: class {
    func CountryCodeDidFinish(data : JSON)
}

class CountryCodeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var countryList : JSON = JSON()
    var filteredCountryList : JSON = JSON()
    var isFiltered: Bool = false
    weak var delegate: CountryCodeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.getCode()
        self.txtSearch.delegate = self
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissScreen))
//        view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }
    
    @objc func dismissScreen() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func getCode() {
        
        if let path = Bundle.main.path(forResource: "CountryCodes", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                print(JSON(jsonResult))
                countryList = JSON(jsonResult)
                self.tableView.reloadData()
            } catch {
                // handle error
                print(error.localizedDescription)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            print(updatedText)
            if updatedText == "" {
                isFiltered = false
                self.tableView.reloadData()
            }
            else {
                isFiltered = true
                let jobj = countryList.arrayValue
                if !jobj.isEmpty {
                    let j = jobj.filter({ (json) -> Bool in
                        return json["name"].stringValue.lowercased().contains(updatedText.lowercased()) || json["dial_code"].stringValue.lowercased().contains(updatedText.lowercased()) ||
                        json["code"].stringValue.lowercased().contains(updatedText.lowercased())
                    })
                    self.filteredCountryList.arrayObject = j
                    self.tableView.reloadData()
                }
            }
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(countryList.count)
        return isFiltered ? self.filteredCountryList.count : countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CountryCodeCell = self.tableView.dequeueReusableCell(withIdentifier: "CountryCodeCell") as! CountryCodeCell
        cell.selectionStyle = .none
        let data = isFiltered ? self.filteredCountryList[indexPath.row] : self.countryList[indexPath.row]
        cell.imgCountry.image = UIImage(named: data["code"].string ?? "")
        cell.lblCode.text = data["dial_code"].string ?? ""
        let str1 = (data["name"].string ?? "") + " ("
        let str2 = (data["code"].string ?? "") + ")"
        cell.lblCountry.text =  str1 + str2
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = isFiltered ? self.filteredCountryList[indexPath.row] : self.countryList[indexPath.row]
        self.delegate?.CountryCodeDidFinish(data: data)
        self.dismiss(animated: false, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
