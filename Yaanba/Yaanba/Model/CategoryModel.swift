import Foundation 
import ObjectMapper 

class CategoryModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var categorydata: [Categorydata]? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		categorydata <- map["data"]
	}
} 

class Categorydata: Mappable { 

	var id: NSNumber? 
	var category: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		category <- map["category"] 
	}
} 

