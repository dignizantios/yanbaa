//
//  ProductTblCell.swift
//  Yaanba
//
//  Created by Abhay on 08/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class ProductTblCell: UITableViewCell {

    //MARK:- Outlet
    
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        vwMain.backgroundColor = UIColor(red: 189/255, green: 196/255, blue: 204/255, alpha: 0.8)
        
        imgProduct.layer.cornerRadius = 8
        imgProduct.layer.masksToBounds = true
        
        [lblPrice,lblProductName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkBlueColor
            lbl?.font = themeFont(size: 13, fontname: .bold)
        }
        
        lblDiscount.textColor = UIColor.appThemeDarkBlueColor
        lblDiscount.layer.cornerRadius = lblDiscount.layer.bounds.height/2
        lblDiscount.layer.masksToBounds = true
        lblDiscount.font = themeFont(size: 9, fontname: .regular)
        lblDiscount.backgroundColor = UIColor(red: 189/255, green: 196/255, blue: 204/255, alpha: 1.0)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(dict:WishlistproductData){
        if dict.productImage !=  nil {
            imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgProduct.sd_setImage(with: dict.productImage?.toURL(), placeholderImage: UIImage(named: "ic_home_futter_splash_holder"), options: .lowPriority, context: nil)
        } else {
            imgProduct.image = UIImage(named: "ic_home_futter_splash_holder")
        }
        
        lblProductName.text = dict.productTitle ?? ""
       // lblPrice.text = "\("KWD".localized) \(dict.price ?? 0)"
        if dict.priceBidBarg?.lowercased() == "bid".lowercased(){
            lblPrice.text = "\("KWD".localized.uppercased()) \(dict.basicPrice ?? 0) "
        }else{
            lblPrice.text = "\("KWD".localized.uppercased()) \(dict.price ?? 0) "
        }
       // lblDiscount.text = ""
    }
    
}
