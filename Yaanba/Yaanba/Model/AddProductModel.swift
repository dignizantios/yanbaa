import Foundation 
import ObjectMapper 

class AddProductModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var addProductdata: AddProductdata? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		addProductdata <- map["data"]
	}
} 

class AddProductdata: Mappable { 

	var productType: String? 
	var categoryId: String? 
	var subCategoryId: String? 
	var productTitle: String? 
	var description: String? 
	var price: String? 
	var alternativeMobileNo: String? 
	var priceBidBarg: String? 
	var userId: NSNumber? 
	var updatedAt: String? 
	var createdAt: String? 
	var id: NSNumber? 
	var productImage: String? 
	var mostPopular: NSNumber? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		productType <- map["product_type"] 
		categoryId <- map["category_id"] 
		subCategoryId <- map["sub_category_id"] 
		productTitle <- map["product_title"] 
		description <- map["description"] 
		price <- map["price"] 
		alternativeMobileNo <- map["alternative_mobile_no"] 
		priceBidBarg <- map["price_bid_barg"] 
		userId <- map["user_id"] 
		updatedAt <- map["updated_at"] 
		createdAt <- map["created_at"] 
		id <- map["id"] 
		productImage <- map["product_image"] 
		mostPopular <- map["most_popular"] 
	}
} 

