//
//  SubCategoryVC.swift
//  Yaanba
//
//  Created by vishal on 27/11/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class SubCategoryVC: UIViewController {

    //MARK: Variables
    lazy var mainView: SubCategoryView = {
        return self.view as! SubCategoryView
    }()
    
    lazy var mainVM: SubCategoryVM = {
        return SubCategoryVM(theController: self)
    }()

    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        getSubCategoryData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationbarBackButton(titleText: self.mainVM.subCategoryName.uppercased())
    }
}

//MARK:- setup
extension SubCategoryVC {
    
}

//MARK:- ApiSetUp
extension SubCategoryVC {
    
    func getSubCategoryData() {
        
        let param = [APIKey.key_sub_category_id : self.mainVM.subCategoryId]
        self.mainVM.SubCategoryApi(param: param) {
            self.mainView.collectionSubCategory.reloadData()
        }
        failed: { error in
            print("Error:- \(error)")
        }
    }
}
