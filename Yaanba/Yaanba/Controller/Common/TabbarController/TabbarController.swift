//
//  TabbarController.swift
//  Yaanba
//
//  Created by YASH on 21/12/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class TabbarController: UITabBarController,UITabBarControllerDelegate {
    
    //MARK: - Variable
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self

        self.tabBar.tintColor = UIColor.appThemeYellowColor
        self.tabBar.unselectedItemTintColor = UIColor.white
        
        let home = homeStoryboard.instantiateViewController(withIdentifier: "HomeVc") as! HomeVc
        let tabbarOneItem = UITabBarItem(title: "HOME_key".localized, image: UIImage(named:"ic_unselect_home")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_select_home")?.withRenderingMode(.alwaysOriginal))
        home.tabBarItem = tabbarOneItem
        
        let deals = dealsStoryboard.instantiateViewController(withIdentifier: "DealsVc") as! DealsVc
        let tabbarTwoItem = UITabBarItem(title: "DEALS_key".localized, image: UIImage(named:"ic_unselect_discount")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_select_discount")?.withRenderingMode(.alwaysOriginal))
        deals.tabBarItem = tabbarTwoItem
        
        let addNew = addNewStoryboard.instantiateViewController(withIdentifier: "AddNewVc") as! AddNewVc
        let tabbarThreeItem = UITabBarItem(title: "ADD_NEW_key".localized, image: UIImage(named:"ic_add_new")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_add_new")?.withRenderingMode(.alwaysOriginal))
        addNew.tabBarItem = tabbarThreeItem
        
        let notification = notificationStoryboard.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationVc
        let tabbarFourItem = UITabBarItem(title: "NOTIFICATION_key".localized, image: UIImage(named:"ic_unselect_notification")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_select_notification")?.withRenderingMode(.alwaysOriginal))
        notification.tabBarItem = tabbarFourItem
        
        let profile = profileStoryboard.instantiateViewController(withIdentifier: "ProfileVc") as! ProfileVc
        let tabbarFiveItem = UITabBarItem(title: "PROFILE_key".localized, image: UIImage(named:"ic_unselect_profile")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_select_profile")?.withRenderingMode(.alwaysOriginal))
        profile.tabBarItem = tabbarFiveItem
        
        let n1 = UINavigationController(rootViewController:home)
        let n2 = UINavigationController(rootViewController:deals)
        let n3 = UINavigationController(rootViewController:addNew)
        let n4 = UINavigationController(rootViewController:notification)
        let n5 = UINavigationController(rootViewController:profile)
        
        n1.isNavigationBarHidden = false
        n2.isNavigationBarHidden = false
        n3.isNavigationBarHidden = false
        n4.isNavigationBarHidden = false
        n5.isNavigationBarHidden = false
        
        self.viewControllers = [n1,n2,n3,n4,n5]
        self.tabBar.barTintColor = UIColor.appThemeDarkBlueColor
        self.tabBar.isTranslucent = false
        isNotSetupTabBar = false

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let navigationController = viewController as? UINavigationController
        navigationController?.popToRootViewController(animated: true)
        print(tabBarController.selectedIndex)
        if tabBarController.selectedIndex == 2{
            guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
                withoutLoginPopup()
                return
            }
        }
    }
    
}

