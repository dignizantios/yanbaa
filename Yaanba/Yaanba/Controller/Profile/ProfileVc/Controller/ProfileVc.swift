//
//  ProfileVc.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileVc: UIViewController {
    
    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:ProfileView = { [unowned self] in
        return self.view as! ProfileView
    }()
    lazy var mainModelView: ProfileViewModel = {
        return ProfileViewModel(theController: self)
    }()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI()
        NotificationCenter.default.addObserver(self, selector: #selector(self.movetoProductDetails(notification:)), name: NSNotification.Name(rawValue: "isBidOnMyProduct"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarCenterText(titleText: "PROFILE_key".localized)
        
        let rightButton = UIBarButtonItem(title: "WISHLIST_key".localized, style: .plain, target: self, action: #selector(wishListTapped))
        rightButton.tintColor = .appThemeDarkBlueColor
        rightButton.setTitleTextAttributes([NSAttributedString.Key.font:themeFont(size: 12, fontname: .bold)], for: .normal)
        rightButton.setTitleTextAttributes([NSAttributedString.Key.font:themeFont(size: 12, fontname: .bold)], for: .selected)
        self.navigationItem.rightBarButtonItem = rightButton
        
        theCurrentView.collectionProduct.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        if pushData["productId"].stringValue != "" {
            movetoProductDetail()
        }
       ProfileApi()
       getMyProductApi()

    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        theCurrentView.collectionProduct.removeObserver(self, forKeyPath: "contentSize")
        
    }

    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UICollectionView {
            theCurrentView.constantCollectionViewHeight.constant = theCurrentView.collectionProduct.contentSize.height
        }
    }
    @objc func movetoProductDetails(notification: NSNotification) {
        if pushData["productId"].stringValue != "" {
            movetoProductDetail()
        }
    }
    func movetoProductDetail()
    {
        let obj = profileStoryboard.instantiateViewController(withIdentifier: "BidListVc") as! BidListVc
        obj.mainModelView.productId = pushData["productId"].stringValue
        obj.mainModelView.productProfile = pushData["product_image"].stringValue
        obj.mainModelView.productName = pushData["product_title"].stringValue
       // obj.mainModelView.productDetails = dict. ?? ""
        self.navigationController?.pushViewController(obj, animated: true)
    }
   
}

//MARK: - IBAction method
extension ProfileVc{
    
    @objc func wishListTapped(){
        let obj = profileStoryboard.instantiateViewController(withIdentifier: "WishListVc") as! WishListVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnUpdateProfileTapped(_ sender: CustomButton) {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            withoutLoginPopup()
            return
        }
        let obj = profileStoryboard.instantiateViewController(withIdentifier: "EditProfileVc") as! EditProfileVc
        obj.mainModelView.profileData = self.mainModelView.profileData
        self.navigationController?.pushViewController(obj, animated: true)
    }
       
   @IBAction func btnLogoutTapped(_ sender: UIButton) {
       
    let alertController = UIAlertController(title: "", message: "Are_you_sure_you_want_to_logout?_key".localized, preferredStyle: UIAlertController.Style.alert)
       
    let okAction = UIAlertAction(title: "Yes_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
        self.logoutApi()
       }
    
    let cancelAction = UIAlertAction(title: "No_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
           print("Cancel")
       }
       alertController.addAction(okAction)
       alertController.addAction(cancelAction)
       self.present(alertController, animated: true, completion: nil)
       
   }
}

//MARK:- Collection View Delegate and DataSource
extension ProfileVc: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          self.mainModelView.productListdata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionCell
        let dict = self.mainModelView.productListdata[indexPath.row]
        cell.setupData(data: dict)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width)/2, height: (collectionView.frame.size.width)/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = self.mainModelView.productListdata[indexPath.row]
        if dict.priceBidBarg == nil && dict.productType?.lowercased() == "service".localized.lowercased(){
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "ServiceDetailVc") as! ServiceDetailVc
            obj.mainModelView.productId = "\(self.mainModelView.productListdata[indexPath.row].id ?? 0)"
            self.navigationController?.pushViewController(obj, animated: true)
        }else if dict.productType?.lowercased() == "product".lowercased() && dict.priceBidBarg?.lowercased() == "bid".lowercased(){
            let obj = profileStoryboard.instantiateViewController(withIdentifier: "BidListVc") as! BidListVc
            obj.mainModelView.productId = dict.id?.stringValue ?? ""
            obj.mainModelView.productProfile = dict.productImage ?? ""
            obj.mainModelView.productName = dict.productTitle ?? ""
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            
             print(dict.priceBidBarg)
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "MainProductDetailsVc") as! MainProductDetailsVc
            obj.mainModelView.productId = "\(dict.id ?? 0)"
            if dict.priceBidBarg?.lowercased() == "bid".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "price".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "bargeding".lowercased(){
                obj.selectedController = .multipleSegment
            }else{
               obj.selectedController = .singleSegment
            }
            obj.mainModelView.productDetailsdata?.priceBidBarg = dict.priceBidBarg ?? ""
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
}
//MARK: ApiSetUp
extension ProfileVc {
    
    ///---- Profile
    func ProfileApi() {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            withoutLoginPopup()
            return
        }
        let parameters =   [String : Any]()
        
        mainModelView.profileApi(param: parameters, success: { (loginData) in
            self.mainModelView.profileData = loginData.logindata
            self.theCurrentView.lblName.text = loginData.logindata?.name ?? ""
            
            if loginData.logindata?.ProfileImage !=  "" {
                self.theCurrentView.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                self.theCurrentView.imgProfile.sd_setImage(with: loginData.logindata?.ProfileImage?.toURL(), placeholderImage: UIImage(named: "ic_profile_splash_holder"), options: .lowPriority, context: nil)
            } else {
                self.theCurrentView.imgProfile.image = UIImage(named: "ic_profile_splash_holder")
            }
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
   ///---- MyProduct Get
    func getMyProductApi() {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            //withoutLoginPopup()
            return
        }
        let parameters =   [String : Any]()
        
        mainModelView.myProductListApi(param: parameters, success: { (productData) in
            if productData.flag == 1 {
                self.mainModelView.productListdata = productData.productListdata ??  [ProductListdata]()
                self.theCurrentView.collectionProduct.reloadData()
            }
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    ///---- Logout
    @objc func logoutApi() {
        let parameters =   [String : Any]()
        
        mainModelView.logoutApi(param: parameters, success: { (loginData) in
           // Defaults.removeObject(forKey: remember_email_key)
           // Defaults.removeObject(forKey: remember_password_key)
            Defaults.removeObject(forKey: remember_accesstoken_key)
            Defaults.removeObject(forKey: "idToken")
            Defaults.removeObject(forKey: "firebase_verification")
            Defaults.removeObject(forKey: "badgeCnt")
            Defaults.synchronize()
            UIApplication.shared.applicationIconBadgeNumber = 0
            appDelegate.objCustomTabBar.tabBar.items![3].badgeValue = nil
             let vc  = mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
             let rearNavigation = UINavigationController(rootViewController: vc)
             appDelegate.window?.rootViewController = rearNavigation
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    
    
}

