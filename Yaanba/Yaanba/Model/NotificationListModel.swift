import Foundation 
import ObjectMapper 

class NotificationListModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var notificationListdata: NotificationListdata? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		notificationListdata <- map["data"]
	}
} 

class NotificationListdata: Mappable { 

	var currentPage: NSNumber? 
	var notificationdata: [Notificationdata]? 
	var from: NSNumber? 
	var lastPage: NSNumber? 
	var nextPageUrl: String? 
	var path: String? 
	var perPage: NSNumber? 
	var prevPageUrl: String?
	var to: NSNumber? 
	var total: Int?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		currentPage <- map["current_page"] 
		notificationdata <- map["data"]
		from <- map["from"] 
		lastPage <- map["last_page"] 
		nextPageUrl <- map["next_page_url"] 
		path <- map["path"] 
		perPage <- map["per_page"] 
		prevPageUrl <- map["prev_page_url"] 
		to <- map["to"] 
		total <- map["total"] 
	}
} 

class Notificationdata: Mappable { 

	var notificationType: NSNumber? 
	var isShow: NSNumber? 
	var isRead: NSNumber? 
	var createdAt: String? 
	var name: String? 
	var profileImage: String? 
	var message: String?
    var productId: NSNumber?
    var productType: String?
    var priceBidBarg: String?
    var userId: String?
    var productImage: String?
    var productTitle: String?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		notificationType <- map["notification_type"] 
		isShow <- map["is_show"] 
		isRead <- map["is_read"] 
		createdAt <- map["created_at"] 
		name <- map["name"] 
		profileImage <- map["profile_image"] 
		message <- map["message"]
        productId <- map["productId"]
        productType <- map["product_type"]
        priceBidBarg <- map["price_bid_barg"]
        userId <- map["user_id"]
        productImage <- map["product_image"]
        productTitle <- map["product_title"]
	}
} 

