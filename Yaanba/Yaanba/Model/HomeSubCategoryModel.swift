import Foundation 
import ObjectMapper 

class HomeSubCategoryModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var homeSubCategorydata: HomeSubCategorydata? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		homeSubCategorydata <- map["data"]
	}
} 

class HomeSubCategorydata: Mappable { 

	var subCategory: [SubCategory]? 
	var mostPopular: [MostPopular]? 
    var discount: [Discount]?
    
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		subCategory <- map["sub_category"] 
		mostPopular <- map["most_popular"]
        discount <- map["discount"]
	}
} 
class Discount: Mappable {

    var id: NSNumber?
    var discount: NSNumber?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        discount <- map["discount"]
    }
}
class MostPopular: Mappable { 

	var id: NSNumber? 
	var productTitle: String? 
	var price: NSNumber? 
	var priceBidBarg: String? 
	var productImage: String? 
	var mostPopular: NSNumber?
    var basicPrice: NSNumber?
    var stepPrice: NSNumber?
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		productTitle <- map["product_title"] 
		price <- map["price"] 
		priceBidBarg <- map["price_bid_barg"] 
		productImage <- map["product_image"] 
		mostPopular <- map["most_popular"]
        basicPrice <- map["basic_price"]
        stepPrice <- map["step_price"]
	}
} 

class SubCategory: Mappable { 

	var id: NSNumber? 
	var categoryId: NSNumber? 
	var categoryImage: String? 
	var subCategory: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		categoryId <- map["category_id"] 
		categoryImage <- map["category_image"] 
		subCategory <- map["sub_category"] 
	}
} 

