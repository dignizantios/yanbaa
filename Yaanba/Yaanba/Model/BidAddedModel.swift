import Foundation 
import ObjectMapper 

class BidAddedModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var bidAddeddata: BidAddeddata? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		bidAddeddata <- map["data"]
	}
} 

class BidAddeddata: Mappable { 

	var productId: String? 
	var bid: String? 
	var bidUserId: NSNumber? 
	var createdAt: String? 
	var id: NSNumber? 
	var biduserData: BiduserData? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		productId <- map["product_id"] 
		bid <- map["bid"] 
		bidUserId <- map["bid_user_id"] 
		createdAt <- map["created_at"] 
		id <- map["id"] 
		biduserData <- map["user_data"]
	}
} 

class BiduserData: Mappable { 

	var id: NSNumber? 
	var name: String? 
	var mobileNo: String? 
	var profileImage: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		name <- map["name"] 
		mobileNo <- map["mobile_no"] 
		profileImage <- map["profile_image"] 
	}
} 

