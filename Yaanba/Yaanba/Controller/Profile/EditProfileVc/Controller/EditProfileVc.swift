//
//  EditProfileVc.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import TOCropViewController
import SDWebImage

class EditProfileVc: UIViewController {

    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:EditProfileView = { [unowned self] in
        return self.view as! EditProfileView
    }()
    lazy var mainModelView: EditProfileViewModel = {
        return EditProfileViewModel(theController: self)
    }()
    
    private var customImagePicker = CustomImagePicker()
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI()
        // Do any additional setup after loading the view.
        setupProfileData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "UPDATE_PROFILE_key".localized.capitalized)
        
    }
    
}

//MARK: - IBAction method

extension EditProfileVc{
    
    @IBAction func btnProfileTaped(_ sender: UIButton) {
        self.view.endEditing(true)
        selectImages()
    }
    
    @IBAction func btnUpdateProfileTapped(_ sender: CustomButton) {
        mainModelView.ValidateDetails()
    }
    
    @IBAction func btnChangePasswordTapped(_ sender: CustomButton) {
        
        let obj = profileStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVc") as! ChangePasswordVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}


//MARK: - Image selection

extension EditProfileVc{
    
    func selectImages()
    {
        customImagePicker.typeOfPicker = .onlyPhoto
        customImagePicker.showImagePicker(fromViewController: self,
                                          navigationColor: UIColor.appThemeYellowColor,
                                          imagePicked: { (response) in
                                            
                                            let theImage = response[UIImagePickerController.InfoKey.originalImage] as! UIImage
                                            
                                            self.openCropVC(Image: theImage)
                                                                                        
                                            
        }, imageCanceled: {
        }, imageRemoved: nil)
    }

}

//MARK: - PhotoGallary

extension EditProfileVc:TOCropViewControllerDelegate
{
    func openCropVC(Image:UIImage)
    {
        let cropVC = TOCropViewController(image: Image)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        
        self.present(cropVC, animated: false, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        // self.imgUserPro.image = image
        
        theCurrentView.imgProfile.image = image
        mainModelView.userProfile = image
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        // isHaveImage = false
        dismiss(animated: true, completion: nil)
    }
}
//MARK: ApiSetUp
extension EditProfileVc {
    
    ///---- Profile
    func setupProfileData() {
        self.theCurrentView.txtUserName.text = self.mainModelView.profileData?.name ?? ""
        self.theCurrentView.txtEmailID.text = self.mainModelView.profileData?.email ?? ""
        self.theCurrentView.txtMobileNumber.text = "+\(self.mainModelView.profileData?.countryCode ?? 0) \(self.mainModelView.profileData?.mobileNo ?? "")"
        if self.mainModelView.profileData?.ProfileImage !=  "" {
            self.theCurrentView.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.theCurrentView.imgProfile.sd_setImage(with: self.mainModelView.profileData?.ProfileImage?.toURL(), placeholderImage: UIImage(named: "ic_profile_splash_holder"), options: .lowPriority, context: nil)
            self.mainModelView.userProfile = self.theCurrentView.imgProfile.image
        } else {
            self.theCurrentView.imgProfile.image = UIImage(named: "ic_profile_splash_holder")
            
        }
    }
    
    ///---- Update Profile
    func updateProfileApi() {
        let parameters : [String : Any] =   [ APIKey.key_email:self.theCurrentView.txtEmailID.text!,
            APIKey.key_name:self.theCurrentView.txtUserName.text!,
            APIKey.key_mobile_no:self.theCurrentView.txtMobileNumber.text!,
        ]
        print("parameters:", parameters)
        
        var dataArray = [Data]()
        var nameArray = [String]()
        var fileNameArray = [String]()
        let data = theCurrentView.imgProfile.image!.sd_imageData(as: .JPEG, compressionQuality: 0.4)
        dataArray.append(data!)
        nameArray.append("profile_image")
        fileNameArray.append("\(UUID().uuidString).png")
        
            
        mainModelView.updateProfileApi(param: parameters, image: dataArray, NameArray: nameArray, FileNameArray: fileNameArray, success: { (registerModel) in
            self.navigationController?.popViewController(animated: true)
            makeToast(strMessage: registerModel.msg ?? "")
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
}

