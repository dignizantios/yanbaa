//
//  OfferCollectioncell.swift
//  Yaanba
//
//  Created by Abhay on 07/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class OfferCollectioncell: UICollectionViewCell {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblExtra: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!

    //MARK: - View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblExtra,lblDiscount].forEach { (lbl) in
            lbl?.textColor = UIColor.white
        }
        
        lblExtra.font = themeFont(size: 14, fontname: .semibold)
        lblDiscount.font = themeFont(size: 16, fontname: .bold)
        
        // Initialization code
    }

}
