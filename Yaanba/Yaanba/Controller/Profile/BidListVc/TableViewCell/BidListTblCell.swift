//
//  BidListTblCell.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class BidListTblCell: UITableViewCell {

    //MARK: - Variable
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblName.textColor = UIColor.appThemeGrayColor
        lblName.font = themeFont(size: 15, fontname: .regular)
        
        lblPrice.textColor = UIColor.black
        lblPrice.font = themeFont(size: 15, fontname: .regular)
        
        if DeviceLanguage == "ar"{
            lblPrice.textAlignment = .left
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
