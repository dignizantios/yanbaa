import Foundation 
import ObjectMapper 

class SearchListModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var searchdata: [Searchdata]? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		searchdata <- map["data"]
	}
} 

class Searchdata: Mappable { 

	var id: NSNumber? 
	var productTitle: String? 
	var productImage: String? 
	var mostPopular: NSNumber?
    var productType: String?
    var priceBidBarg: String?
    

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		productTitle <- map["product_title"] 
		productImage <- map["product_image"] 
		mostPopular <- map["most_popular"]
        priceBidBarg <- map["price_bid_barg"]
        productType <- map["product_type"]
	}
} 

