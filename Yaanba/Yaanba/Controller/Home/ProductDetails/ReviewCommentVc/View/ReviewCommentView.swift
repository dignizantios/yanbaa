//
//  ReviewCommentView.swift
//  Yaanba
//
//  Created by Abhay on 11/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ReviewCommentView: UIView {

    @IBOutlet weak var tblReviewCommnet: UITableView!
    
    func setupUI(){
        
        tblReviewCommnet.register(UINib(nibName: "CommentTblCell", bundle: nil), forCellReuseIdentifier: "CommentTblCell")
        
        tblReviewCommnet.register(UINib(nibName: "ReviewTblCell", bundle: nil), forCellReuseIdentifier: "ReviewTblCell")
        
        tblReviewCommnet.tableFooterView = UIView()
        
    }
    
}
