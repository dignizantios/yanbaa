//
//  ReviewVc.swift
//  Yaanba
//
//  Created by Abhay on 18/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ReviewVc: UIViewController {

    
    //MARK: - Variable
    
    lazy var theCurrentView:ReviewView = { [unowned self] in
        return self.view as! ReviewView
    }()
    lazy var mainModelView: ReviewViewModel = {
        return ReviewViewModel(theController: self)
    }()
    //MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "Review_key".localized)
    }
    
}

//MARK: - IBAction method

extension ReviewVc{
    
    @IBAction func btnSubmitTapped(_ sender: CustomButton) {
        self.mainModelView.ValidateDetails()
        
    }
}
//MARK: ApiSetUp
extension ReviewVc {
    
    ///---- Add comment
    func addReviewApi() {
        
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            withoutLoginPopup()
            return
        }
        
        let parameters : [String : Any] =   [ APIKey.key_rate:self.theCurrentView.vwRatting.value,
           APIKey.key_description:self.theCurrentView.txtRattingDescription.text!,
           APIKey.key_product_id:self.mainModelView.productId
            
        ]
        print("parameters:", parameters)
        
        mainModelView.addReviewApi(param: parameters, success: { (addCoomentData) in
            
            if addCoomentData.flag == 1{
                self.mainModelView.ProductAdddelegate?.reloadAddProduct()
                self.navigationController?.popViewController(animated: true)
            }
            makeToast(strMessage: addCoomentData.msg ?? "")
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
}

