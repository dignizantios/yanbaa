//
//  AddCommentVc.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class AddCommentVc: UIViewController {

    //MARK: - Variable
    
     lazy var theCurrentView:AddCommentView = { [unowned self] in
        return self.view as! AddCommentView
    }()
    
    lazy var mainModelView: AddCommentViewModel = {
        return AddCommentViewModel(theController: self)
    }()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI(theController: self)
        // Do any additional setup after loading the view.
    }
    
}

//MARK: - IBAction

extension AddCommentVc{
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        self.mainModelView.ValidateDetails()
    }
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

//MARK: - TextView Delegate

extension AddCommentVc:UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        theCurrentView.lblEnterDescription.isHidden = (textView.text == "") ? false : true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK: - TextField Delegate

extension AddCommentVc:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
}
//MARK: ApiSetUp
extension AddCommentVc {
    
    ///---- Add comment
    func addCommentApi() {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            withoutLoginPopup()
            return
        }
        let parameters : [String : Any] =   [ APIKey.key_comment_price:self.theCurrentView.txtPrice.text!,
           APIKey.key_comment:self.theCurrentView.txtComment.text!,
           APIKey.key_user_id:Defaults.object(forKey: remember_userid_key) ?? "0",
           APIKey.key_product_id:self.mainModelView.productId
            
        ]
        print("parameters:", parameters)
        
        mainModelView.addCommnetApi(param: parameters, success: { (addCoomentData) in
            
            if addCoomentData.flag == 1{
                self.mainModelView.ProductAdddelegate?.reloadAddProduct()
                self.dismiss(animated: true, completion: nil)
            }
           // makeToast(strMessage: addCoomentData.msg ?? "")
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    
    
}

