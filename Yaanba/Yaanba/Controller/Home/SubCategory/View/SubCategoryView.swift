//
//  SubCategoryView.swift
//  Yaanba
//
//  Created by vishal on 27/11/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class SubCategoryView: UIView {
    
    //MARK:- Outlets
    @IBOutlet weak var collectionSubCategory: UICollectionView!
    
    func setUpUI()  {
        
        registerXib()
    }
    
    func registerXib() {
        
        collectionSubCategory.register(UINib(nibName: "HomeProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeProductCollectionCell")
    }
    
}
