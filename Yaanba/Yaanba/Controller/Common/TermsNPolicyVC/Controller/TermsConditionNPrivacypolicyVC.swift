//
//  TermsConditionNPrivacypolicyVC.swift
//  MusicTeachersHelper
//
//  Created by Abhay on 02/08/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import WebKit

class TermsConditionNPrivacypolicyVC: UIViewController {
    
    //#MARK:- outlets
    
    //#MARK:- Declaration
    var lblTitle = String()
    var tag = Int()
    var webView = WKWebView()
    //#MARK:- View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.hidesBackButton = true
        self.webView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.webView.backgroundColor = .clear
        self.view.addSubview(self.webView)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "Terms_of_use_key".localized)
        let url = URL(string: "http://52.66.128.99/yanbaa/public/terms_and_condition/en%7Car")
        let webRequest : NSURLRequest = NSURLRequest(url: url!)
        webView.load(webRequest as URLRequest)
        /*if tag == 1{
           
            let url = URL(string: "https://musicteachershelper.com/terms/")
            let webRequest : NSURLRequest = NSURLRequest(url: url!)
            webView.load(webRequest as URLRequest)
        }else if tag == 2{
            lblTitle = getCommonString(key: "Privacy_Policy_key")
            setUpNavigationBarBGTitleAndBack(strTitle: lblTitle)
            let url = URL(string: "https://musicteachershelper.com/your-privacy/")
            let webRequest : NSURLRequest = NSURLRequest(url: url!)
            webView.load(webRequest as URLRequest)
        }else{
            print("tag-->",tag)
        }*/
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
}
extension TermsConditionNPrivacypolicyVC: WKNavigationDelegate{
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print(#function)
        //if tag != 1 && tag != 2{
            hideLoader()
        //}
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print(#function)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(#function)
        //if tag != 1 && tag != 2{
            hideLoader()
       // }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print(#function)
       // if tag != 1 && tag != 2{
            showLoader()
       // }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(#function)
        //if tag != 1 && tag != 2{
            hideLoader()
        //}
    }
    
}
