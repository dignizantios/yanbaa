//
//  WebServices.swift
//  Taal
//
//  Created by Vishal on 09/12/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON
import SystemConfiguration

struct WebServices {
    
    func isNetworkReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canConnectWithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
    
    //MARK: - Post with body
    
    func makePostApiWithBody(name: String, params:[String:Any], completionHandler: @escaping(NSDictionary?, String?, Int?) -> Void) {
        
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
        header["accept"] = "application/json"
        if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
            header["Authorization"] = "Bearer \(auth)"
        }
        // header["Version"] = iosVersion
        header["X-localization"] = DeviceLanguage
        print(header)
        print("header:-\(header)")

        let url = aBaseURL + name
        print("url:-\(url)")
        print("Param:-\(JSON(params))")
        
        request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: header).responseJSON(completionHandler: { (response) in
            print("response:", response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
        })
    }
    
    //MARK: - Post
    func MakePostAPI(name:String, params:[String:Any], completionHandler: @escaping (Result<JSON>)-> Void)
    {
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
         header["accept"] = "application/json"
         if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
              header["Authorization"] = "Bearer \(auth)"
         }
        // header["Version"] = iosVersion
         header["X-localization"] = DeviceLanguage
        print("Authorization:-\(header)")
        
        let url = aBaseURL + name
        print("url:-\(url)")
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).validate().responseSwiftyJSON { (response) in
            print(response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
             completionHandler(response.result)
           /* switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }*/
            }.resume()
    }
    
    
    //MARK: - Get
    func MakeGetAPI(name:String, params:[String:Any], completionHandler: @escaping (Result<JSON>)-> Void){
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
         header["accept"] = "application/json"
         if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
              header["Authorization"] = "Bearer \(auth)"
         }
        // header["Version"] = iosVersion
         header["X-localization"] = DeviceLanguage
        
        let url = aBaseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        print("header:-\(header)")
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).validate().responseSwiftyJSON { (response) in
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            print("Response: ", response)
            completionHandler(response.result)
            /*switch response.result {
            case .success( _):
                if(response.error == nil) {
                    
                    if let _ = response.result.value as? Any {
                        completionHandler(response.result.value as? Any, nil, statusCode)
                    }
//                    else {
//                        completionHandler(response.result.value as? Any, nil, statusCode)
//                    }
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }*/
            }.resume()
    }
    func MakeGetWithOutBaseUrlAPI(name:String, params:[String:Any], completionHandler: @escaping (Result<JSON>)-> Void){
            
    //        guard ReachabilityTest.isConnectedToNetwork() else {
    //            makeToast(strMessage: "No internet connection available")
    //            return
    //        }
            checkAccessTokenAndUpdate()
            var header:[String:String] = [:]
             header["accept"] = "application/json"
             if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
                  header["Authorization"] = "Bearer \(auth)"
             }
            // header["Version"] = iosVersion
             header["X-localization"] = DeviceLanguage
            
            let url = name
            print("url:-\(url)")
            print("Json:-\(JSON(params))")
            print("header:-\(header)")
            
            Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).validate().responseSwiftyJSON { (response) in
                
                let statusCode = response.response?.statusCode
                print("statusCode:-\(statusCode ?? 0)")
                print("Response: ", response)
                completionHandler(response.result)
                }.resume()
        }
    func MakeGetAPIWithoutAuth(name:String, params:[String:Any], progress: Bool = true, completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        
       checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
         header["accept"] = "application/json"
         if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
              header["Authorization"] = "Bearer \(auth)"
         }
        // header["Version"] = iosVersion
         header["X-localization"] = DeviceLanguage
        print(header)
        
        let url = aBaseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).validate().responseJSON { (response) in
            print(response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
            }.resume()
    }
    
    //MARK: - Put
    func MakePutAPI(name:String, params:[String:Any], completionHandler: @escaping (Any?, String?, Int?)-> Void) {
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
         header["accept"] = "application/json"
         if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
              header["Authorization"] = "Bearer \(auth)"
         }
        // header["Version"] = iosVersion
         header["X-localization"] = DeviceLanguage
        print("header:",header)
        
        let url = aBaseURL + name
        print("url:-\(url)")
//        print("Params:-\(JSON(params))")
        
        Alamofire.request(url, method: .put, parameters: params, encoding: URLEncoding.default, headers: header).validate().responseJSON { (response) in
            print(response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? Any, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
            }.resume()
    }
    
    func MakePutJsonEncodingAPI(name:String, params:[String:Any], completionHandler: @escaping (Any?, String?, Int?)-> Void) {
            
    //        guard ReachabilityTest.isConnectedToNetwork() else {
    //            makeToast(strMessage: "No internet connection available")
    //            return
    //        }
            checkAccessTokenAndUpdate()
            var header:[String:String] = [:]
            header["accept"] = "application/json"
            if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
                header["Authorization"] = "Bearer \(auth)"
            }
            // header["Version"] = iosVersion
            header["X-localization"] = DeviceLanguage
            print(header)
            print("header:",header)
            
            let url = aBaseURL + name
            print("url:-\(url)")
//            print("Params:-\(JSON(params))")
            
            Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).validate().responseJSON { (response) in
                print(response)
                
                let statusCode = response.response?.statusCode
                print("statusCode:-\(statusCode ?? 0)")
                
                switch response.result {
                case .success( _):
                    if(response.error == nil) {
                        completionHandler(response.result.value as? Any, nil, statusCode)
                    }
                    else {
                        completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                    }
                case .failure( _):
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
                }.resume()
        }
    
    //MARK: PATCH
    func MakePatchAPI(name:String, params:[String:Any], completionHandler: @escaping (Any?, String?, Int?)-> Void) {
        
        //        guard ReachabilityTest.isConnectedToNetwork() else {
        //            makeToast(strMessage: "No internet connection available")
        //            return
        //        }
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
        header["accept"] = "application/json"
        if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
            header["Authorization"] = "Bearer \(auth)"
        }
        // header["Version"] = iosVersion
        header["X-localization"] = DeviceLanguage
        print(header)
        let url = aBaseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        print("header:-\(header)")
        
        Alamofire.request(url, method: .patch, parameters: params, encoding: URLEncoding.default, headers: header).validate().responseJSON { (response) in
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            print("Response: ", response)
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    
                    if let _ = response.result.value as? Any {
                        completionHandler(response.result.value as? Any, nil, statusCode)
                    }
                    //                    else {
                    //                        completionHandler(response.result.value as? Any, nil, statusCode)
                    //                    }
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
        }.resume()
    }
    
    
    func MakeDeleteAPI(name:String, params:[String:Any], completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
        header["accept"] = "application/json"
        if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
            header["Authorization"] = "Bearer \(auth)"
        }
        // header["Version"] = iosVersion
        header["X-localization"] = DeviceLanguage
        print(header)
        print("header: ",header)
        let url = aBaseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        
        Alamofire.request(url, method: .delete, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
            }.resume()
    }
    
    //MARK: - Post with Image upload
    func MakePostWithImageAPI(name:String, params:[String:Any], images:[UIImage], imageName:String = "photo", isAuth:Bool = true,completionHandler: @escaping (Result<JSON>)-> Void,failure: @escaping (Error)-> Void)
    {
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        
       // var headers:[String : String] = [:]
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
        if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
             header["Authorization"] = "Bearer \(auth)"
        }
        // header["Version"] = iosVersion
        header["Content-Type"] = "multipart/form-data; boundary=--------------------------503146432196697590930355"
        print("header:-",header)
        
        let url = aBaseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (i,image) in images.enumerated() {
                print("\(imageName)[\(i)]")
                print(image)
                multipartFormData.append(image.pngData()!, withName: "\(imageName)[\(i)]",fileName: "\(randomString(length: 5)).png", mimeType: "image/png")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method:.post,
           headers:header, encodingCompletion: { result in
             
            switch result {
            case .success(let upload, _, _):
                upload.responseSwiftyJSON(completionHandler: { (response) in
                    print(response)
                    completionHandler(response.result)
                    let statusCode = response.response?.statusCode
                    print("statusCode:-\(statusCode ?? 0)")
                    
                    if(response.error == nil) {
                       // completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                    }
                    else {
                      //  completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                    }
                })
            case .failure(let error):
                failure(error)
                hideLoader()
               // completionHandler(response.error)
                
               // completionHandler(nil, "Server_not_responding_please_try_again_key".localized, 500)
            }
        })
    }
    //currently use
    
    func postUsingMultiPartData(name:String ,parameter:[String:Any],files:[Data],withName:[String],withFileName:[String],mimeType:[String],completion:@escaping(JSON) -> Void) {
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
        if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
             header["Authorization"] = "Bearer \(auth)"
        }
        // header["Version"] = iosVersion
        header["Content-Type"] = "multipart/form-data; boundary=--------------------------503146432196697590930355"
        
        print("header:-",header)
        
        let url = aBaseURL + name
        
        var para = parameter
        /*para[APIKey.key_api_key] = api_value
        para[APIKey.key_device_type] = deviceType
        para[APIKey.key_device_id] = deviceID
        para[APIKey.key_register_id] = push_token
        para[APIKey.key_lang] = lang*/
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            var index = 0
            for data in files {
                multipartFormData.append(data, withName: withName[index], fileName: withFileName[index] , mimeType: mimeType[0])
                index += 1
            }
            print("multipartFormData:=",multipartFormData)
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: header) { (result) in
            print("header:=",header)
            print("url:=",url)
            print("parameter:=",para)
            
            switch result {
            case .success(let upload, _, _):
                print("statusCode:=",upload.response?.statusCode ?? "nil")
                upload.responseSwiftyJSON(completionHandler: { (response) in
                   // print(response)
                    //let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                    //print(datastring)
                    if let json = response.result.value {
                        print("json:=",json)
                        completion(json)
                    } else {
                        completion(JSON(["flag":"0","msg":"Something_went_wrong_key".localized]))
                    }//value["msg"].stringValue
                    //response.result.value["msg"].stringValue
                })
                break
                
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                var msg = "Something_went_wrong_key".localized
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    msg = "No_internet_connection_key".localized
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    msg = "Request_time_out_Please_try_again".localized
                }
                let result = JSON(["flag":"0","msg":msg])
                completion(result)
                
                break
            }
        }
    }
    func MakePutWithImageAPI(name:String, params:[String:Any], images:[UIImage], imageName:String = "photo", completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
        //        guard ReachabilityTest.isConnectedToNetwork() else {
        //            makeToast(strMessage: "No internet connection available")
        //            return
        //        }
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
        header["accept"] = "application/json"
        if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
            header["Authorization"] = "Bearer \(auth)"
        }
        // header["Version"] = iosVersion
        header["X-localization"] = DeviceLanguage
        
        print("header:-",header)
        
        let url = aBaseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for image in images {
                multipartFormData.append(image.jpeg(.medium)!, withName: imageName,fileName: "\(randomString(length: 5)).jpg", mimeType: "image/jpg")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method:.put,
           headers:header, encodingCompletion: { result in
            
            
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    
                    let statusCode = response.response?.statusCode
                    print("statusCode:-\(statusCode ?? 0)")
                    
                    if(response.error == nil) {
                        completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                    }
                    else {
                        completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                    }
                })
            case .failure( _):
                
                
                
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, 500)
            }
        })
    }
    
    //MARK: - Delete with body
    func makeDeleteApiWithBody(name: String, params:[String:Any], completionHandler: @escaping(NSDictionary?, String?, Int?) -> Void) {
        
        checkAccessTokenAndUpdate()
        var header:[String:String] = [:]
        header["accept"] = "application/json"
        if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
            header["Authorization"] = "Bearer \(auth)"
        }
        // header["Version"] = iosVersion
        header["X-localization"] = DeviceLanguage
        
        print("header:-",header)
        
        let url = aBaseURL + name
        print("url:-\(url)")
        print("Param:-\(JSON(params))")
        
        request(url, method: .delete, parameters: params, encoding: URLEncoding.httpBody , headers: header).responseJSON(completionHandler: { (response) in
            print("response:", response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
        })
    }
    //MARK:- Token Refresh
    func checkAccessTokenAndUpdate()
    {
        if let preDt = Defaults.object(forKey: token_expire_date_key) as? Date
        {
            
            //print(preDt.daysTo(Date()))
            if let diff = preDt.daysTo(Date()), diff >= expireTokenDaysHour
            {
                
                let (responesObj, response, error) = service_callPOSTJsonSync()
                if error != nil
                {
                    print(error?.localizedDescription ?? "")
                }
                else
                {
                    switch response?.getStatusCode() ?? 0 {
                    case 200:
                        print(responesObj as Any)
                        if let response_dict = responesObj
                        {
                            if response_dict["flag"] as? Int == 1{
                                Defaults.set(response_dict["access_token"], forKey: remember_accesstoken_key)
                                Defaults.set(Date(), forKey: token_expire_date_key)
                                Defaults.synchronize()
                            }else{
                                print("token refresh getting error")
                            }
                        }
                        break
                    default:
                        break
                    }
                }
                
            }
            else
            {
                print("NoNeedToRefresh")
            }
        }
    }
    func service_callPOSTJsonSync() -> (NSDictionary?, URLResponse?, Error?)
    {
        var dict_data: NSDictionary?
        var responsesup: URLResponse?
        var errorsup: Error?
        let semaphore = DispatchSemaphore(value: 0)
        
        let url = aRefreshToken
        var header:[String:String] = [:]
        header["accept"] = "application/json"
        if let auth = Defaults.object(forKey: remember_accesstoken_key) as? String{
            header["Authorization"] = "Bearer \(auth)"
        }
        // header["Version"] = iosVersion
        header["X-localization"] = DeviceLanguage
        let request = NSMutableURLRequest(url: URL(string: aBaseURL+url)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = header
        //request.httpBody = parameterData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            responsesup = response
            if (error != nil)
            {
                
                errorsup = error
                semaphore.signal()
            }
            else
            {
                do
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    
                    let jsonDictionary:NSDictionary;
                    jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    dict_data = jsonDictionary
                    semaphore.signal()
                }
                catch
                {
                    print(error.localizedDescription)
                    
                    errorsup = error
                    semaphore.signal()
                }
            }
        })
        dataTask.resume()
        _ = semaphore.wait(timeout: .distantFuture)
        return (dict_data, responsesup, errorsup)
    }
    
}
extension URLResponse {
    
    func getStatusCode() -> Int? {
        if let httpResponse = self as? HTTPURLResponse {
            return httpResponse.statusCode
        }
        return nil
    }
}
