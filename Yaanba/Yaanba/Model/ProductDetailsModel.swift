import Foundation 
import ObjectMapper 

class ProductDetailsModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var productdata: Productdata? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		productdata <- map["data"] 
	}
} 

class Productdata: Mappable { 

	var id: NSNumber? 
	var userId: NSNumber? 
	var productType: String? 
	var categoryId: NSNumber? 
	var subCategoryId: NSNumber? 
	var productTitle: String? 
	var description: String? 
	var price: NSNumber? 
	var alternativeMobileNo: String? 
	var priceBidBarg: String? 
	var basicPrice: NSNumber?
	var stepPrice: NSNumber?
	var startDate: String?
	var endDate: String?
	var bid: Bool?
	var status: String? 
	var createdAt: String? 
	var updatedAt: String? 
	var productReview: [ProductReview]? 
	var productComment: [ProductComment]? 
	var productImage: String?
    var productCall: String?
    var alternativeCall: String?
	var mostPopular: NSNumber? 
	var productMultiImage: [ProductMultiImage]?
    var isWishlist:Bool?
    var yourBid: NSNumber?
    var heighestBid: NSNumber?
    
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		userId <- map["user_id"] 
		productType <- map["product_type"] 
		categoryId <- map["category_id"] 
		subCategoryId <- map["sub_category_id"] 
		productTitle <- map["product_title"] 
		description <- map["description"] 
		price <- map["price"] 
		alternativeMobileNo <- map["alternative_mobile_no"] 
		priceBidBarg <- map["price_bid_barg"] 
		basicPrice <- map["basic_price"] 
		stepPrice <- map["step_price"] 
		startDate <- map["start_date"] 
		endDate <- map["end_date"] 
		bid <- map["bid"] 
		status <- map["status"] 
		createdAt <- map["created_at"] 
		updatedAt <- map["updated_at"] 
		productReview <- map["product_review"] 
		productComment <- map["product_comment"] 
		productImage <- map["product_image"] 
		mostPopular <- map["most_popular"] 
		productMultiImage <- map["product_multi_image"]
        productCall <- map["product_call"]
        isWishlist <- map["wishlist"]
        yourBid <- map["your_bid"]
        heighestBid <- map["heighest_bid"]
        alternativeCall <- map["alternative_mobile_no"]
        
	}
} 

class ProductMultiImage: Mappable {

	var id: NSNumber? 
	var productId: NSNumber? 
	var productImage: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		productId <- map["product_id"] 
		productImage <- map["product_image"] 
	}
} 

class ProductComment: Mappable { 

	var id: NSNumber? 
	var userId: NSNumber? 
	var productId: NSNumber? 
	var commentPrice: NSNumber? 
	var comment: String? 
	var createdAt: String? 
	var userName: String? 
    var userProfile: String?
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		userId <- map["user_id"] 
		productId <- map["product_id"] 
		commentPrice <- map["comment_price"] 
		comment <- map["comment"] 
		createdAt <- map["created_at"] 
		userName <- map["user_name"]
        userProfile <- map["user_image"]
	}
} 

class ProductReview: Mappable { 

	var id: NSNumber? 
	var userId: NSNumber? 
	var productId: NSNumber? 
	var rate: NSNumber? 
	var description: String? 
	var createdAt: String? 
	var userName: String?
    var userProfile: String?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		userId <- map["user_id"] 
		productId <- map["product_id"] 
		rate <- map["rate"] 
		description <- map["description"] 
		createdAt <- map["created_at"] 
		userName <- map["user_name"]
        userProfile <- map["user_image"]
	}
} 

