//
//  MainProductDetailsVc.swift
//  Yaanba
//
//  Created by Abhay on 11/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import MXScroll
import SwiftyJSON

enum checkSegmentType{
    case singleSegment
    case multipleSegment
}
protocol ReloadProductDelegate: class {
    func reloadProduct()
}

class MainProductDetailsVc: UIViewController {

    //MARK: - Variable
    
//    fileprivate lazy var theCurrentView:MainProductDetailView = { [unowned self] in
//        return self.view as! MainProductDetailView
//    }()

    @IBOutlet weak var contentView: UIView!
    lazy var mainModelView: MainProductDetailViewModel = {
           return MainProductDetailViewModel(theController: self)
    }()
    
    var btnFav = UIButton()
    var selectedController = checkSegmentType.multipleSegment
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupCaregotyDetails()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshProductDetails(notification:)), name: NSNotification.Name(rawValue: "refreshProductDetails"), object: nil)
        getProductDetailsApi()
    }

    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "Detail_View_key".localized)
       isProductDetailsOpen = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        isProductDetailsOpen = false
    }
    
    @objc func btnFavUnfavouriteTapped(){
       // btnFav.isSelected = !btnFav.isSelected
        self.addDeleteWishListApi(isSelected: !btnFav.isSelected)
    }
    @objc func refreshProductDetails(notification: NSNotification) {
      //  DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
        if pushData["productId"].stringValue != "" && self.mainModelView.productId == pushData["productId"].stringValue{
            pushData = JSON()
             getProductDetailsApi()
        }
          
       // }
        
    }
}

//MARK: - MXScroll Full setup

extension MainProductDetailsVc{
    
    func setupCaregotyDetails(){
        
        let objHeaderDetails = homeStoryboard.instantiateViewController(withIdentifier: "ProductUpperDetailsVc") as! ProductUpperDetailsVc
        objHeaderDetails.mainModelView.productId = "\(self.mainModelView.productDetailsdata?.id ?? 0)"
        objHeaderDetails.mainModelView.productDetailsdata = self.mainModelView.productDetailsdata
        objHeaderDetails.mainModelView.Productdelegate = self
        var headerTitle = [String]()
        var mx = MXViewController<MSSegmentControl>()
        print(selectedController)
        print(self.mainModelView.productDetailsdata?.priceBidBarg)
        if selectedController == .multipleSegment{
            objHeaderDetails.setCallNowHidden(hidden: true)
            objHeaderDetails.setBidNowHidden(hidden: true)
            objHeaderDetails.setBidCompareHidden(hidden: true)
            
            headerTitle = ["Comment_key".localized,"Review_key".localized]
            
            let comntvc = homeStoryboard.instantiateViewController(withIdentifier: "ReviewCommentVc") as! ReviewCommentVc
            comntvc.mainModelView.productDetailsdata = self.mainModelView.productDetailsdata
            comntvc.mainSegmentController = .multipleSegment
            comntvc.selectedSegmetController = .comment
            comntvc.mainModelView.Productdelegate = self
            comntvc.mainModelView.productId = self.mainModelView.productId
            comntvc.mainModelView.userId = self.mainModelView.productDetailsdata?.userId?.stringValue ?? ""
            
            let rwvc = homeStoryboard.instantiateViewController(withIdentifier: "ReviewCommentVc") as! ReviewCommentVc
            rwvc.mainModelView.productDetailsdata = self.mainModelView.productDetailsdata
            rwvc.mainSegmentController = .multipleSegment
            rwvc.selectedSegmetController = .review
            rwvc.mainModelView.Productdelegate = self
            rwvc.mainModelView.productId = self.mainModelView.productId
            rwvc.mainModelView.userId = self.mainModelView.productDetailsdata?.userId?.stringValue ?? ""
            
            let segment = MSSegmentControl(sectionTitles: headerTitle)
            setupSegment(segmentView: segment)
            
            mx = MXViewController<MSSegmentControl>.init(headerViewController: objHeaderDetails, segmentControllers: [comntvc,rwvc], segmentView: segment)
            
        }else if selectedController == .singleSegment{
            
            if self.mainModelView.productDetailsdata?.priceBidBarg?.lowercased() == "bid".lowercased(){
                objHeaderDetails.setCallNowHidden(hidden: true)
                if self.mainModelView.productDetailsdata?.bid ?? false{
                    objHeaderDetails.setBidNowHidden(hidden: true)
                    Defaults.object(forKey: remember_userid_key) as? String != self.mainModelView.productDetailsdata?.userId?.stringValue ? objHeaderDetails.setBidCompareHidden(hidden: false) : objHeaderDetails.setBidCompareHidden(hidden: true)
                }else{
                    Defaults.object(forKey: remember_userid_key) as? String != self.mainModelView.productDetailsdata?.userId?.stringValue ? objHeaderDetails.setBidNowHidden(hidden: false) : objHeaderDetails.setBidNowHidden(hidden: true)
                    objHeaderDetails.setBidCompareHidden(hidden: true)
                }
            }else if self.mainModelView.productDetailsdata?.priceBidBarg?.lowercased() == "price".lowercased()
            {
                Defaults.object(forKey: remember_userid_key) as? String != self.mainModelView.productDetailsdata?.userId?.stringValue ? objHeaderDetails.setCallNowHidden(hidden: false) : objHeaderDetails.setCallNowHidden(hidden: true)
                objHeaderDetails.setBidNowHidden(hidden: true)
                objHeaderDetails.setBidCompareHidden(hidden: true)
            }else if self.mainModelView.productDetailsdata?.priceBidBarg?.lowercased() == "bargeding".lowercased(){
                objHeaderDetails.setCallNowHidden(hidden: true)
                objHeaderDetails.setBidNowHidden(hidden: true)
                objHeaderDetails.setBidCompareHidden(hidden: true)
            }else{
               objHeaderDetails.setCallNowHidden(hidden: true)
                objHeaderDetails.setBidNowHidden(hidden: true)
               objHeaderDetails.setBidCompareHidden(hidden: true)
            }
             /*if Defaults.object(forKey: remember_userid_key) as? String != self.mainModelView.productDetailsdata?.userId?.stringValue {
                       theCurrentView.vwBidNow.isHidden = false
                       theCurrentView.btnBidNow.isHidden = false
                       theCurrentView.vwCallNow.isHidden = false
                       theCurrentView.btnCallNow.isHidden = false
                   }*/
            
            headerTitle = ["Review_key".localized]
            
            let rwvc = homeStoryboard.instantiateViewController(withIdentifier: "ReviewCommentVc") as! ReviewCommentVc
            rwvc.mainModelView.productDetailsdata = self.mainModelView.productDetailsdata
            rwvc.mainSegmentController = .singleSegment
            rwvc.selectedSegmetController = .review
            rwvc.mainModelView.Productdelegate = self
            rwvc.mainModelView.productId = self.mainModelView.productId
            rwvc.mainModelView.userId = self.mainModelView.productDetailsdata?.userId?.stringValue ?? ""
            let segment = MSSegmentControl(sectionTitles: headerTitle)
            setupSegment(segmentView: segment)
            
            mx = MXViewController<MSSegmentControl>.init(headerViewController: objHeaderDetails, segmentControllers: [rwvc], segmentView: segment)
                   
        }
        
        mx.headerViewOffsetHeight = 0
        mx.shouldScrollToBottomAtFirstTime = false
        mx.showsHorizontalScrollIndicator = false
        mx.showsVerticalScrollIndicator = false
        
        if selectedController == .singleSegment{
            mx.segmentViewHeight = 0
        }else if selectedController == .multipleSegment{
            mx.segmentViewHeight = 50
        }
        
        addChild(mx)
        contentView.addSubview(mx.view)
        mx.view.frame = contentView.bounds
        mx.didMove(toParent: self)

    }
    
    func setupSegment(segmentView: MSSegmentControl) {
        
        segmentView.borderType = .none
        segmentView.backgroundColor = UIColor.appThemeDarkBlueColor
        segmentView.selectionIndicatorColor = UIColor.appThemeYellowColor
        segmentView.selectionIndicatorHeight = 2
        
        segmentView.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: themeFont(size: 14, fontname: .regular)]
        segmentView.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: themeFont(size: 14, fontname: .regular)]
        
        segmentView.segmentWidthStyle = .fixed
        segmentView.selectionStyle = .fullWidth
        segmentView.selectedSegmentIndex = 0
        segmentView.segmentStart = .left
        
    }
}
extension MainProductDetailsVc: ReloadProductDelegate{
    func reloadProduct() {
        getProductDetailsApi()
    }
}
//MARK: ApiSetUp
extension MainProductDetailsVc {
    
    ///---- Get product
    func getProductDetailsApi() {
        if pushData["productId"].stringValue != ""{
             pushData = JSON()
        }
        let parameters : [String : Any] =   [ APIKey.key_product_id:self.mainModelView.productId
        ]
        print("parameters",parameters)
        mainModelView.productDetailApi(param: parameters, success: { (productData) in
            if productData.flag == 1{
                
                self.mainModelView.productDetailsdata = productData.productdata!
                self.setupCaregotyDetails()
                self.btnFav.isSelected = self.mainModelView.productDetailsdata?.isWishlist ?? false
                if Defaults.object(forKey: remember_userid_key) as? String != self.mainModelView.productDetailsdata?.userId?.stringValue{
                    self.btnFav.setImage(UIImage(named: "ic_like_unselect"), for: .normal)
                    self.btnFav.setImage(UIImage(named: "ic_like_select"), for: .selected)
                    self.btnFav.addTarget(self, action: #selector(self.btnFavUnfavouriteTapped), for: .touchUpInside)
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.btnFav)
                }
                       
            }
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    func addDeleteWishListApi(isSelected:Bool) {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            withoutLoginPopup()
            return
        }
        
        let parameters : [String : Any] =   [ APIKey.key_product_id:self.mainModelView.productId,
              APIKey.key_action:isSelected == true ? 1 : 0
        ]
        
        mainModelView.addRemoveWishList(param: parameters, success: { (loginData) in
            self.btnFav.isSelected = isSelected
            self.mainModelView.reloadWishListdelegate?.reloadProduct()
            makeToast(strMessage: loginData.msg ?? "")
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
}

