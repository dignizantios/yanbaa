//
//  ServiceDetailView.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class ServiceDetailView: UIView {

    //MARK: - Outlet
     @IBOutlet weak var imgMainHeader: UIImageView!
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblServiceDescription: UILabel!
    @IBOutlet weak var btnCallNow: CustomButton!
    @IBOutlet weak var btnReportNow: UIButton!
    
    //MARK: - SetupUI
    
    func setupUI(thedelegate:ServiceDetailVc){
        
        lblServiceName.textColor = UIColor.black
        lblServiceName.font = themeFont(size: 15, fontname: .semibold)
        
        lblServiceDescription.textColor = UIColor.black
        lblServiceDescription.font = themeFont(size: 12, fontname: .regular)

        btnCallNow.setupThemeButtonUI()
        
        collectionImages.register(UINib(nibName: "AddImagesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddImagesCollectionCell")
        
        btnCallNow.setImage(UIImage(named: "ic_telephone")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        
        if DeviceLanguage == "ar"{
            btnCallNow.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -50)
        }else{
            btnCallNow.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 50)
        }
        
    }
    
}
