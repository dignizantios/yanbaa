//
//  HomeSubCategoryVc.swift
//  Yaanba
//
//  Created by Abhay on 08/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class HomeSubCategoryVc: UIViewController {

    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:HomeSubCategoryView = { [unowned self] in
        return self.view as! HomeSubCategoryView
    }()
    lazy var mainModelView: HomeSubCategoryViewModel = {
        return HomeSubCategoryViewModel(theController: self)
    }()
    //MARKL - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI()
        // Do any additional setup after loading the view.
        getSubCategoryApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: mainModelView.selectedCategoryName.uppercased())
    }
    
}


//MARK: - CollectionView

extension HomeSubCategoryVc:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.mainModelView.subCategorys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeProductCollectionCell", for: indexPath) as! HomeProductCollectionCell
        let dic = self.mainModelView.subCategorys[indexPath.row]
        if dic.categoryImage !=  "" {
            cell.imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgProduct.sd_setImage(with: dic.categoryImage?.toURL(), placeholderImage: UIImage(named: "ic_detail_view_big_splash_holder"), options: .lowPriority, context: nil)
        } else {
            cell.imgProduct.image = UIImage(named: "ic_detail_view_big_splash_holder")
        }
        cell.lblProduct.text = dic.subCategory ?? ""
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = homeStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
        obj.mainVM.subCategoryId = "\(self.mainModelView.subCategorys[indexPath.row].id ?? 0)"
        obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
        
//        let obj = homeStoryboard.instantiateViewController(withIdentifier: "SpecificCategoryVc") as! SpecificCategoryVc
//        obj.mainModelView.subCategoryId = "\(self.mainModelView.subCategorys[indexPath.row].id ?? 0)"
//        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width/3, height: 180) // Product size
    
    }
    
}

//MARK: ApiSetUp
extension HomeSubCategoryVc {
    
    ///---- Get CategoryList
    func getSubCategoryApi() {
        let parameters : [String : Any] =   [ APIKey.key_category_id:self.mainModelView.selectedCategoryId
        ]
        print("parameters",parameters)
        mainModelView.SubCategoryApi(param: parameters, success: { (categoryData) in
            if let data = categoryData.subCategorydata{
                self.mainModelView.subCategorys = []
                self.mainModelView.subCategorys = data
                self.theCurrentView.collectionSubCategory.reloadData()
                self.theCurrentView.lblQuntity.text = "(\(self.mainModelView.subCategorys.count))"
            }
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
//                makeToast(strMessage: error)
            }
        })
    }
    
    
}
