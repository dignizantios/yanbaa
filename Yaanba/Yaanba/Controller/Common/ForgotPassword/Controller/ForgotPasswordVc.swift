//
//  ForgotPasswordVc.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ForgotPasswordVc: UIViewController {

    //MARK: - outlet
    
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!
    //MARK: - Varaible
    lazy var mainModelView: ForgotPasswordViewModel = {
        return ForgotPasswordViewModel(theController: self)
    }()
    
    //MARK: - View life cyclee
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        transperentNavigationBar()
    }

}

//MARK: - SetupUI
extension ForgotPasswordVc{
    
    func setupUI(){
        
        lblForgotPassword.font = themeFont(size: 18, fontname: .bold)
        
        lblEmail.textColor = UIColor.appThemeDarkBlueColor
        lblEmail.font = themeFont(size: 11, fontname: .regular)
        
        txtEmail.font = themeFont(size: 13, fontname: .regular)
        txtEmail.textColor = UIColor.black
         txtEmail.placeholder = "EMAIL_ID_key".localized
        lblForgotPassword.text = "Forgot_Password_key".localized
        
        btnSubmit.setupThemeButtonUI()
        btnSubmit.setTitle("Submit_key".localized, for: .normal)

        lblEmail.text = "EMAIL_ID_key".localized
        
    }
    
}

//MARK: - IBAction method
extension ForgotPasswordVc{
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        mainModelView.ValidateDetails()
    }
    
}
//MARK: ApiSetUp
extension ForgotPasswordVc {
    
    ///---- LOgin
    func forgotApi() {
        let parameters : [String : Any] =   [ APIKey.key_email:self.txtEmail.text!
        ]
        
        mainModelView.forgotPwdApi(param: parameters, success: { (loginData) in
            self.navigationController?.popViewController(animated: true)
            makeToast(strMessage: loginData.msg ?? "")
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    
    
}
