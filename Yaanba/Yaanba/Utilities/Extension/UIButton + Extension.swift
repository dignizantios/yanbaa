//
//  UIButton + Extension.swift
//  Liber
//
//  Created by YASH on 25/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

extension UIButton
{
    
    func setupThemeButtonUI()
    {
        self.setTitleColor(UIColor.black, for: .normal)
        self.backgroundColor = UIColor.appThemeYellowColor
        self.titleLabel?.font = themeFont(size: 17, fontname: .bold)
        self.layer.cornerRadius = self.frame.height/2
        self.layer.masksToBounds = true
        
    }
    
}
