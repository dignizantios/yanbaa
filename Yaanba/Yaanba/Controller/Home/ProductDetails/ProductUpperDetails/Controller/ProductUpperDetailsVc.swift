//
//  ProductUpperDetailsVc.swift
//  Yaanba
//
//  Created by Abhay on 10/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import MXScroll
import SDWebImage



class ProductUpperDetailsVc: UIViewController {

    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:ProductUpperDetailsView = { [unowned self] in
        return self.view as! ProductUpperDetailsView
    }()
    lazy var mainModelView: ProductUpperDetailsViewModel = {
        return ProductUpperDetailsViewModel(theController: self)
    }()
    var arrImages = ["1.jpg","2.jpeg","3.jpg","4.jpeg","5.jpg"]
    var isBidOutOfDate = Bool()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI(thedelegate:self)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if self.mainModelView.productDetailsdata?.endDate != nil && self.mainModelView.productDetailsdata?.priceBidBarg?.lowercased() == "bid".lowercased(){
            if let endDate = self.mainModelView.productDetailsdata?.endDate{
                let formater = DateFormatter()
                formater.dateFormat = "yyyy-MM-dd HH:mm:ss"
                if Date() > formater.date(from: endDate)!{
                    //theCurrentView.vwBidNow.isHidden = true
                    theCurrentView.btnBidNow.backgroundColor = UIColor.init(red: 255/255, green: 216/255, blue: 0/255, alpha: 0.3)
                    self.isBidOutOfDate = true
                }
            }
        }
        if Defaults.object(forKey: remember_userid_key) as? String == self.mainModelView.productDetailsdata?.userId?.stringValue {
            theCurrentView.btnReport.isHidden = true
        }
    }
    
}

//MARK: - IBAction method

extension ProductUpperDetailsVc{
    
    @IBAction func btnReportTapped(_ sender: UIButton) {
        
        let obj = homeStoryboard.instantiateViewController(withIdentifier: "ReportVc") as! ReportVc
        print(self.mainModelView.productId)
        obj.mainModelView.productId = self.mainModelView.productId
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnBidNowTapped(_ sender: CustomButton) {
        guard self.isBidOutOfDate == false else {
            makeToast(strMessage: "Bid_out_of_endDate_key".localized)
            return
        }
        
        let obj = homeStoryboard.instantiateViewController(withIdentifier: "PlusMinusBiddingPopupVc") as! PlusMinusBiddingPopupVc
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.mainModelView.BasicPrice = Int(self.mainModelView.productDetailsdata?.basicPrice ?? 0)
        obj.mainModelView.stepPrice = Int(self.mainModelView.productDetailsdata?.stepPrice ?? 0)
        obj.mainModelView.productId = "\(self.mainModelView.productDetailsdata?.id ?? 0)"
        obj.mainModelView.BidAddedelegate = self
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnCallNowTapped(_ sender: CustomButton) {
          let actionsheet = UIAlertController(title: nil, message: "Make_a_call_key".localized, preferredStyle: UIAlertController.Style.actionSheet)

              actionsheet.addAction(UIAlertAction(title: "Main_number_key".localized.uppercased(), style: UIAlertAction.Style.default, handler: { (action) -> Void in
                  if let call = self.mainModelView.productDetailsdata?.productCall!,
                      let url = URL(string: "tel://+\(call)"),
                      UIApplication.shared.canOpenURL(url) {
                      UIApplication.shared.open(url)
                  }
              }))

              actionsheet.addAction(UIAlertAction(title: "Alternative_number_key".localized.uppercased(), style: UIAlertAction.Style.default, handler: { (action) -> Void in
                  if let call = self.mainModelView.productDetailsdata?.alternativeCall!,
                      let url = URL(string: "tel://+\(call)"),
                      UIApplication.shared.canOpenURL(url) {
                      UIApplication.shared.open(url)
                  }
              }))
              actionsheet.addAction(UIAlertAction(title: "Cancel_key".localized.uppercased(), style: UIAlertAction.Style.cancel, handler: { (action) -> Void in

              }))
              self.present(actionsheet, animated: true, completion: nil)
    }
    
    func setCallNowHidden(hidden:Bool){
        theCurrentView.vwCallNow.isHidden = hidden
    }
    
    func setBidNowHidden(hidden:Bool){
        theCurrentView.vwBidNow.isHidden = hidden
    }
    
    func setBidCompareHidden(hidden:Bool){
        theCurrentView.vwBidCompare.isHidden = hidden
    }
    
}
extension ProductUpperDetailsVc: ReloadAddProductDelegate{
    func reloadAddProduct() {
        self.mainModelView.Productdelegate?.reloadProduct()
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            self.theCurrentView.setupUI(thedelegate:self)
        }
    }
}

//MARK: - CollectionView
extension ProductUpperDetailsVc:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.mainModelView.productDetailsdata?.productMultiImage?.count ?? 0 > 0{
            return self.mainModelView.productDetailsdata!.productMultiImage!.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImagesCollectionCell", for: indexPath) as! AddImagesCollectionCell
        cell.btnDelete.isHidden = true
        if self.mainModelView.productDetailsdata?.productMultiImage?.count ?? 0 > 0{
            let dict = self.mainModelView.productDetailsdata?.productMultiImage?[indexPath.row]
            if dict?.productImage !=  nil {
                cell.imgSelected.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.imgSelected.sd_setImage(with: dict?.productImage?.toURL(), placeholderImage: UIImage(named: "ic_upload_picture"), options: .lowPriority, context: nil)
            } else {
                cell.imgSelected.image = UIImage(named: "ic_upload_picture")
            }
            
        }else{
            cell.imgSelected.image = UIImage(named: "ic_upload_picture")
        }
            
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        theCurrentView.imgMainHeader.image = UIImage(named: self.arrImages[indexPath.row])
        if self.mainModelView.productDetailsdata?.productMultiImage?[indexPath.row].productImage !=  nil {
            theCurrentView.imgMainHeader.sd_imageIndicator = SDWebImageActivityIndicator.gray
            theCurrentView.imgMainHeader.sd_setImage(with: self.mainModelView.productDetailsdata?.productMultiImage?[indexPath.row].productImage?.toURL(), placeholderImage: UIImage(named: "ic_detail_view_screen_big_place_holder"), options: .lowPriority, context: nil)
        } else {
            theCurrentView.imgMainHeader.image = UIImage(named: "ic_detail_view_screen_big_place_holder")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width/4, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        let totalCellWidth = 80 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)

        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset

        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)

    }
    
}



//MARK:- MXScrollview delegate

extension ProductUpperDetailsVc: MXViewControllerViewSource {
    func headerViewForMixObserveContentOffsetChange() -> UIView? {
        return theCurrentView.vwScrollHeader
    }
}




