//
//  PlusMinusBiddingPopupViewModel.swift
//  Yaanba
//
//  Created by Abhay on 15/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class PlusMinusBiddingPopupViewModel{
    
    //MARK: - View life cycle
    fileprivate weak var theController:PlusMinusBiddingPopupVc!
    
    init(theController:PlusMinusBiddingPopupVc) {
        self.theController = theController
    }
    //MARK: - Variable
    
    var BasicPrice = Int()
    var stepPrice = Int()
    var productId = String()
    weak var BidAddedelegate: ReloadAddProductDelegate?
    var fixPrice = Int()
    
    func incrementStep(stepValue:Int) -> Int{
        BasicPrice += stepValue
        return BasicPrice
        
    }
    
    func decrementStep(stepValue:Int) -> Int{
        let tempPrice = BasicPrice - stepValue
        if tempPrice <= fixPrice{
            BasicPrice = fixPrice
            return fixPrice
        }
        BasicPrice = tempPrice
        return BasicPrice
        
    }
    
}
//MARK: Api setup
extension PlusMinusBiddingPopupViewModel {
    func ValidateDetails() {
        
         if (self.theController.theCurrentView.lblValue.text?.trimmingCharacters(in: .whitespaces))!.isEmpty{
            makeToast(strMessage: "Please_enter_bid_value_Key".localized)
        }
        else {
            self.theController.addBidApi()
        }
    }
    func addBidApi(param:[String:Any],success:@escaping(_ token : BidAddedModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aBidNow        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let BidAddData = BidAddedModel(JSON: data) {
                             success(BidAddData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}

