//
//  DiscountDetailsVC.swift
//  Yaanba
//
//  Created by Abhay on 05/03/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import MessageUI
import SDWebImage

class DiscountDetailsVC: UIViewController {
    
    //MARK: - Variable
    
     lazy var theCurrentView:DiscountDetailsView = { [unowned self] in
        return self.view as! DiscountDetailsView
    }()
    
    lazy var mainModelView: DiscountDetailsViewModel = {
        return DiscountDetailsViewModel(theController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        theCurrentView.setupUI()
        getDiscountDetailsApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "Discount_detail_key".localized)
    }
    

}
//MARK: - IBAction method

extension DiscountDetailsVC:MFMailComposeViewControllerDelegate{
    
    @IBAction func btnSendEmailTapped(_ sender: UIButton) {
        //TODO:  You should chack if we can send email or not
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([self.mainModelView.discountDetaildata?.email ?? ""])
            mail.setSubject("Enter Subject Here")
            mail.setMessageBody("", isHTML: true)//"<p>You're so awesome!</p>"
            present(mail, animated: true)
        } else {
            print("Application is not able to send an email")
        }
    }
    
    @IBAction func btnCallNowTapped(_ sender: UIButton) {
        if let call = self.mainModelView.discountDetaildata?.contact!,
            let url = URL(string: "tel://\(call)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    //MARK: MFMail Compose ViewController Delegate method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
//MARK: - CollectionView Delegate

extension DiscountDetailsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImagesCollectionCell", for: indexPath) as! AddImagesCollectionCell
        cell.btnDelete.isHidden = true
        if self.mainModelView.discountDetaildata?.bannerImage !=  nil {
            cell.imgSelected.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgSelected.sd_setImage(with: self.mainModelView.discountDetaildata?.bannerImage?.toURL(), placeholderImage: UIImage(named: "ic_upload_picture"), options: .lowPriority, context: nil)
        } else {
            cell.imgSelected.image = UIImage(named: "ic_upload_picture")
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 200)
    }
    
}
//MARK: ApiSetUp
extension DiscountDetailsVC {
    
    ///---- product details
    
    func getDiscountDetailsApi() {
        
        let parameters : [String : Any] =   [ APIKey.key_id:self.mainModelView.discountId
        ]
        print("parameters",parameters)
        mainModelView.discountDetailApi(param: parameters, success: { (discountData) in
            if discountData.flag == 1{
                
                self.mainModelView.discountDetaildata = discountData.discountDetaildata!
                self.theCurrentView.lblDate.text = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "dd MMM yyyy", strDate: self.mainModelView.discountDetaildata?.createdAt ?? "")//dd MMM yyyy hh:mm a
                self.theCurrentView.lblCompanyName.text = self.mainModelView.discountDetaildata?.companyName  ?? ""
                self.theCurrentView.lblDiscount.text = "\(self.mainModelView.discountDetaildata?.discount ?? 0)"+"\n% "+"Off_key".localized
                self.theCurrentView.lblDescription.text = self.mainModelView.discountDetaildata?.description  ?? ""
                self.theCurrentView.btnCall.setTitle(self.mainModelView.discountDetaildata?.contact ?? "", for: .normal)
                self.theCurrentView.btnEmail.setTitle(self.mainModelView.discountDetaildata?.email ?? "", for: .normal)
                
                self.theCurrentView.discountCollectionView.reloadData()
            }
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    
}

