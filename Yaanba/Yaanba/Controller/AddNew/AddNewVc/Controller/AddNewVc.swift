//
//  AddNewVc.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import TOCropViewController
import DropDown
import SwiftyJSON

class AddNewVc: UIViewController {
    
    //MARK: - Variable
    
     lazy var theCurrentView:AddNewView = { [unowned self] in
        return self.view as! AddNewView
    }()

    lazy var mainModelView: AddNewViewModel = {
        return AddNewViewModel(theController: self)
    }()
    
    private var customImagePicker = CustomImagePicker()
    
    var chooseTypeDD = DropDown() // Select main type
    var chooseSellTypeDD = DropDown() // Product sell type
    var chooseCategoryDD = DropDown() // Select main category
    var chooseSubCategoryDD = DropDown() // Select sub category
    var chooseSubOfSubCategoryDD = DropDown() // Select sub-subcategory
    
    var isStartDateTapped = "0"
    var isStartTimeTapped = "0"
    var selectedStartDate = Date()
    var isImgSelectionOpen = Bool()
    var countryDialCode: String = "+61"
    var countryName: String = "Australia"
    var countryCode: String = "AU"
    //MARK:- View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        chooseTypeDD.dataSource = ["Product".localized,"Service".localized]
        chooseSellTypeDD.dataSource = ["Price".localized,"Bidding".localized,"Auction".localized]
        theCurrentView.setupUI(theController: self)
        // Do any additional setup after loading the view.
        categoryListApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarCenterText(titleText: "ADD_NEW_key".localized.capitalized)
        
        if isImgSelectionOpen == false{
            theCurrentView.setupUI(theController: self)
            isImgSelectionOpen = false
            mainModelView.arrayOfImages.removeAll()
            theCurrentView.collectionImages.reloadData()
            theCurrentView.txtvwDescribeYourItem.text = ""
            theCurrentView.lblDescribeYourItem.isHidden = false
        }
        isImgSelectionOpen = false
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow")?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(btnHomeScreen))
        leftButton.tintColor = .appThemeDarkBlueColor
        self.navigationItem.leftBarButtonItem = leftButton

    }
    
    override func viewDidLayoutSubviews() {

        configureDropdown(dropdown: self.chooseTypeDD, sender: theCurrentView.txtChooseType)
        configureDropdown(dropdown: self.chooseSellTypeDD, sender: theCurrentView.txtChooseSellType)
        configureDropdown(dropdown: self.chooseCategoryDD, sender: theCurrentView.txtSelectProductCategory)
        configureDropdown(dropdown: self.chooseSubCategoryDD, sender: theCurrentView.txtSelectSubCategory)
        configureDropdown(dropdown: self.chooseSubOfSubCategoryDD, sender: theCurrentView.txtSubOfSubCategory)
        selectionIndex()
    }
    
    
    func selectionIndex()
    {
        
        self.chooseTypeDD.selectionAction = { (index, item) in
            self.theCurrentView.txtChooseType.text = item
            self.view.endEditing(true)
            
            if index == 0{ // For product tapped
                self.theCurrentView.vwProductDetail.isHidden = false
            }else{
                self.theCurrentView.vwProductDetail.isHidden = true
            }
            self.theCurrentView.vwItemPrice.isHidden = true
            self.chooseTypeDD.hide()
        }
        
        self.chooseSellTypeDD.selectionAction = { (index, item) in
            self.theCurrentView.txtChooseSellType.text = item
            self.view.endEditing(true)
            
            if index == 0{ // For price
                self.theCurrentView.vwBidding.isHidden = true
                self.theCurrentView.vwItemPrice.isHidden = false
            }else if index == 1{
                self.theCurrentView.vwBidding.isHidden = false
                self.theCurrentView.vwItemPrice.isHidden = true
            }else{
                self.theCurrentView.vwBidding.isHidden = true
                self.theCurrentView.vwItemPrice.isHidden = false
            }
            
            self.chooseSellTypeDD.hide()
        }
        self.chooseCategoryDD.selectionAction = { (index, item) in
            self.theCurrentView.txtSelectProductCategory.text = item
            self.view.endEditing(true)
            self.theCurrentView.txtSelectSubCategory.text = ""
            self.theCurrentView.txtSubOfSubCategory.text = ""
            self.mainModelView.selectedCategoryId = self.mainModelView.arrayCategoryData[index]["id"].stringValue
            self.getSubCategoryApi()
            self.chooseCategoryDD.hide()
        }
        
        self.chooseSubCategoryDD.selectionAction = { (index, item) in
            self.theCurrentView.txtSelectSubCategory.text = item
            self.view.endEditing(true)
            self.mainModelView.selectedSubCategoryId = "\(self.mainModelView.subCategorys[index].id ?? 0)"
            self.chooseSubCategoryDD.hide()
            self.theCurrentView.txtSubOfSubCategory.text = ""
            self.getSubOfSubCategory(subOfSubCategory: "\(self.mainModelView.subCategorys[index].id ?? 0)")
        }
        
        self.chooseSubOfSubCategoryDD.selectionAction = { (index, item) in
            self.theCurrentView.txtSubOfSubCategory.text = item
            self.view.endEditing(true)
            self.mainModelView.selectedSubofSubCategoryId = "\(self.mainModelView.subofSubCategorys[index].id ?? 0)"
            self.chooseSubOfSubCategoryDD.hide()
        }
    }
    
}

//MARK: - IBAction

extension AddNewVc{
    
    @objc func btnHomeScreen(){
        appDelegate.objCustomTabBar.selectedIndex = 0
    }
    
    @IBAction func btnPostTapped(_ sender: UIButton) {
        
        mainModelView.ValidateDetails()
    }
    
    @objc func removeSelectedImage(_ sender: UIButton){
        mainModelView.arrayOfImages.remove(at: sender.tag)
        theCurrentView.collectionImages.reloadData()
    }
    @IBAction func btnCountryTapped(_ sender: UIButton) {
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.delegate = self
        self.present(obj, animated:  false, completion: nil)
    }

    
}

extension AddNewVc:CountryCodeDelegate{
    func CountryCodeDidFinish(data: JSON) {
        self.countryDialCode = data["dial_code"].string ?? ""
        self.countryName = data["name"].string ?? ""
        self.countryCode = data["code"].string ?? ""
        
        self.theCurrentView.imgCountryCode.image = UIImage(named : countryCode)
        self.theCurrentView.lblCountryCode.text = countryDialCode
    }
}


//MARK:- textField Delegate

extension AddNewVc{
    
    func isCheckCurrentDate(date: Date) -> Bool{
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd-MM-yyyy"
        let dtStr = dateformatter.string(from: Date())
        let dateValue = dateformatter.date(from: dtStr)!
        
        if date == dateValue{
            return true
        }
        
        return false
    }
    
    func generateFromDate(date:String,time:String) -> Date{
        
        let dateFormatter = DateFormatter()
        let strfinalDate = "\(date) \(time)"
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        let date = dateFormatter.date(from: strfinalDate)!
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        let strDate = dateFormatter.string(from: date)
        print("strDate : \(dateFormatter.date(from: strDate))")
        return dateFormatter.date(from: strDate)!
    }
    
    
}

//MARK: - TextView Delegate

extension AddNewVc:UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        theCurrentView.lblDescribeYourItem.isHidden = (textView.text == "") ? false : true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK: - CollectionView datasource

extension AddNewVc: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainModelView.arrayOfImages.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImagesCollectionCell", for: indexPath) as! AddImagesCollectionCell
        
        if mainModelView.arrayOfImages.count == indexPath.row{
            cell.imgSelected.image = UIImage(named: "ic_add_image")
            cell.btnDelete.isHidden = true
        }
        else{
            cell.imgSelected.image = mainModelView.arrayOfImages[indexPath.row]
            cell.btnDelete.isHidden = false
            
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(removeSelectedImage), for: .touchUpInside)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if mainModelView.arrayOfImages.count == indexPath.row{
            self.selectImages()
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width/4, height: collectionView.bounds.height)
        
    }
    
}

//MARK: - ImageSelection

extension AddNewVc: TOCropViewControllerDelegate {
    
    func selectImages()
    {
        customImagePicker.typeOfPicker = .onlyPhoto
        customImagePicker.showImagePicker(fromViewController: self,
                                          navigationColor: UIColor.appThemeYellowColor,
                                          imagePicked: { (response) in
                                            
                                            let theImage = response[UIImagePickerController.InfoKey.originalImage] as! UIImage
                                            self.openCropVC(Image: theImage)
                                                                                        
                                            
        }, imageCanceled: {
        }, imageRemoved: nil)
    }
    
    func openCropVC(Image:UIImage)
    {
        let cropVC = TOCropViewController(image: Image)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        isImgSelectionOpen = true
        self.present(cropVC, animated: false, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        
        mainModelView.arrayOfImages.append(image)
        theCurrentView.collectionImages.reloadData()
        theCurrentView.collectionImages.scrollToItem(at: IndexPath(item: mainModelView.arrayOfImages.count, section: 0), at: .right, animated: false)
        
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        
        dismiss(animated: true, completion: nil)
    }

}

//MARK: - Date and Time selection Delegat

extension AddNewVc: datePickerDelegate{

    func setDateValue(dateValue: Date, selectedType: Int) {
        
        print("dateValue : \(dateValue)")
        
        if selectedType == 0{ // date
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let strDate = dateFormatter.string(from: dateValue)
            
            if isStartDateTapped == "0"{
                self.selectedStartDate = dateFormatter.date(from: strDate)!
                theCurrentView.txtStartDate.text = strDate
                
                theCurrentView.txtEndDate.text = ""
                theCurrentView.txtStartTime.text = ""
                theCurrentView.txtEndTime.text = ""
                
            }else{
                theCurrentView.txtEndDate.text = strDate
                theCurrentView.txtEndTime.text = ""
            }
            
        }else{ // Time
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let strDate = dateFormatter.string(from: dateValue)
            
            if self.isStartTimeTapped == "0"{
                theCurrentView.txtStartTime.text = strDate
                
                theCurrentView.txtEndDate.text = ""
                theCurrentView.txtEndTime.text = ""
                
            }else{
                theCurrentView.txtEndTime.text = strDate
            }
            
        }
        
    }
        
}
//MARK: ApiSetUp
extension AddNewVc {
    
    ///---- Get CategoryList
    func categoryListApi() {
        let parameters =   [String : Any]()
        
        mainModelView.CategoryListApi(param: parameters, success: { (categoryData) in
            if let data = categoryData.categorydata{
                self.mainModelView.arrayCategoryData = [JSON]()
                for (i, category) in data.enumerated(){
                    var dict = JSON()
                    dict["id"].stringValue = "\(category.id ?? 0)"
                    dict["name"].stringValue = category.category ?? ""
                    if i == 0{
                        dict["selected"].stringValue = "1"
                    }else{
                        dict["selected"].stringValue = "0"
                    }
                    self.mainModelView.arrayCategoryData.append(dict)
                }
                var array = [String]()
                self.mainModelView.arrayCategoryData.forEach { (category) in
                    array.append(category["name"].stringValue)
                }
                self.chooseCategoryDD.dataSource = array
                //print(self.mainModelView.arrayData)AddNewVc
                 //self.theCurrentView.collectionheader.reloadData()
                //self.carbonSetupStyle()
               
            }
            
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    ///---- Get CategoryList
    
    func getSubCategoryApi() {
        let parameters : [String : Any] =   [ APIKey.key_category_id:self.mainModelView.selectedCategoryId
        ]
        print("parameters",parameters)
        mainModelView.SubCategoryApi(param: parameters, success: { (categoryData) in
            if let data = categoryData.subCategorydata{
                self.mainModelView.subCategorys = []
                self.mainModelView.subCategorys = data
                var array = [String]()
                self.mainModelView.subCategorys.forEach { (Subcategory) in
                    array.append(Subcategory.subCategory ?? "")
                }
                self.chooseSubCategoryDD.dataSource = array
            }
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                if error == "token_expired"{
                    self.logoutApiCall()
                }else{
                    makeToast(strMessage: error)
                }
            }
        })
    }
    
    func getSubOfSubCategory(subOfSubCategory: String) {
        let param = [APIKey.key_sub_category_id:subOfSubCategory]
        
        mainModelView.subOfSubCategoryApi(param: param) {
            print("Get sub of sub category")
        } failed: { error in
            if error == "token_expired" {
                self.logoutApiCall()
            }
            else {
                if error == "token_expired" {
                    self.logoutApiCall()
                }
                else {
                    makeToast(strMessage: error)
                }
            }
        }
    }
    
    func addProductApi() {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            withoutLoginPopup()
            return
        }
        print(self.countryDialCode.dropFirst())
        var parameters : [String : Any] =   [ APIKey.key_category_id:self.mainModelView.selectedCategoryId,
                                              APIKey.key_product_type:self.theCurrentView.txtChooseType.text!.lowercased(),
                                              APIKey.key_sub_category_id:self.mainModelView.selectedSubCategoryId,
                                              APIKey.key_sub_subcategory_id:self.mainModelView.selectedSubofSubCategoryId,
                                              APIKey.key_product_title:self.theCurrentView.txtItemTitle.text!,
                                              APIKey.key_alternative_mobile_no:self.theCurrentView.txtAlternativeMobileNumber.text!,
                                              APIKey.key_description:self.theCurrentView.txtvwDescribeYourItem.text!,
                                              APIKey.key_country_code:self.countryDialCode.dropFirst()
        ]
        var dataArray = [Data]()
        var nameArray = [String]()
        var fileNameArray = [String]()
        for i in 0...mainModelView.arrayOfImages.count-1{
            let img = mainModelView.arrayOfImages[i]
            let data = img.pngData()
            dataArray.append(data!)
            nameArray.append("product_image[\(i)]")
            fileNameArray.append("\(UUID().uuidString).png")
            //parameters["\(APIKey.key_product_image)[\(i)]"] = data
            
        }
        parameters[APIKey.key_price] = self.theCurrentView.txtItemPrice.text!
       
        parameters[APIKey.key_basic_price] = self.theCurrentView.txtBasicPrice.text!
        parameters[APIKey.key_step_price] = self.theCurrentView.txtStepPrice.text!
        if self.theCurrentView.txtChooseSellType.text!.lowercased() == "Bidding".localized.lowercased(){
            parameters[APIKey.key_price_bid_barg] = "bid"
        }else{
            print(self.theCurrentView.txtChooseSellType.text!)
            parameters[APIKey.key_price_bid_barg] = self.theCurrentView.txtChooseSellType.text!.localized.lowercased() == "Auction".localized.lowercased() ? "Bargeding".localized.lowercased() : self.theCurrentView.txtChooseSellType.text!.localized.lowercased()
        }
         
        if theCurrentView.txtStartTime.text! != "" && theCurrentView.txtStartTime.text! != "" && theCurrentView.txtEndDate.text! != "" && theCurrentView.txtEndTime.text! != ""{
                   parameters[APIKey.key_start_date] = localToUTC(date: DateToString(Formatter: "yyyy-MM-dd HH:mm:ss", date: generateFromDate(date: theCurrentView.txtStartDate.text!, time: theCurrentView.txtStartTime.text!)))
                   parameters[APIKey.key_end_date] = localToUTC(date: DateToString(Formatter: "yyyy-MM-dd HH:mm:ss", date: generateFromDate(date: theCurrentView.txtEndDate.text!, time: theCurrentView.txtEndTime.text!)))
        }else{
            parameters[APIKey.key_start_date] = ""
            parameters[APIKey.key_end_date] = ""
        }
       
        
        print("parameters",parameters)
       /* self.service_callImagePOST(parameter: parameters as NSDictionary, path: "", image: mainModelView.arrayOfImages[0], isImage: true, withSuccess: { (result) in
            print(result)
        }) { (error) in
            print(error)
        }*/
        //<#T##[UIImage]#>
       /* mainModelView.AddProductApi(param: parameters, image: mainModelView.arrayOfImages, success: { (addProductData) in
            self.mainModelView.arrayOfImages.removeAll()
            self.theCurrentView.txtvwDescribeYourItem.text = ""
            self.theCurrentView.lblDescribeYourItem.isHidden = false
            makeToast(strMessage: addProductData.msg ?? "")
            self.navigationController?.popViewController(animated: true)
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            print(error)
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                if error == "token_expired"{
                    self.logoutApiCall()
                }else{
                    makeToast(strMessage: error)
                }
            }
        })*/
       
        mainModelView.AddProductApi(param: parameters, image: dataArray, NameArray: nameArray, FileNameArray: fileNameArray, success: { (AddProductModel) in
            print(AddProductModel.flag)
            if AddProductModel.flag == 1{
                self.mainModelView.arrayOfImages.removeAll()
                self.theCurrentView.collectionImages.reloadData()
                self.theCurrentView.txtvwDescribeYourItem.text = ""
                self.theCurrentView.lblDescribeYourItem.isHidden = false
                makeToast(strMessage: AddProductModel.msg ?? "")
                //appDelegate.objCustomTabBar.selectedIndex = 0
                let obj = homeStoryboard.instantiateViewController(withIdentifier: "CompleteOrderVc") as! CompleteOrderVc
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
            
        }) { (error) in
            print(error)
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        }
    }
   
}

