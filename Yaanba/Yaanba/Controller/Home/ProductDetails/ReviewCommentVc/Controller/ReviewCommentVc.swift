//
//  ReviewCommentVc.swift
//  Yaanba
//
//  Created by Abhay on 11/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import MXScroll
import SDWebImage

enum checkSelectedSegmet {
    case review
    case comment
}
protocol ReloadAddProductDelegate: class {
    func reloadAddProduct()
}
class ReviewCommentVc: UIViewController {

    //MARK: - Variable
    
    lazy var theCurrentView:ReviewCommentView = { [unowned self] in
        return self.view as! ReviewCommentView
    }()
    lazy var mainModelView: ReviewCommentViewModel = {
        return ReviewCommentViewModel(theController: self)
    }()
    var selectedSegmetController = checkSelectedSegmet.review
    var mainSegmentController = checkSegmentType.multipleSegment
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI()
        // Do any additional setup after loading the view.
    }

}

//MARK: - IBAction method

extension ReviewCommentVc{
    
    @objc func btnAddCommentTapped(){
        
        let commentVc = homeStoryboard.instantiateViewController(withIdentifier: "AddCommentVc") as! AddCommentVc
        commentVc.modalPresentationStyle = .overCurrentContext
        commentVc.modalTransitionStyle = .crossDissolve
        commentVc.mainModelView.ProductAdddelegate = self
        commentVc.mainModelView.productId = self.mainModelView.productId
        self.present(commentVc, animated: false, completion: nil)
        
    }
    
    @objc func btnWriteReviewTapped(){
        
        let reviewvc = homeStoryboard.instantiateViewController(withIdentifier: "ReviewVc") as! ReviewVc
        reviewvc.mainModelView.ProductAdddelegate = self
        reviewvc.mainModelView.productId = self.mainModelView.productId
        self.navigationController?.pushViewController(reviewvc, animated: true)
        
    }
    
}
extension ReviewCommentVc: ReloadAddProductDelegate{
    func reloadAddProduct() {
        self.mainModelView.Productdelegate?.reloadProduct()
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            self.theCurrentView.tblReviewCommnet.reloadData()
        }
    }
}
//MARK: - Table Delegate

extension ReviewCommentVc:  UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if selectedSegmetController == .comment
         {
            if self.mainModelView.productDetailsdata?.productComment?.count == 0
            {
                let lbl = UILabel()
                lbl.text = "No_list_yet".localized
                lbl.numberOfLines = 5
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appThemeGrayColor
                lbl.font = themeFont(size: 16.0, fontname: .regular)
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            tableView.backgroundView = nil
            return self.mainModelView.productDetailsdata?.productComment?.count ?? 0
        }
        if self.mainModelView.productDetailsdata?.productReview?.count == 0
        {
            let lbl = UILabel()
            lbl.text = "No_list_yet".localized
            lbl.numberOfLines = 5
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeGrayColor
            lbl.font = themeFont(size: 16.0, fontname: .regular)
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return self.mainModelView.productDetailsdata?.productReview?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedSegmetController == .comment{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTblCell") as!  CommentTblCell
            let dict = self.mainModelView.productDetailsdata?.productComment?[indexPath.row]
            if dict?.userProfile !=  nil {
                cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.imgProfile.sd_setImage(with: dict?.userProfile?.toURL(), placeholderImage: UIImage(named: "ic_review_splash_holder"), options: .lowPriority, context: nil)
            } else {
                cell.imgProfile.image = UIImage(named: "ic_review_splash_holder")
            }
            cell.lblName.text = dict?.userName ?? ""
            cell.lblDate.text = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "dd MMM yyyy", strDate: dict?.createdAt ?? "")
            cell.lblCommentValue.text = dict?.comment ?? ""
            cell.lblPrice.text = "\("KWD".localized) \(dict?.commentPrice ?? 0)"
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTblCell") as!  ReviewTblCell
        let dict = self.mainModelView.productDetailsdata?.productReview?[indexPath.row]
        if dict?.userProfile !=  nil {
            cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgProfile.sd_setImage(with: dict?.userProfile?.toURL(), placeholderImage: UIImage(named: "ic_review_splash_holder"), options: .lowPriority, context: nil)
            cell.imgProfile.layer.cornerRadius = cell.imgProfile.bounds.width/2
            cell.imgProfile.layer.masksToBounds = true
        } else {
            cell.imgProfile.image = UIImage(named: "ic_review_splash_holder")
        }
        cell.lblName.text = dict?.userName ?? ""
        cell.lblDate.text = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "dd MMM yyyy", strDate: dict?.createdAt ?? "")
        cell.lblCommentValue.text = dict?.description ?? ""
        //cell.lblReviewRatting.text = "\(dict?.rate ?? 0)"
        cell.lblReviewRatting.addImageToLabel(imageName: "ic_favorite_black", strText: "(\(dict?.rate ?? 0))", aboveString: "")
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //In HeaderView : HeaderTitle height : 50 // vwReview height : 65
        print(Defaults.object(forKey: remember_userid_key) as? String)
        print(self.mainModelView.userId)
//        if Defaults.object(forKey: remember_userid_key) as? String != self.mainModelView.userId{
//            if mainSegmentController == .singleSegment{
//                return 50+65
//            }
//            return 65
//        }
        if Defaults.object(forKey: remember_userid_key) as? String == self.mainModelView.userId{
            if mainSegmentController == .singleSegment{
                return 50
            }
            return 0
        }else{
            if mainSegmentController == .singleSegment{
                return 50+65
            }
            return 65
        }
        //return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = Bundle.main.loadNibNamed("HeaderViewForSingleSegment", owner: self, options: nil)?[0] as? HeaderViewForSingleSegment
        
        vw?.btnAddComment.addTarget(self, action: #selector(btnAddCommentTapped), for: .touchUpInside)
        
        vw?.btnWriteReview.addTarget(self, action: #selector(btnWriteReviewTapped), for: .touchUpInside)
        
        if mainSegmentController == .multipleSegment{
            vw?.vwTitle.isHidden = true
            
            if selectedSegmetController == .comment{
                vw?.vwAddComment.isHidden = false
                vw?.vwWriteReview.isHidden = true
            }else if selectedSegmetController == .review{
                vw?.vwWriteReview.isHidden = false
                vw?.vwAddComment.isHidden = true
            }
            
        }else if mainSegmentController == .singleSegment{
            vw?.vwTitle.isHidden = false
            
            vw?.vwAddComment.isHidden = true
            vw?.vwWriteReview.isHidden = false
        }
        if Defaults.object(forKey: remember_userid_key) as? String == self.mainModelView.userId{
            vw?.vwAddComment.isHidden = true
            vw?.vwWriteReview.isHidden = true
        }
        return vw
    }
    
}

//MARK: - MXScroll Delegate
extension ReviewCommentVc:MXViewControllerViewSource{
    func viewForMixToObserveContentOffsetChange() -> UIView {
        return theCurrentView.tblReviewCommnet
    }
}
