//
//  TourCollectionCell.swift
//  Yaanba
//
//  Created by Abhay on 06/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class TourCollectionCell: UICollectionViewCell {

    //MARK: - Outlet
    @IBOutlet weak var img: UIImageView!
    
    //MARK: - View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        img.image = UIImage(named: "ic_slider")?.imageFlippedForRightToLeftLayoutDirection()

        // Initialization code
    }

}
