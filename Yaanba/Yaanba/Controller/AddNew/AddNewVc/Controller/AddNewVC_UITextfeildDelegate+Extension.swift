//
//  AddNewVC_UITextfeildDelegate+Extension.swift
//  Yaanba
//
//  Created by Abhay on 15/02/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit

//MARK:- TestFeild Delegate
extension AddNewVc : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if (textField == self.theCurrentView.txtSelectProductCategory) {
            self.view.endEditing(true)
            self.chooseCategoryDD.show()
            return false
        }else if textField == self.theCurrentView.txtChooseType{
            self.chooseTypeDD.show()
            return false
        }else if textField == self.theCurrentView.txtSelectSubCategory{
            if theCurrentView.txtSelectProductCategory.text == ""{
                makeToast(strMessage: "Please select product category".localized)
                return false
            }
            self.chooseSubCategoryDD.show()
            return false
        }
        else if textField == self.theCurrentView.txtSubOfSubCategory{
            if theCurrentView.txtSelectProductCategory.text == ""{
                makeToast(strMessage: "Please select product category".localized)
                return false
            }
            else if theCurrentView.txtSelectSubCategory.text == ""{
                makeToast(strMessage: "Please select sub category".localized)
                return false
            }
            self.chooseSubOfSubCategoryDD.show()
            return false
        }
        else if textField == self.theCurrentView.txtChooseSellType{
            
            if theCurrentView.txtChooseType.text == ""{
                makeToast(strMessage: "Please select choose type.")
                return false
            }
            self.chooseSellTypeDD.show()
            return false
        }else if textField == self.theCurrentView.txtStartDate{
            
            
            self.isStartDateTapped = "0"
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.datePickerCustomMode.pickerMode = .date
            obj.minimumDate = Date()
            obj.isSetMinimumDate = true
            obj.modalTransitionStyle = .crossDissolve
            obj.modalPresentationStyle = .overCurrentContext
            self.present(obj, animated: false, completion: nil)
            
            
            return false
        }else if textField == self.theCurrentView.txtStartTime{
            
            if theCurrentView.txtStartDate.text == ""{
                makeToast(strMessage: "Please_select_start_date_key".localized)
                return false
            }
            
            self.isStartTimeTapped = "0"
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.datePickerCustomMode.pickerMode = .time
            if isCheckCurrentDate(date: selectedStartDate)
            {
                obj.isSetMinimumDate = true
                obj.minimumDate = Date()
            }
            obj.modalTransitionStyle = .crossDissolve
            obj.modalPresentationStyle = .overCurrentContext
            self.present(obj, animated: false, completion: nil)

            
            return false
        }else if textField == self.theCurrentView.txtEndDate{
            
            if theCurrentView.txtStartDate.text == ""{
                makeToast(strMessage: "Please_select_start_date_key".localized)
                return false
            }
            
            self.isStartDateTapped = "1"
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.datePickerCustomMode.pickerMode = .date
            obj.isSetMinimumDate = true
            obj.minimumDate = selectedStartDate
            obj.modalTransitionStyle = .crossDissolve
            obj.modalPresentationStyle = .overCurrentContext
            self.present(obj, animated: false, completion: nil)

            
            return false
        }else if textField == self.theCurrentView.txtEndTime{
            
            if theCurrentView.txtEndDate.text == ""{
                makeToast(strMessage: "Please_select_end_date_key".localized)
                return false
                
            }else  if theCurrentView.txtStartTime.text == ""{
                makeToast(strMessage: "Please_select_start_time_key".localized)
                return false
            }
            
            self.isStartTimeTapped = "1"
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            print("Date : \(generateFromDate(date: theCurrentView.txtStartDate.text!, time: theCurrentView.txtStartTime.text!))")
            obj.datePickerCustomMode.pickerMode = .time
            
            if theCurrentView.txtStartDate.text == theCurrentView.txtEndDate.text{
                
                obj.isSetMinimumDate = true
                obj.minimumDate = generateFromDate(date: theCurrentView.txtStartDate.text!, time: theCurrentView.txtStartTime.text!)
            }
            
            obj.modalTransitionStyle = .crossDissolve
            obj.modalPresentationStyle = .overCurrentContext
            self.present(obj, animated: false, completion: nil)

            return false
        }
        
        return true
    }
    
}
