import Foundation 
import ObjectMapper 

class ProductListModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var productListdata: [ProductListdata]? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		productListdata <- map["data"]
	}
} 

class ProductListdata: Mappable { 

	var id: NSNumber? 
	var productTitle: String? 
	var price: NSNumber?
    var basicPrice: NSNumber?
	var priceBidBarg: String? 
	var productImage: String? 
	var mostPopular: NSNumber?
    var productType: String?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		productTitle <- map["product_title"] 
		price <- map["price"] 
		priceBidBarg <- map["price_bid_barg"] 
		productImage <- map["product_image"] 
		mostPopular <- map["most_popular"]
        basicPrice <- map["basic_price"]
        productType <- map["product_type"]
	}
} 

