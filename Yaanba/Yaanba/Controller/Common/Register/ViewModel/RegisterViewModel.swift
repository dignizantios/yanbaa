//
//  RegisterViewModel.swift
//  Yaanba
//
//  Created by Abhay on 02/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class RegisterViewModel{
    
    //MARK: - View life cycle
    
    //MARK: - view life cycle
    fileprivate weak var theController:RegisterVc!
    
    init(theController:RegisterVc) {
        self.theController = theController
    }
    var userProfile = UIImage(named: "ic_profile_splash_holder")
    var isTemrsSelected = Bool()
    
}
//MARK: Api setup
extension RegisterViewModel {
    
    func ValidateDetails() {
        
        /*if userProfile == UIImage(named: "ic_profile_splash_holder"){
            makeToast(strMessage:"Please_select_profile_image_Key".localized)
        }else*/
        if (self.theController.txtUserName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage:"Please_enter_user_name_Key".localized)
        }else if (self.theController.txtMobileNumber.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
           makeToast(strMessage: "Please_Enter_MobileNumber_Key".localized)
            
        }else if self.theController.txtMobileNumber.text!.count <= 7 {
           makeToast(strMessage: "Mobile_number_should_be_greater_than_7_digits".localized)
            
        }else if ((self.theController)!.txtEmailID.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Enter_EmailId_Key".localized)
        }else if (self.theController!.txtEmailID.text!.isValidEmail() == false)  {
            
            makeToast(strMessage: "Please_Enter_Valid_EmailId_Key".localized)
        }
        else if ((self.theController)!.txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_enter_password_key".localized)
        }else if self.theController.txtPassword.text!.count < 6 {
            makeToast(strMessage: "Please_enter_password_greater_than_6_characters".localized)
        }else if ((self.theController)!.txtConfirmPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Enter_confirm_pwd_key".localized)
        }else if (self.theController.txtConfirmPassword.text!) != self.theController.txtPassword.text! {
            
            makeToast(strMessage: "Password_mismatch_key".localized)
        }else if (!theController.btnCheckUncheck.isSelected){
            makeToast(strMessage: "Please_read_and_agree_terms_condition_key".localized)
        }else{
            self.theController.checkMobileApi()
        }
    }
   
    
    func checkMobileApi(param:[String:Any],success:@escaping(_ token : LogoutModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aCheckMobile
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            //hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LogoutModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                hideLoader()
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                hideLoader()
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
}

