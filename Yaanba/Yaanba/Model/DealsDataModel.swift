import Foundation 
import ObjectMapper 

class DealsDataModel: Mappable { 

	var flag: NSNumber? 
	var dealdata: [Dealdata]? 
	var msg: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		dealdata <- map["data"]
		msg <- map["msg"] 
	}
} 

class Dealdata: Mappable { 

	var id: NSNumber? 
	var startTime: String? 
	var endTime: String? 
	var specialPrice: String? 
	var price: String? 
	var status: String? 
	var createdAt: String? 
	var images: [String]? 
	var title: String? 
	var description: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		startTime <- map["start_time"] 
		endTime <- map["end_time"] 
		specialPrice <- map["special_price"] 
		price <- map["price"] 
		status <- map["status"] 
		createdAt <- map["created_at"] 
		images <- map["images"] 
		title <- map["title"] 
		description <- map["description"] 
	}
} 

