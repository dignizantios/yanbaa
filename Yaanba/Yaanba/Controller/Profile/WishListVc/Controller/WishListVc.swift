//
//  WishListVc.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
protocol ReloadWishListDelegate: class {
    func reloadProduct()
}

class WishListVc: UIViewController {

    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:WishListView = { [unowned self] in
        return self.view as! WishListView
    }()
    lazy var mainModelView: WishListViewModel = {
        return WishListViewModel(theController: self)
    }()
    var emptyStr = ""
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        theCurrentView.setupUI()
        // Do any additional setup after loading the view.
        if Defaults.object(forKey: remember_accesstoken_key) as? String != nil{
            mainModelView.refreshControl.attributedTitle = NSAttributedString(string: "")
            mainModelView.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
            mainModelView.refreshControl.attributedTitle = NSAttributedString(string: "")
            theCurrentView.tblWishlist.addSubview(mainModelView.refreshControl)
        }
        getWishListApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "WISHLIST_key".localized)
    }
    @objc func refresh(sender:AnyObject) {
        self.mainModelView.WishlistArray.removeAll()
        getWishListApi()
        
    }
}
extension WishListVc: ReloadWishListDelegate{
    func reloadProduct() {
        self.mainModelView.WishlistArray.removeAll()
        getWishListApi()
    }
}

//MARK: - TableView

extension WishListVc:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mainModelView.WishlistArray.count == 0
        {
            let lbl = UILabel()
            lbl.text = emptyStr
            lbl.numberOfLines = 5
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeGrayColor
            lbl.font = themeFont(size: 16.0, fontname: .regular)
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return self.mainModelView.WishlistArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTblCell") as! ProductTblCell
        
        //Cell setup changes
        cell.vwMain.backgroundColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 0.8)
        [cell.lblPrice,cell.lblProductName].forEach { (lbl) in
            lbl?.font = themeFont(size: 13, fontname: .bold)
        }
        cell.lblProductName.textColor = UIColor.white
        cell.lblPrice.textColor = UIColor.appThemeYellowColor
        cell.lblDiscount.textColor = UIColor.white
        cell.lblDiscount.backgroundColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)
        let dict = self.mainModelView.WishlistArray[indexPath.row]
        cell.setData(dict: dict)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let dict = self.mainModelView.WishlistArray[indexPath.row]
        print(dict.priceBidBarg)
        if dict.priceBidBarg == nil{
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "ServiceDetailVc") as! ServiceDetailVc
            obj.mainModelView.productId = "\(dict.id ?? 0)"
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
           let obj = homeStoryboard.instantiateViewController(withIdentifier: "MainProductDetailsVc") as! MainProductDetailsVc
            obj.mainModelView.productId = "\(dict.id ?? 0)"
            if dict.priceBidBarg?.lowercased() == "bid".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "price".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "bargeding".lowercased(){
                obj.selectedController = .multipleSegment
            }else{
               obj.selectedController = .singleSegment
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    //If ios 10
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//
//        let deleteAction = UITableViewRowAction(style: .destructive, title: "") { action, indexPath in
//            // Handle delete action
//        }
//
//        deleteAction.backgroundColor = .red
//
//        (UIButton.appearance(whenContainedInInstancesOf: [UIView.self])).setImage(UIImage(named: "ic_delete_white")?.withRenderingMode(.alwaysOriginal), for: .normal)
//        return [deleteAction]
//    }

    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
                //do stuff
                print("row : \(indexPath.row)")
            self.addDeleteWishListApi(productId: self.mainModelView.WishlistArray[indexPath.row].id ?? 0, isSelected: false)
                completionHandler(true)
            })
        action.image = UIImage(named: "ic_delete_white")
        action.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [action])

        return configuration

    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
    

}
//MARK: ApiSetUp
extension WishListVc {
    
    ///---- product details
    
    func getWishListApi() {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            withoutLoginPopup()
            return
        }
        let parameters = [String : Any]()
        print("parameters",parameters)
        mainModelView.getWishlistApi(param: parameters, success: { (productData) in
            self.mainModelView.refreshControl.endRefreshing()
            if productData.flag == 1{
                
                self.mainModelView.WishlistArray = productData.wishlistproductData ?? [WishlistproductData]()
                print(self.mainModelView.WishlistArray)
                self.theCurrentView.tblWishlist.reloadData()
            }
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            self.mainModelView.refreshControl.endRefreshing()
            self.emptyStr = error
            self.theCurrentView.tblWishlist.reloadData()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
//                makeToast(strMessage: error)
            }
        })
    }
    func addDeleteWishListApi(productId:NSNumber,isSelected:Bool) {
        let parameters : [String : Any] =   [ APIKey.key_product_id:productId,
              APIKey.key_action:isSelected == true ? 1 : 0
        ]
        
        mainModelView.addRemoveWishList(param: parameters, success: { (loginData) in
            self.mainModelView.WishlistArray.removeAll()
            self.getWishListApi()
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
}

