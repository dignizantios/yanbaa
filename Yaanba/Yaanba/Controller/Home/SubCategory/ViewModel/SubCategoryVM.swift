//
//  SubCategoryVM.swift
//  Yaanba
//
//  Created by vishal on 27/11/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class SubCategoryVM {
    
    //MARK:- Variables
    fileprivate let theController: SubCategoryVC!
    var subCategoryName = ""
    var subCategoryId = ""
    var dictSubCategoryData: SubCategoryModel? = nil
    
    init(theController: SubCategoryVC) {
        self.theController = theController
    }
    
    func SubCategoryApi(param:[String:Any],success:@escaping() -> Void, failed:@escaping(String) -> Void) {
        
        let url = sub_subcategoryURL
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        print("PARAM:- \(param)")
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                        
                        self.dictSubCategoryData = SubCategoryModel(JSON: data)!
                        success()
                        return
                     }
                     return
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
}
