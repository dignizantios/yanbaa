//
//  ChangePasswordVc.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ChangePasswordVc: UIViewController {
   
    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:ChangePasswordView = { [unowned self] in
        return self.view as! ChangePasswordView
    }()
    lazy var mainModelView: ChangePasswordViewModel = {
        return ChangePasswordViewModel(theController: self)
    }()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "CHANGE_PASSWORD_key".localized)
    }
    
    @IBAction func btnChangePwdAction(_ sender: UIButton) {
        self.mainModelView.ValidateDetails()
    }
    
}
//MARK: ApiSetUp
extension ChangePasswordVc {
    
    ///---- Profile
    func ChangePwdApi() {
        let parameters : [String : Any] =   [ APIKey.key_old_password:self.theCurrentView.txtOldPassword.text!,
                                                  APIKey.key_new_password:self.theCurrentView.txtNewPassword.text!
               ]
        
        mainModelView.ChangePwd(param: parameters, success: { (loginData) in
           // Defaults.removeObject(forKey: remember_email_key)
           // Defaults.removeObject(forKey: remember_password_key)
            Defaults.removeObject(forKey: remember_accesstoken_key)
            Defaults.synchronize()
             let vc  = mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
             let rearNavigation = UINavigationController(rootViewController: vc)
             appDelegate.window?.rootViewController = rearNavigation
            makeToast(strMessage: loginData.msg ?? "")
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    
    
}

