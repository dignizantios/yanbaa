//
//  NotificationTblCell.swift
//  Yaanba
//
//  Created by Abhay on 06/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class NotificationTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDateandTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgProfile.layer.cornerRadius = imgProfile.layer.bounds.width/2
        imgProfile.layer.masksToBounds = true
        
        lblName.textColor = UIColor.appThemeDarkBlueColor
        lblName.font = themeFont(size: 16, fontname: .regular)
        
        lblDateandTime.textColor = UIColor.appThemeGrayColor
        lblDateandTime.font = themeFont(size: 14, fontname: .regular)
        
        lblDescription.textColor = UIColor.white
        lblDescription.font = themeFont(size: 13, fontname: .regular)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(dic:Notificationdata){
        if dic.profileImage !=  "" {
            self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imgProfile.sd_setImage(with: dic.profileImage?.toURL(), placeholderImage: UIImage(named: "ic_review_splash_holder"), options: .lowPriority, context: nil)
            imgProfile.layer.cornerRadius = imgProfile.layer.bounds.width/2
            imgProfile.layer.masksToBounds = true
        } else {
            self.imgProfile.image = UIImage(named: "ic_review_splash_holder")
        }
        lblDateandTime.text = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "dd MMM yyyy hh:mm a", strDate: dic.createdAt ?? "")
        lblName.text = dic.name ?? ""
        lblDescription.text = dic.message ?? ""
        if dic.isRead == 0{
            lblName.font = themeFont(size: 16, fontname: .bold)
            lblDateandTime.font = themeFont(size: 14, fontname: .bold)
            lblDescription.font = themeFont(size: 13, fontname: .bold)
        }else{
            lblName.font = themeFont(size: 16, fontname: .regular)
            lblDateandTime.font = themeFont(size: 14, fontname: .regular)
            lblDescription.font = themeFont(size: 13, fontname: .regular)
        }
        
    }
    
}
