//
//  RegisterVc.swift
//  Yaanba
//
//  Created by Abhay on 02/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import TOCropViewController
import Firebase
import FirebaseAuth
import SwiftyJSON
class RegisterVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblCreateANewAccount: UILabel!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtUserName: CustomTextField!
    
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var lblCountryCode: UITextField!
    @IBOutlet weak var imgCountryCode: CustomImageView!
    
    @IBOutlet weak var lblEmailID: UILabel!
    @IBOutlet weak var txtEmailID: CustomTextField!
    
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtPassword: CustomTextField!
    
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    
    @IBOutlet weak var btnCheckUncheck: UIButton!
    @IBOutlet weak var lblTermsAndCondition: UILabel!
    
    @IBOutlet weak var btnSignUp: CustomButton!
    @IBOutlet weak var lblAlreadyhaveanAccount: UILabel!
    //MARK: - Variable
    
    private var customImagePicker = CustomImagePicker()
    lazy var mainModelView: RegisterViewModel = {
           return RegisterViewModel(theController: self)
       }()
    var countryDialCode: String = "+61"
    var countryName: String = "Australia"
    var countryCode: String = "AU"
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        transperentNavigationBar()
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode

    }
    
}

//MARK: - SetupUI

extension RegisterVc{
    
    func setupUI(){
        
        lblCreateANewAccount.font = themeFont(size: 18, fontname: .bold)
        
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.layer.masksToBounds = true
//        imgProfile.layer.borderWidth = 4.0
//        imgProfile.layer.borderColor = UIColor.appThemeDarkBlueColor.cgColor
        
        [lblUserName,lblMobileNumber,lblEmailID,lblPassword,lblConfirmPassword].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkBlueColor
            lbl?.font = themeFont(size: 11, fontname: .regular)
        }
    [txtUserName,txtEmailID,txtPassword,txtMobileNumber,txtPassword,txtConfirmPassword].forEach { (txt) in
            txt?.font = themeFont(size: 13, fontname: .regular)
            txt?.textColor = UIColor.black
        }
         txtUserName.placeholder = "USER_NAME_key".localized
        txtEmailID.placeholder = "EMAIL_ID_key".localized
        txtPassword.placeholder = "PASSWORD_key".localized
        txtMobileNumber.placeholder = "MOBILE_NUMBER_key".localized
        txtConfirmPassword.placeholder = "CONFIRM_PASSWORD_key".localized
        
        lblCountryCode.font = themeFont(size: 13.0, fontname: .light)
        lblCountryCode.textColor = UIColor.black
        
        btnSignUp.setupThemeButtonUI()
        
        lblTermsAndCondition.textColor = UIColor.appThemeDarkBlueColor
        lblAlreadyhaveanAccount.textColor = UIColor.appThemeDarkBlueColor
        
        lblCreateANewAccount.text = "Create_a_New_Account_key".localized
        
        lblUserName.text = "USER_NAME_key".localized
        lblMobileNumber.text = "MOBILE_NUMBER_key".localized
        lblEmailID.text = "EMAIL_ID_key".localized
        lblPassword.text = "PASSWORD_key".localized
        lblConfirmPassword.text = "CONFIRM_PASSWORD_key".localized
        
        btnSignUp.setTitle("Sign_Up_key".localized, for: .normal)
        
        lblTermsAndCondition.attributedText = setUIForUnderline(normal:"I_Read_and_agree_to_key".localized,underLine:"Terms_&_Conditions_key".localized)
        
        let termsAndCondition = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
        lblTermsAndCondition.addGestureRecognizer(termsAndCondition)
        
        lblAlreadyhaveanAccount.attributedText = setUIForUnderline(normal:"Already_have_an_account?_Key".localized,underLine:"Login_key".localized)
        
        let loginTapGeture = UITapGestureRecognizer(target: self, action: #selector(tapLoginLabel))
        lblAlreadyhaveanAccount.addGestureRecognizer(loginTapGeture)
    }
    
    
    @objc func tapLabel(_ gesture: UITapGestureRecognizer)
    {
        
        let str1 = "I_Read_and_agree_to_key".localized
        let str2 = "Terms_&_Conditions_key".localized
        let str = str1 + " " + str2
        
        let rangeRegister = (str as NSString).range(of: str2, options: .caseInsensitive)

        let checkTermsTapped = gesture.didTapAttributedTextInLabel(lblTermsAndCondition,targetRange:rangeRegister)
        
        if(checkTermsTapped)
        {
            print("Terms and condition")
            let TermsConditionNPrivacypolicyVC = mainStoryboard.instantiateViewController(withIdentifier: "TermsConditionNPrivacypolicyVC") as! TermsConditionNPrivacypolicyVC
            TermsConditionNPrivacypolicyVC.title = "Terms_of_use_key".localized
            self.navigationController?.pushViewController(TermsConditionNPrivacypolicyVC, animated: true)
        }
        
    }
    
    @objc func tapLoginLabel(_ gesture: UITapGestureRecognizer)
    {
        
        let str1 = "Already_have_an_account?_Key".localized
        let str2 = "Login_key".localized
        let str = str1 + " " + str2
        
        let rangeRegister = (str as NSString).range(of: str2, options: .caseInsensitive)

        let checkLoginTapped = gesture.didTapAttributedTextInLabel(lblAlreadyhaveanAccount,targetRange:rangeRegister)
        
        if(checkLoginTapped)
        {
            print("login")
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
}


//MARK: - IBAction method
extension RegisterVc{
    
    @IBAction func btnCheckUncheckTermsAndConditionTapped(_ sender: UIButton) {
        self.btnCheckUncheck.isSelected = !self.btnCheckUncheck.isSelected
    }
    
    @IBAction func btnSignUpTapped(_ sender: UIButton) {
        self.mainModelView.ValidateDetails()
        
    }
    
    @IBAction func btnProfileTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        selectImages()
    }
    @IBAction func btnCountryTapped(_ sender: UIButton) {
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.delegate = self
        self.present(obj, animated:  false, completion: nil)
    }

}
extension RegisterVc:CountryCodeDelegate{
    func CountryCodeDidFinish(data: JSON) {
        self.countryDialCode = data["dial_code"].string ?? ""
        self.countryName = data["name"].string ?? ""
        self.countryCode = data["code"].string ?? ""
        
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode
    }
}
//MARK: - Image selection

extension RegisterVc{
    
    func selectImages()
    {
        customImagePicker.typeOfPicker = .onlyPhoto
        customImagePicker.showImagePicker(fromViewController: self,
                                          navigationColor: UIColor.appThemeYellowColor,
                                          imagePicked: { (response) in
                                            
                                            let theImage = response[UIImagePickerController.InfoKey.originalImage] as! UIImage
                                            
                                            self.openCropVC(Image: theImage)
                                                                                        
                                            
        }, imageCanceled: {
        }, imageRemoved: nil)
    }

}

//MARK: - PhotoGallary

extension RegisterVc:TOCropViewControllerDelegate
{
    func openCropVC(Image:UIImage)
    {
        let cropVC = TOCropViewController(image: Image)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        
        self.present(cropVC, animated: false, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        // self.imgUserPro.image = image
        self.mainModelView.userProfile = image
        imgProfile.image = image
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        // isHaveImage = false
        dismiss(animated: true, completion: nil)
    }
}
//MARK: ApiSetUp
extension RegisterVc:AuthUIDelegate {
    
    ///---- Register //registerApi
    func checkMobileApi() {
        aCheckOnlyMobile
        let parameters : [String : Any] =   [ APIKey.key_mobile_no:self.txtMobileNumber.text!,
                                               APIKey.key_email:self.txtEmailID.text!,
                                               APIKey.key_country_code:self.countryDialCode
        ]
        print("parameters:", parameters)
        mainModelView.checkMobileApi(param: parameters, success: { (checkMobileData) in
            if checkMobileData.flag == 1{
                let firebaseAuth = Auth.auth()
                do {
                    try firebaseAuth.signOut()
                } catch let signOutError as NSError {
                    print ("Error signing out: %@", signOutError)
                }
                
               // showLoader()
                PhoneAuthProvider.provider().verifyPhoneNumber("\(self.lblCountryCode.text!)\(self.txtMobileNumber.text!)", uiDelegate: nil)
                { (verificationID, error) in
                    hideLoader()
                    if ((error) != nil)
                    {
                        // Verification code not sent.
                        print(error?.localizedDescription ?? "")
//                        makeToast(strMessage: "Something_went_wrong_key".localized)
                        makeToast(strMessage: error?.localizedDescription ?? "")
                    } else {
                        // Successful. User gets verification code
                        // Save verificationID in UserDefaults
                        print("verificationId: ",verificationID)
                        Defaults.set(verificationID, forKey: "firebase_verification")
                       Defaults.synchronize()
                        var contryCode = self.lblCountryCode.text!
                        contryCode.remove(at: contryCode.startIndex)

                        //And show the Screen to enter the Code.
                        let param : [String : Any] =   [ APIKey.key_email:self.txtEmailID.text!,
                                                           APIKey.key_password:self.txtPassword.text!,
                                                           APIKey.key_name:self.txtUserName.text!,
                                                           APIKey.key_country_code:contryCode,
                                                           APIKey.key_mobile_no:self.txtMobileNumber.text!,
                            
                        ]
                        var dataArray = [Data]()
                        var nameArray = [String]()
                        var fileNameArray = [String]()
                        let data = self.imgProfile.image! == UIImage(named: "ic_profile_splash_holder") ? UIImage().pngData() : self.imgProfile.image!.pngData()
                        dataArray.append(data!)
                        nameArray.append("profile_image")
                        fileNameArray.append("\(UUID().uuidString).png")
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                        obj.mainModelView.signupDataDict = JSON(param)
                        obj.mainModelView.dataArray = dataArray
                        obj.mainModelView.nameArray = nameArray
                        obj.mainModelView.fileNameArray = fileNameArray
                        obj.mainModelView.enterPhoneNum = "\(self.lblCountryCode.text!)\(self.txtMobileNumber.text!)"
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    
                }
            }else{
                makeToast(strMessage: checkMobileData.msg ?? "")
            }
        }) { (error) in
            //Utility.shared.stopActivityIndicator()
            hideLoader()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        }
    }
}
