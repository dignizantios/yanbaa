//
//  EditProfileView.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class EditProfileView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtUserName: CustomTextField!
    
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    
    @IBOutlet weak var lblEmailID: UILabel!
    @IBOutlet weak var txtEmailID: CustomTextField!
    
    @IBOutlet weak var btnUpdateProfile: CustomButton!
    @IBOutlet weak var btnChangePassword: CustomButton!

    //MARK: - SetupUI
    
    func setupUI(){
        
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.layer.masksToBounds = true
        
        [lblUserName,lblMobileNumber,lblEmailID].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkBlueColor
            lbl?.font = themeFont(size: 11, fontname: .regular)
        }
        [txtUserName,txtEmailID,txtMobileNumber].forEach { (txt) in
            txt?.font = themeFont(size: 13, fontname: .regular)
            txt?.textColor = UIColor.black
        }
        txtUserName.placeholder = "USER_NAME_key".localized
        txtEmailID.placeholder = "EMAIL_ID_key".localized
        txtMobileNumber.placeholder = "MOBILE_NUMBER_key".localized
        
        txtMobileNumber.isUserInteractionEnabled = false
        btnUpdateProfile.setupThemeButtonUI()
        btnChangePassword.setupThemeButtonUI()
        
        lblUserName.text = "USER_NAME_key".localized
        lblMobileNumber.text = "MOBILE_NUMBER_key".localized
        lblEmailID.text = "EMAIL_ID_key".localized
        
        btnUpdateProfile.setTitle("UPDATE_PROFILE_key".localized, for: .normal)
        btnChangePassword.setTitle("CHANGE_PASSWORD_key".localized, for: .normal)
        
    }
}
