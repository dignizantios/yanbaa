//
//  MainProductDetailViewModel.swift
//  Yaanba
//
//  Created by Abhay on 19/02/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class MainProductDetailViewModel{
    
    //MARK: - View life cycle
    fileprivate weak var theController:MainProductDetailsVc!
    
    init(theController:MainProductDetailsVc) {
        self.theController = theController
    }
    var productId = String()
    var productDetailsdata:Productdata?
    weak var reloadWishListdelegate: ReloadWishListDelegate?
    
}
//MARK: Api setup
extension MainProductDetailViewModel {
    
    func productDetailApi(param:[String:Any],success:@escaping(_ token : ProductDetailsModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aProductDetails
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let SubCategoryData = ProductDetailsModel(JSON: data) {
                             success(SubCategoryData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    func addRemoveWishList(param:[String:Any],success:@escaping(_ token : LogoutModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aAddRemoveWishList
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        //showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
          //  hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LogoutModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
}

