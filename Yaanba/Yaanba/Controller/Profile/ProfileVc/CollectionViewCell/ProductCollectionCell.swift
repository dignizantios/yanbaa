//
//  ProductCollectionCell.swift
//  Yaanba
//
//  Created by Abhay on 04/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCollectionCell: UICollectionViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwBackground: CustomView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!    
    @IBOutlet weak var lblPrice: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.vwBackground.backgroundColor = UIColor.appThemeLightGrayColor
        
        imgProduct.layer.cornerRadius = 16
        imgProduct.layer.masksToBounds = true
        
        lblProductName.font = themeFont(size: 11, fontname: .regular)
        
        lblPrice.font = themeFont(size: 11, fontname: .regular)

        // Initialization code
    }

    func setupData(data:ProductListdata){
        if data.productImage !=  "" {
            self.imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imgProduct.sd_setImage(with: data.productImage?.toURL(), placeholderImage: UIImage(named: "ic_home_futter_splash_holder"), options: .lowPriority, context: nil)
        } else {
            self.imgProduct.image = UIImage(named: "ic_home_futter_splash_holder")
        }
        lblProductName.text = data.productTitle ?? ""
        if data.priceBidBarg?.lowercased() == "bid".lowercased(){
            lblPrice.text = "\(data.basicPrice ?? 0) \("KWD".localized)"
        }else{
            lblPrice.text = "\(data.price ?? 0) \("KWD".localized)"
        }
        
    }
}
