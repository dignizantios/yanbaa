//
//  OTPViewModel.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import SwiftyJSON

class OTPViewModel{
    
    //MARK: - View life cycle
    fileprivate weak var theController:OTPVC!
    
    init(theController:OTPVC) {
        self.theController = theController
    }
    var signupDataDict = JSON()
    var dataArray = [Data]()
    var nameArray = [String]()
    var fileNameArray = [String]()
    var enterPhoneNum = String()
    var isComeFromLogin = Bool()
}
//MARK: ApiSetUp
extension OTPViewModel {
    func registerApi(param:[String:Any],image:[Data],NameArray:[String],FileNameArray:[String],success:@escaping(_ token : LoginModel) -> Void, failed:@escaping(String) -> Void) {
        let boundary = "\(UUID().uuidString).png"
        let url = aRegister
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().postUsingMultiPartData(name: url, parameter: param, files: image, withName: NameArray, withFileName: FileNameArray, mimeType: ["image/png"]) { (result) in
            print(result["msg"].stringValue)
            hideLoader()
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result.dictionaryObject {
                    
                    if let registerData = LoginModel(JSON: data) {
                        success(registerData)
                        return
                    }
                }
                return
                
            }
            failed(result["msg"].stringValue)
        }
    }
     func loginApi(param:[String:Any],success:@escaping(_ token : LoginModel) -> Void, failed:@escaping(String) -> Void) {
            
            let url = aLoginWithMobile
            
            guard ReachabilityTest.isConnectedToNetwork() else {
                makeToast(strMessage: "No_internet_connection_available_key".localized)
                return
            }
            
            showLoader()
            WebServices().MakePostAPI(name: url, params: param) { (result) in
                
                hideLoader()
                 switch result {
                 case .success(let value):
                    if value[APIKey.key_flag].intValue == 1 {
                         if let data = value.dictionaryObject {
                             
                             if let LoginData = LoginModel(JSON: data) {
                                 success(LoginData)
                                 return
                             }
                         }
                         return
                         
                     }
                     failed(value["msg"].stringValue)
                     break
                 case .failure(let error):
                     if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                         // no internet connection
                        failed("No_internet_connection_key".localized)
                         
                     } else if let err = error as? URLError, err.code == URLError.timedOut {
                         failed("Request_time_out_Please_try_again".localized)
                     } else {
                         // other failures
                         failed("Something_went_wrong_key".localized)
                     }
                    // Utility.shared.stopActivityIndicator()
                     break
                 }
               
               /* if "flag" == success {
                    
                    
                    completionHandlor()
                }
                else if statusCode == notFound {
                    completionHandlor()
                }
                else if error != nil {
    //                makeToast(strMessage: error?.localized ?? "")
                    completionHandlor()
                }*/
            }
        }
}

