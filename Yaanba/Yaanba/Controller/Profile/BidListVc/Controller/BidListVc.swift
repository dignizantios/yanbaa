//
//  BidListVc.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyJSON

class BidListVc: UIViewController {
    
    
    //MARK: - Variable
    
     lazy var theCurrentView:BidListView = { [unowned self] in
        return self.view as! BidListView
    }()
    lazy var mainModelView: BidListViewModel = {
        return BidListViewModel(theController: self)
    }()
    
    //MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI()

        // Do any additional setup after loading the view.
        if mainModelView.productProfile !=  "" {
            theCurrentView.productImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            theCurrentView.productImg.sd_setImage(with: mainModelView.productProfile.toURL(), placeholderImage: UIImage(named: "ic_home_futter_splash_holder"), options: .lowPriority, context: nil)
        } else {
            theCurrentView.productImg.image = UIImage(named: "ic_home_futter_splash_holder")
        }
        theCurrentView.lblProductName.text = mainModelView.productName
        getBidListApi()
    }

    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "Post_User_key".localized)
    }
    
}

//MARK: - TableView delegate and datasource

extension BidListVc:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mainModelView.bidListdata.count == 0
        {
            let lbl = UILabel()
            lbl.text = "No_list_yet".localized
            lbl.numberOfLines = 5
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeGrayColor
            lbl.font = themeFont(size: 16.0, fontname: .regular)
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            tableView.separatorStyle = .none
            tableView.isScrollEnabled = false
            return 0
        }
        tableView.backgroundView = nil
        return self.mainModelView.bidListdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BidListTblCell") as! BidListTblCell
        cell.contentView.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        cell.lblName.text = self.mainModelView.bidListdata[indexPath.row].userData?.name ?? ""
        cell.lblPrice.text = "$\(self.mainModelView.bidListdata[indexPath.row].bid ?? 0)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = homeStoryboard.instantiateViewController(withIdentifier: "CallNowVc") as! CallNowVc
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.mainModelView.bidData = self.mainModelView.bidListdata[indexPath.row]
        self.present(obj, animated: false, completion: nil)

    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = Bundle.main.loadNibNamed("BidListTblCell", owner: self, options: nil)?[0] as? BidListTblCell
        
        vw?.lblName.text = "Name_key".localized
        vw?.lblPrice.text = "Amount_key".localized
        
        vw?.lblName.textColor = UIColor.white
        vw?.lblPrice.textColor = UIColor.white
        
        vw?.contentView.backgroundColor = UIColor.appThemeDarkBlueColor
        return vw?.contentView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        50
    }
    
}
//MARK: ApiSetUp
extension BidListVc {
    
    ///---- get Bid list
    func getBidListApi() {
        
        if pushData["productId"].stringValue != ""{
             pushData = JSON()
        }
        let parameters : [String : Any] =   [
           APIKey.key_product_id:self.mainModelView.productId
            
        ]
        print("parameters:", parameters)
        
        mainModelView.getBidListApi(param: parameters, success: { (BidListData) in
            
            if BidListData.flag == 1{
                self.mainModelView.bidListdata = BidListData.bidListdata?.bids ?? [Bids]()
                self.theCurrentView.tblBidList.reloadData()
                self.mainModelView.productDetails = BidListData.bidListdata?.description ?? ""
            }
           
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    
    
}

