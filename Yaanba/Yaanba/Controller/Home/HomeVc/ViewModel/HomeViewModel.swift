//
//  HomeViewModel.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import SwiftyJSON

class HomeViewModel{
    
    //MARK: - Outlet
    
    var arrayData : [JSON] = []
    
    //MARK: - view life cycle
    fileprivate weak var theController:HomeVc!
    
    init(theController:HomeVc) {
        self.theController = theController
    }
    /*
    init() {
        setupArray()
    }*/
    
    func setupArray(){
        
        var dict = JSON()
        dict["name"].stringValue = "Clothing"
        dict["selected"].stringValue = "1"
        arrayData.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Shoes"
        dict["selected"].stringValue = "0"
        arrayData.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Bags"
        dict["selected"].stringValue = "0"
        arrayData.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Accessories"
        dict["selected"].stringValue = "0"
        arrayData.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Footwear"
        dict["selected"].stringValue = "0"
        arrayData.append(dict)
        
    }
    
    
}
//MARK: Api setup
extension HomeViewModel {
    
    func CategoryListApi(param:[String:Any],success:@escaping(_ token : CategoryModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aHomeCategory
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let CategoryData = CategoryModel(JSON: data) {
                             success(CategoryData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    func notificationCountApi(urls:String,param:[String:Any],success:@escaping(_ token : LoginModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aNotificationCount
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        //showLoader()
        WebServices().MakeGetAPI(name: url, params: param) { (result) in
            
            //hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let notificationCountData = LoginModel(JSON: data) {
                             success(notificationCountData)
                             return
                         }
                     }
                     return
                     
                 }
                failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
}

