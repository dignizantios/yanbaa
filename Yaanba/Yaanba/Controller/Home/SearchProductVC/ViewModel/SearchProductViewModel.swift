//
//  SearchProductViewModel.swift
//  Yaanba
//
//  Created by Abhay on 29/02/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation


class SearchProductViewModel{
    
    //MARK: - view life cycle
    /*
    init() {
        
    }*/
    //MARK:- Variables
    fileprivate weak var theController:SearchProductVC!
    
    init(theController:SearchProductVC) {
        self.theController = theController
    }
    var searchTxt = String()
    var searchListdata = [Searchdata]()
}
//MARK: Api setup
extension SearchProductViewModel {
    
    
    func searchApi(param:[String:Any],success:@escaping(_ token : SearchListModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aSearchProduct
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        //showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            //hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let SearchData = SearchListModel(JSON: data) {
                             success(SearchData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}

