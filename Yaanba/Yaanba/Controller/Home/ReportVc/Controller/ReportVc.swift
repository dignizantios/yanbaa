//
//  ReportVc.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ReportVc: UIViewController {

    //MARK: - Variable
    
    lazy var theCurrentView:ReportView = { [unowned self] in
        return self.view as! ReportView
    }()
    
    lazy var mainModelView: ReportViewModel = {
        return ReportViewModel(theController: self)
    }()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI(thedelegate: self)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: "Report_key".localized)
    }
    //MARK:- button actions
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        self.mainModelView.ValidateDetails()
    }
    
}
//MARK: ApiSetUp
extension ReportVc {
    
    ///---- Add comment
    func addReportApi() {
        guard Defaults.object(forKey: remember_accesstoken_key) as? String != nil else {
            withoutLoginPopup()
            return
        }
        let parameters : [String : Any] =   [
           APIKey.key_description:self.theCurrentView.txtDescription.text!,
           APIKey.key_product_id:self.mainModelView.productId
            
        ]
        print("parameters:", parameters)
        
        mainModelView.addReportApi(param: parameters, success: { (addCoomentData) in
            
            if addCoomentData.flag == 1{
                //self.mainModelView.ProductAdddelegate?.reloadAddProduct()
                self.navigationController?.popViewController(animated: true)
            }
           // makeToast(strMessage: addCoomentData.msg ?? "")
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    
    
}

