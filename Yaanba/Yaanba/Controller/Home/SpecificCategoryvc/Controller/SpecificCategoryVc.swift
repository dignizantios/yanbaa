//
//  SpecificCategoryVc.swift
//  Yaanba
//
//  Created by Abhay on 08/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class SpecificCategoryVc: UIViewController {

    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:SpecificCategoryView = { [unowned self] in
        return self.view as! SpecificCategoryView
    }()
    lazy var mainModelView: SpecificCategoryViewModel = {
           return SpecificCategoryViewModel(theController: self)
       }()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI()
        // Do any additional setup after loading the view.
        getProductListApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarBackButton(titleText: self.mainModelView.subCategoryName.uppercased())
    }

}

//MARK: - CollectionView

extension SpecificCategoryVc:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.mainModelView.productListdata.count == 0
        {
            let lbl = UILabel()
            lbl.text = "No_list_yet".localized
            lbl.numberOfLines = 5
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeGrayColor
            lbl.font = themeFont(size: 16.0, fontname: .regular)
            lbl.center = collectionView.center
            collectionView.backgroundView = lbl
            return 0
        }
        collectionView.backgroundView = nil
        return self.mainModelView.productListdata.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionCell
        let dict = self.mainModelView.productListdata[indexPath.row]
        cell.setupData(data: dict)
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = self.mainModelView.productListdata[indexPath.row]
        if dict.priceBidBarg == nil && dict.productType?.lowercased() == "Service".localized.lowercased(){
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "ServiceDetailVc") as! ServiceDetailVc
            obj.mainModelView.productId = "\(self.mainModelView.productListdata[indexPath.row].id ?? 0)"
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let obj = homeStoryboard.instantiateViewController(withIdentifier: "MainProductDetailsVc") as! MainProductDetailsVc
            obj.mainModelView.productId = "\(dict.id ?? 0)"
            if dict.priceBidBarg?.lowercased() == "bid".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "price".lowercased(){
                obj.selectedController = .singleSegment
            }else if dict.priceBidBarg?.lowercased() == "bargeding".lowercased(){
                obj.selectedController = .multipleSegment
            }else{
               obj.selectedController = .singleSegment
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.size.width)/2, height: (collectionView.frame.size.width)/2)
        
     //   return CGSize(width: collectionView.bounds.width/3, height: 180) // Product size
    
    }
    
}
//MARK: ApiSetUp
extension SpecificCategoryVc {
    
    ///---- Get Product list
    func getProductListApi() {
        
//        let parameters : [String : Any] =   [ APIKey.key_sub_category_id:self.mainModelView.subCategoryId]
        let parameters : [String : Any] =   [ APIKey.key_sub_subcategory_id:self.mainModelView.subCategoryId]
        
        print("parameters:", parameters)
        
        mainModelView.productListApi(param: parameters, success: { (productListData) in
            self.mainModelView.productListdata = productListData.productListdata ??  [ProductListdata]()
            self.theCurrentView.collectionCategory.reloadData()
            self.theCurrentView.lblQuntity.text = "(\(self.mainModelView.productListdata.count))"
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
//                makeToast(strMessage: error)
            }
        })
    }
}

