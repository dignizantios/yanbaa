//
//  ChangePasswordViewModel.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class ChangePasswordViewModel {
    
    //MARK: - View life cycle
    fileprivate weak var theController:ChangePasswordVc!
    
    init(theController:ChangePasswordVc) {
        self.theController = theController
    }
}
//MARK: Api setup
extension ChangePasswordViewModel {
    
    func ValidateDetails() {
        
        if ((self.theController.view as? ChangePasswordView)!.txtOldPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Enter_old_pwd_key".localized)
        }else if ((self.theController.view as? ChangePasswordView)!.txtNewPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please_Enter_new_pwd_key".localized)
        }else if ((self.theController.view as? ChangePasswordView)!.txtConfirmPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Enter_confirm_pwd_key".localized)
        }
         else if ((self.theController.view as? ChangePasswordView)!.txtNewPassword.text! != (self.theController.view as? ChangePasswordView)!.txtConfirmPassword.text!)  {
            makeToast(strMessage: "Password_mismatch_key".localized)
        }
        else {
            
            self.theController.ChangePwdApi()
            
        }
    }
    func ChangePwd(param:[String:Any],success:@escaping(_ token : LogoutModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aChnagePassword
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LogoutModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}

