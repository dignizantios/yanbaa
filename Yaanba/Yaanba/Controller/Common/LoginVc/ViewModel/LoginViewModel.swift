//
//  LoginViewModel.swift
//  Yaanba
//
//  Created by Abhay on 02/01/20.

//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation


class LoginViewModel{
    
    //MARK: - view life cycle
    /*
    init() {
        
    }*/
    //MARK:- Variables
    fileprivate weak var theController:LoginVc!
    
    init(theController:LoginVc) {
        self.theController = theController
    }
}
//MARK: Api setup
extension LoginViewModel {
    
    func ValidateDetails() {
        
//        if ((self.theController)!.txtEmailID.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
//
//            makeToast(strMessage: "Please_Enter_EmailId_Key".localized)
//        }
//         else if (self.theController!.txtEmailID.text!.isValidEmail() == false)  {
//
//            makeToast(strMessage: "Please_Enter_Valid_EmailId_Key".localized)
//        }
        if ((self.theController)!.txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Enter_EmailId_Key".localized)
        }
         else if (self.theController!.txtEmail.text!.isValidEmail() == false)  {
            
            makeToast(strMessage: "Please_Enter_Valid_EmailId_Key".localized)
        }
        else if ((self.theController)!.txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_enter_password_key".localized)
        }
        else {
            
            self.theController.loginApi()
            
        }
    }
    func ValidateOTPSend() {
        
        if ((self.theController)!.lblCountryCode.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Select_CountryCode_Key".localized)
        }
        else if ((self.theController)!.txtEmailID.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Select_CountryCode_Key".localized)
        }else if self.theController.txtEmailID.text!.count <= 7 {
           makeToast(strMessage: "Mobile_number_should_be_greater_than_7_digits".localized)
            
        }else {
            //self.theController.loginApi()
            self.theController.checkMobileApi()
        }
    }
    func loginApi(param:[String:Any],success:@escaping(_ token : LoginModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aLogin
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LoginModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
           
           /* if "flag" == success {
                
                
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
//                makeToast(strMessage: error?.localized ?? "")
                completionHandlor()
            }*/
        }
    }
    func checkMobileApi(param:[String:Any],success:@escaping(_ token : LogoutModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aCheckOnlyMobile
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            //hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LogoutModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                hideLoader()
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                hideLoader()
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
}

