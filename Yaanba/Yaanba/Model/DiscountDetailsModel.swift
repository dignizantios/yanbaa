import Foundation 
import ObjectMapper 

class DiscountDetailsModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var discountDetaildata: DiscountDetaildata? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		discountDetaildata <- map["data"]
	}
} 

class DiscountDetaildata: Mappable { 

	var id: NSNumber? 
	var discount: NSNumber? 
	var bannerImage: String? 
	var email: String? 
	var contact: String? 
	var status: String? 
	var createdAt: String? 
	var companyName: String? 
	var description: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		discount <- map["discount"] 
		bannerImage <- map["banner_image"] 
		email <- map["email"] 
		contact <- map["contact"] 
		status <- map["status"] 
		createdAt <- map["created_at"] 
		companyName <- map["company_name"] 
		description <- map["description"] 
	}
} 

