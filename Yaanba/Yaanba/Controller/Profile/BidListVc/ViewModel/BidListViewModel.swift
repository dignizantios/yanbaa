//
//  BidListViewModel.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class BidListViewModel{
    
    fileprivate weak var theController:BidListVc!
    
    init(theController:BidListVc) {
        self.theController = theController
    }
    var productId = String()
    var bidListdata = [Bids]()
    var productProfile = String()
    var productName = String()
    var productDetails = String()
}
//MARK: Api setup
extension BidListViewModel {
    
    func getBidListApi(param:[String:Any],success:@escaping(_ token : BidListModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aBidList
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let BidListData = BidListModel(JSON: data) {
                             success(BidListData)
                             return
                         }
                     }
                     return
                     
                 }
                 failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}

