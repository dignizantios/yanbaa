//
//  DatePickerVC.swift
//  Swipy
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import UIKit

protocol datePickerDelegate {
    func setDateValue(dateValue : Date,selectedType: Int)
}

struct datepickerMode
{
    var pickerMode : UIDatePicker.Mode
    
    init(customPickerMode:UIDatePicker.Mode)
    {
        self.pickerMode = customPickerMode
    }
}

class DatePickerVC: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet var vwButtonHeader : UIView!
    @IBOutlet var btnCancel : UIButton!
    @IBOutlet var btnDone : UIButton!
    @IBOutlet var datePicker : UIDatePicker!
    @IBOutlet weak var vwTappedGesture: UIView!
    
    //MARK: - Variables
    var datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
    var pickerDelegate : datePickerDelegate?
    var isSetMaximumDate = false
    var isSetMinimumDate = false

    var maximumDate = Date()
    var minimumDate = Date()
    
    var type = 0 // type  = 0 for date and Type = 1 for time
    
//    var isSetDate = false
//    var setDateValue : Date?
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        SetupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
//MARK: - Setup UI
extension DatePickerVC
{
    func SetupUI()
    {
        [btnDone,btnCancel].forEach({
            $0?.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
            $0?.titleLabel?.font = themeFont(size: 16, fontname:.semibold)
        })
        
        btnDone.setTitle("Done_key".localized, for: .normal)
        btnCancel.setTitle("Cancel_key".localized, for: .normal)
        
        vwButtonHeader.backgroundColor = UIColor.appThemeYellowColor
        
        datePicker.datePickerMode = datePickerCustomMode.pickerMode
        datePicker.date = Date()
        
        if(isSetMaximumDate)
        {
            datePicker.maximumDate = maximumDate
        }
        if(isSetMinimumDate)
        {
            datePicker.minimumDate = minimumDate
        }
        
//        if(isSetDate)
//        {0
//            datePicker.setDate(setDateValue ?? Date(), animated: false)
//        }
//
//        if datePickerCustomMode.pickerMode == .time
//        {
//            datePicker.minuteInterval = 30
//        }
        
        if datePickerCustomMode.pickerMode == .date{
            self.type = 0
        }else{
            self.type = 1
        }
        
        let tappedGesture = UITapGestureRecognizer(target: self, action: #selector(btnCancelAction(_:)))
        self.vwTappedGesture.addGestureRecognizer(tappedGesture)
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        
        
    }

}
//MARK: - Button Action
extension DatePickerVC
{
    @IBAction func btnDoneAction(_ sender : UIButton)
    {
        pickerDelegate?.setDateValue(dateValue: datePicker.date,selectedType:type)
        self.dismiss(animated: true, completion: nil)

    }
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
