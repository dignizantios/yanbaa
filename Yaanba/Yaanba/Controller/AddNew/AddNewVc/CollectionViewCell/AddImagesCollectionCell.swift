//
//  AddImagesCollectionCell.swift
//  Yaanba
//
//  Created by Abhay on 09/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class AddImagesCollectionCell: UICollectionViewCell {

    //MARK: - Variable
    
    @IBOutlet weak var imgSelected: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    
    weak var DealImgCounterDelegate: ReloadDealImgCountDelegate?
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

}
