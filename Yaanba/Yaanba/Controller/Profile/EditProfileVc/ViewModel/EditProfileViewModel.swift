//
//  EditProfileViewModel.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class EditProfileViewModel {
    
    //MARK: - View life cycle
    fileprivate weak var theController:EditProfileVc!
    
    init(theController:EditProfileVc) {
        self.theController = theController
    }
    var profileData:Logindata?
    var userProfile = UIImage(named: "ic_profile_splash_holder")
}
//MARK: Api setup
extension EditProfileViewModel {
    
    func ValidateDetails() {
        
        if userProfile == UIImage(named: "ic_profile_splash_holder") {
            makeToast(strMessage:"Please_select_profile_image_Key".localized)
        }else if ((self.theController.view as? EditProfileView)!.txtUserName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Enter_Username_Key".localized)
        }else if ((self.theController.view as? EditProfileView)!.txtMobileNumber.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: "Please_Enter_MobileNumber_Key".localized)
        }else if ((self.theController.view as? EditProfileView)!.txtEmailID.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            
            makeToast(strMessage: "Please_Enter_EmailId_Key".localized)
        }
         else if ((self.theController.view as? EditProfileView)!.txtEmailID.text!.isValidEmail() == false)  {
            
            makeToast(strMessage: "Please_Enter_Valid_EmailId_Key".localized)
        }
        else {
            
            self.theController.updateProfileApi()
            
        }
    }
    func profileApi(param:[String:Any],success:@escaping(_ token : LoginModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aUserProfile
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        
        showLoader()
        WebServices().MakePostAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let LoginData = LoginModel(JSON: data) {
                             success(LoginData)
                             return
                         }
                     }
                     return
                     
                 }
                failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    func updateProfileApi(param:[String:Any],image:[Data],NameArray:[String],FileNameArray:[String],success:@escaping(_ token : LoginModel) -> Void, failed:@escaping(String) -> Void) {
        let _ = "\(UUID().uuidString).png"
        let url = aUpdateProfile
        
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        
        showLoader()
        WebServices().postUsingMultiPartData(name: url, parameter: param, files: image, withName: NameArray, withFileName: FileNameArray, mimeType: ["image/png"]) { (result) in
            print(result["msg"].stringValue)
            hideLoader()
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result.dictionaryObject {
                    
                    if let registerData = LoginModel(JSON: data) {
                        success(registerData)
                        return
                    }
                }
                return
                
            }
            failed(result["msg"].stringValue)
        }
       
       
    }
     
}

