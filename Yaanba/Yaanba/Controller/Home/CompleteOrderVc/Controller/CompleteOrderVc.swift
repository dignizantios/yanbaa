//
//  CompleteOrderVc.swift
//  Yaanba
//
//  Created by Abhay on 18/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class CompleteOrderVc: UIViewController {
   
    //MARK: - Variable
    
    fileprivate lazy var theCurrentView:CompleteOrderView = { [unowned self] in
        return self.view as! CompleteOrderView
    }()
    
    //MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func btnContinueTapped(_ sender: CustomButton) {
        self.navigationController?.popToRootViewController(animated: false)
        appDelegate.objCustomTabBar.selectedIndex = 0
    }
    
}

