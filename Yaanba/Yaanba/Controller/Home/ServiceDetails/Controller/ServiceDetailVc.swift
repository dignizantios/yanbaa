//
//  ServiceDetailVc.swift
//  Yaanba
//
//  Created by Abhay on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyJSON

class ServiceDetailVc: UIViewController {

    //MARK: - Variable
    
    lazy var theCurrentView:ServiceDetailView = { [unowned self] in
        return self.view as! ServiceDetailView
    }()
    lazy var mainModelView: ServiceDetailViewModel = {
        return ServiceDetailViewModel(theController: self)
    }()
    var btnFav = UIButton()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI(thedelegate:self)
        // Do any additional setup after loading the view.
        getProductDetailsApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationbarBackButton(titleText: "Detail_View_key".localized)
        
        btnFav.setImage(UIImage(named: "ic_like_unselect"), for: .normal)
        btnFav.setImage(UIImage(named: "ic_like_select"), for: .selected)
        btnFav.addTarget(self, action: #selector(btnFavUnfavouriteTapped), for: .touchUpInside)
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnFav)
       
    }
    
    @objc func btnFavUnfavouriteTapped(){
        //btnFav.isSelected = !btnFav.isSelected
         self.addDeleteWishListApi(isSelected: !btnFav.isSelected)
    }
    
}

//MARK: - IBAction method

extension ServiceDetailVc{
    
    @IBAction func btnReportTapped(_ sender: UIButton) {
        let obj = homeStoryboard.instantiateViewController(withIdentifier: "ReportVc") as! ReportVc
        print(self.mainModelView.productId)
        obj.mainModelView.productId = self.mainModelView.productId
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnCallNowTapped(_ sender: UIButton) {
        let actionsheet = UIAlertController(title: nil, message: "Make_a_call_key".localized, preferredStyle: UIAlertController.Style.actionSheet)

        actionsheet.addAction(UIAlertAction(title: "Main_number_key".localized.uppercased(), style: UIAlertAction.Style.default, handler: { (action) -> Void in
            if let call = self.mainModelView.productDetailsdata?.productCall!,
                let url = URL(string: "tel://+\(call)"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            }
        }))

        actionsheet.addAction(UIAlertAction(title: "Alternative_number_key".localized.uppercased(), style: UIAlertAction.Style.default, handler: { (action) -> Void in
            if let call = self.mainModelView.productDetailsdata?.alternativeCall!,
                let url = URL(string: "tel://+\(call)"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            }
        }))
        actionsheet.addAction(UIAlertAction(title: "Cancel_key".localized.uppercased(), style: UIAlertAction.Style.cancel, handler: { (action) -> Void in

        }))
        self.present(actionsheet, animated: true, completion: nil)
        
    }
    
}

//MARK: - CollectionView Delegate

extension ServiceDetailVc:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.mainModelView.productDetailsdata?.productMultiImage?.count ?? 0 > 0{
            return self.mainModelView.productDetailsdata!.productMultiImage!.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImagesCollectionCell", for: indexPath) as! AddImagesCollectionCell
        cell.btnDelete.isHidden = true
        if self.mainModelView.productDetailsdata?.productMultiImage?.count ?? 0 > 0{
            let dict = self.mainModelView.productDetailsdata?.productMultiImage?[indexPath.row]
            if dict?.productImage !=  nil {
                cell.imgSelected.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.imgSelected.sd_setImage(with: dict?.productImage?.toURL(), placeholderImage: UIImage(named: "ic_upload_picture"), options: .lowPriority, context: nil)
            } else {
                cell.imgSelected.image = UIImage(named: "ic_upload_picture")
            }
            
        }else{
            cell.imgSelected.image = UIImage(named: "ic_upload_picture")
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width/4, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //theCurrentView.imgMainHeader.image = UIImage(named: self.arrImages[indexPath.row])
        if self.mainModelView.productDetailsdata?.productMultiImage?[indexPath.row].productImage !=  nil {
            theCurrentView.imgMainHeader.sd_imageIndicator = SDWebImageActivityIndicator.gray
            theCurrentView.imgMainHeader.sd_setImage(with: self.mainModelView.productDetailsdata?.productMultiImage?[indexPath.row].productImage?.toURL(), placeholderImage: UIImage(named: "ic_detail_view_screen_big_place_holder"), options: .lowPriority, context: nil)
        } else {
            theCurrentView.imgMainHeader.image = UIImage(named: "ic_detail_view_screen_big_place_holder")
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        let totalCellWidth = 80 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)

        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset

        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)

    }
    
}
//MARK: ApiSetUp
extension ServiceDetailVc {
    
    ///---- product details
    
    func getProductDetailsApi() {
        if pushData["productId"].stringValue != ""{
            pushData = JSON()
        }
        let parameters : [String : Any] =   [ APIKey.key_product_id:self.mainModelView.productId
        ]
        print("parameters",parameters)
        mainModelView.productServicesDetailApi(param: parameters, success: { (productData) in
            if productData.flag == 1{
                
                self.mainModelView.productDetailsdata = productData.productdata!
                self.theCurrentView.lblServiceName.text = self.mainModelView.productDetailsdata?.productTitle ?? ""
                self.theCurrentView.lblServiceDescription.text = self.mainModelView.productDetailsdata?.description ?? ""
                //self.theCurrentView.btnCallNow.setTitle(self.mainModelView.productDetailsdata?.productCall ?? "", for: .normal)
                self.theCurrentView.btnCallNow.setTitle("CALL_NOW_key".localized, for: .normal)
                
                self.btnFav.isSelected = self.mainModelView.productDetailsdata?.isWishlist ?? false
                if Defaults.object(forKey: remember_userid_key) as? String != self.mainModelView.productDetailsdata?.userId?.stringValue{
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.btnFav)
                }
                self.mainModelView.productDetailsdata!.productMultiImage = productData.productdata!.productMultiImage
                print(self.mainModelView.productDetailsdata!.productMultiImage?.count)
                self.theCurrentView.collectionImages.reloadData()
                if self.mainModelView.productDetailsdata?.productImage !=  nil {
                    self.theCurrentView.imgMainHeader.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.theCurrentView.imgMainHeader.sd_setImage(with: self.mainModelView.productDetailsdata?.productImage?.toURL(), placeholderImage: UIImage(named: "ic_detail_view_screen_big_place_holder"), options: .lowPriority, context: nil)
                } else {
                    self.theCurrentView.imgMainHeader.image = UIImage(named: "ic_detail_view_screen_big_place_holder")
                }

                if Defaults.object(forKey: remember_userid_key) as? String == self.mainModelView.productDetailsdata?.userId?.stringValue {
                    self.theCurrentView.btnCallNow.isHidden = true
                    self.theCurrentView.btnReportNow.isHidden = true
                }
            }
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    func addDeleteWishListApi(isSelected:Bool) {
        let parameters : [String : Any] =   [ APIKey.key_product_id:self.mainModelView.productId,
              APIKey.key_action:isSelected == true ? 1 : 0
        ]
        
        mainModelView.addRemoveWishList(param: parameters, success: { (loginData) in
            self.btnFav.isSelected = isSelected
            makeToast(strMessage: loginData.msg ?? "")
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
}

