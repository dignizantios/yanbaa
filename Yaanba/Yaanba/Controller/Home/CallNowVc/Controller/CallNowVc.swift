//
//  CallNowVc.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class CallNowVc: UIViewController {

    //MARK: - Variable
    
    lazy var theCurrentView:CallNowView = { [unowned self] in
        return self.view as! CallNowView
    }()
    lazy var mainModelView: CallNowViewModel = {
        return CallNowViewModel(theController: self)
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI(thedelegate:self)
        // Do any additional setup after loading the view.
    }

    @IBAction func btnCallNowTapped(_ sender: CustomButton) {
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
