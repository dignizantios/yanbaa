//
//  ProductUpperDetailsView.swift
//  Yaanba
//
//  Created by Abhay on 10/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class ProductUpperDetailsView: UIView {

    //MARK: - Outlet
    @IBOutlet weak var vwScrollHeader: UIScrollView!
    @IBOutlet weak var imgMainHeader: UIImageView!
    @IBOutlet weak var collectionViewOtherImages: UICollectionView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblProdctDescription: UILabel!
    
    @IBOutlet weak var btnReport: UIButton!
    
    @IBOutlet weak var vwBidNow: UIView!
    @IBOutlet weak var btnBidNow: CustomButton!
    @IBOutlet weak var vwCallNow: UIView!
    @IBOutlet weak var btnCallNow: CustomButton!
    
    @IBOutlet weak var vwBidCompare: UIView!
    @IBOutlet weak var lblYourBid: UILabel!
    
    @IBOutlet weak var lblYourBidValue: UILabel!
    @IBOutlet weak var lblHeighestBid: UILabel!
    
    @IBOutlet weak var lblHeighstBidValue: UILabel!
    func setupUI(thedelegate:ProductUpperDetailsVc){
        
        collectionViewOtherImages.register(UINib(nibName: "AddImagesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddImagesCollectionCell")
        
        [lblProductName,lblProductPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkBlueColor
            lbl?.font = themeFont(size: 17, fontname: .semibold)
        }
        
        lblProdctDescription.textColor = UIColor.appThemeGrayColor
        lblProdctDescription.font = themeFont(size: 12, fontname: .regular)
        
        [btnCallNow,btnBidNow].forEach { (btn) in
            btn?.setupThemeButtonUI()
        }
        
        btnCallNow.setTitle("CALL_NOW_key".localized, for: .normal)
        btnBidNow.setTitle("BID_NOW_key".localized, for: .normal)
     
        [lblYourBid,lblHeighestBid].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkBlueColor
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        lblYourBid.text = "Your_Bid_key".localized
        lblHeighestBid.text = "Heighest_Bid_key".localized
        
        btnCallNow.setImage(UIImage(named: "ic_telephone")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
         btnCallNow.setTitle("CALL_NOW_key".localized, for: .normal)
        
        if DeviceLanguage == "ar"{
            btnCallNow.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -50)
        }else{
            btnCallNow.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 50)
        }
        
        if thedelegate.mainModelView.productDetailsdata?.productImage !=  nil {
            imgMainHeader.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgMainHeader.sd_setImage(with: thedelegate.mainModelView.productDetailsdata?.productImage?.toURL(), placeholderImage: UIImage(named: "ic_detail_view_screen_big_place_holder"), options: .lowPriority, context: nil)
        } else {
            imgMainHeader.image = UIImage(named: "ic_detail_view_screen_big_place_holder")
        }
        lblProductName.text = thedelegate.mainModelView.productDetailsdata?.productTitle ?? ""
        lblProductName.text = thedelegate.mainModelView.productDetailsdata?.productTitle ?? ""
        if thedelegate.mainModelView.productDetailsdata?.priceBidBarg?.lowercased() == "bid".lowercased(){
            lblProductPrice.text = "\("KWD".localized.uppercased()) \(thedelegate.mainModelView.productDetailsdata?.basicPrice ?? 0) "
        }else{
            lblProductPrice.text = "\("KWD".localized.uppercased()) \(thedelegate.mainModelView.productDetailsdata?.price ?? 0) "
        }
        lblProdctDescription.text = thedelegate.mainModelView.productDetailsdata?.description ?? ""
       // btnCallNow.setTitle(thedelegate.mainModelView.productDetailsdata?.productCall ?? "", for: .normal)
        print(thedelegate.mainModelView.productDetailsdata?.bid)
        if thedelegate.mainModelView.productDetailsdata?.bid ?? false{
            thedelegate.setBidNowHidden(hidden: true)
            thedelegate.setBidCompareHidden(hidden: false)
            self.lblYourBidValue.text = "KWD".localized+" \(thedelegate.mainModelView.productDetailsdata?.yourBid ?? 0)"
            self.lblHeighstBidValue.text = "KWD".localized+" \(thedelegate.mainModelView.productDetailsdata?.heighestBid ?? 0)"
        }else{
            thedelegate.setBidNowHidden(hidden: false)
            thedelegate.setBidCompareHidden(hidden: true)
            
        }
    }
    
}
