//
//  ViewController.swift
//  Yaanba
//
//  Created by Abhay on 01/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblMainHeader: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    @IBOutlet weak var collectionTour: UICollectionView!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    //MARK: - Variable
    
    var timer = Timer()
    var array = [String]()
    var selectedIndex = 0
    lazy var mainModelView: TourViewModel = {
           return TourViewModel(theController: self)
    }()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(Defaults.object(forKey: remember_accesstoken_key))
        if Defaults.object(forKey: remember_accesstoken_key) != nil{
            /*appDelegate.objCustomTabBar = TabbarController()
            self.navigationController?.pushViewController(appDelegate.objCustomTabBar, animated: false)
 */
            appDelegate.loadHomeScreen()
            self.navigationController?.navigationBar.isHidden = true
        }
        setupUI()
        // Do any additional setup after loading the view.
        getTourApi()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        lblMainHeader.text = "Shopping_is_the_Best_Medicine_key".localized
        lblSubHeader.text = "Shopping_for_your_key".localized
        
        lblMainHeader.font = themeFont(size: 16, fontname: .regular)
        lblSubHeader.font = themeFont(size: 20, fontname: .bold)
        
        lblMainHeader.textColor = UIColor.appThemeDarkBlueColor
        lblSubHeader.textColor = UIColor.appThemeGrayColor
        
        btnNext.setTitle("NEXT_key".localized, for: .normal)
        btnSkip.setTitle("SKIP_key".localized, for: .normal)
        
        [btnNext,btnSkip].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeDarkBlueColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        }
        
        self.collectionTour.register(UINib(nibName: "TourCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TourCollectionCell")
        
    }
    
    @objc func moveToLogin(){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        self.navigationController?.pushViewController(obj, animated: true)
    }

    @IBAction func btnNextTapped(_ sender: UIButton) {
        
        if selectedIndex == self.mainModelView.tourData.count-1{
            moveToLogin()
        }else{
            selectedIndex += 1
            collectionTour.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: .right, animated: true)
            if selectedIndex == self.mainModelView.tourData.count-1{
                btnNext.setTitle("FINISH_key".localized, for: .normal)
            }
        }
    }
    
    @IBAction func btnSkipTapped(_ sender: UIButton) {
        moveToLogin()
    }
    
}

//MARK: - CollectionView

extension ViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.mainModelView.tourData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TourCollectionCell", for: indexPath) as! TourCollectionCell
        let dict = self.mainModelView.tourData[indexPath.row]
        if dict.image !=  "" {
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img.sd_setImage(with: dict.image?.toURL(), placeholderImage: UIImage(named: "ic_home_futter_splash_holder"), options: .lowPriority, context: nil)
        } else {
            cell.img.image = UIImage(named: "ic_home_futter_splash_holder")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.width)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        let dic = self.mainModelView.tourData[selectedIndex]
        lblMainHeader.text = dic.title ?? ""
        lblSubHeader.text = dic.description ?? ""
        
    }
    
}
//MARK: ApiSetUp
extension ViewController {
    
    ///---- Get product
    func getTourApi() {
        let parameters = [String : Any]()
        print("parameters",parameters)
        mainModelView.getTourApi(param: parameters, success: { (TourData) in
            if TourData.flag == 1{
                self.mainModelView.tourData =  TourData.tourdata ?? [Tourdata]()
                self.collectionTour.reloadData()
            }
            
        }, failed: { (error) in
            //Utility.shared.stopActivityIndicator()
            if error == "token_expired"{
                self.logoutApiCall()
            }else{
                makeToast(strMessage: error)
            }
        })
    }
    
}

