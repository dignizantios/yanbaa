//
//  NSNotification+Extension.swift
//  PGCME
//
//  Created by Jaydeep on 14/12/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {
    static let didRefreshSideMenu = Notification.Name("refreshSideMenu")
}
