//
//  ProfileView.swift
//  Yaanba
//
//  Created by Abhay on 04/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ProfileView: UIView {

    //MARK: - Outlet
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMyProduct: UILabel!
    @IBOutlet weak var vwMyProduct: UIView!
    @IBOutlet weak var collectionProduct: UICollectionView!
    @IBOutlet weak var btnUpdateProfile: CustomButton!
    @IBOutlet weak var btnLogout: CustomButton!
    @IBOutlet weak var constantCollectionViewHeight: NSLayoutConstraint!
    
    //MARK: - SetupUI
    
    func setupUI(){
        
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.layer.masksToBounds = true
        
        lblName.textColor = UIColor.appThemeDarkBlueColor
        lblName.font = themeFont(size: 18, fontname: .bold)
        
        lblMyProduct.textColor = UIColor.appThemeDarkBlueColor
        lblMyProduct.font = themeFont(size: 18, fontname: .bold)
        lblMyProduct.text = "My_Product_key".localized
        
        btnUpdateProfile.setupThemeButtonUI()
        btnLogout.setupThemeButtonUI()
        
        btnUpdateProfile.setTitle("UPDATE_PROFILE_key".localized.capitalized, for: .normal)
        btnLogout.setTitle("LOGOUT_key".localized.capitalized, for: .normal)
        
        collectionProduct.register(UINib(nibName: "ProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionCell")
        if Defaults.object(forKey: remember_accesstoken_key) as? String == nil
        {
            btnLogout.isHidden = true
        }
        
    }
    
}
