//
//  ReviewView.swift
//  Yaanba
//
//  Created by Abhay on 18/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ReviewView: UIView {

    //MARK: - Outlet
    @IBOutlet weak var vwRatting: HCSStarRatingView!
    @IBOutlet weak var lblRattingDescription: UILabel!
    @IBOutlet weak var txtRattingDescription: UITextView!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    //MARK: - View life cycle
    
    func setupUI(){
        
        lblRattingDescription.textColor = UIColor.appThemeDarkBlueColor
        lblRattingDescription.font = themeFont(size: 14, fontname: .bold)
        lblRattingDescription.text = "Review_Description_key".localized
        
        txtRattingDescription.textColor = UIColor.black
        txtRattingDescription.font = themeFont(size: 16, fontname: .regular)
        
        btnSubmit.setupThemeButtonUI()
        btnSubmit.setTitle("SEND_key".localized, for: .normal)
    
    }
    
}
