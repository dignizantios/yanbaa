//
//  CallNowView.swift
//  Yaanba
//
//  Created by Abhay on 13/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SDWebImage

class CallNowView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCommentValue: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var btnCallNow: CustomButton!

    //MARK: - SetupUI
    func setupUI(thedelegate:CallNowVc){
        
        lblName.textColor = UIColor.appThemeDarkBlueColor
        lblName.font = themeFont(size: 14, fontname: .regular)
        
        lblDate.textColor = UIColor.appThemeGrayColor
        lblDate.font = themeFont(size: 9, fontname: .regular)
        
        lblCommentValue.textColor = UIColor.appThemeGrayColor
        lblCommentValue.font = themeFont(size: 13, fontname: .regular)
        
        lblMobileNumber.textColor = UIColor.black
        lblMobileNumber.font = themeFont(size: 12, fontname: .regular)

        btnCallNow.setupThemeButtonUI()
        btnCallNow.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        btnCallNow.setTitle("CALL_NOW_key".localized, for: .normal)
        
        btnCallNow.setImage(UIImage(named: "ic_telephone")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        
        if DeviceLanguage == "ar"{
            btnCallNow.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -50)
        }else{
            btnCallNow.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 50)
        }
        
        if thedelegate.mainModelView.bidData?.userData?.profileImage !=  nil {
            self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imgProfile.sd_setImage(with: thedelegate.mainModelView.bidData?.userData?.profileImage?.toURL(), placeholderImage: UIImage(named: "ic_review_splash_holder"), options: .lowPriority, context: nil)
        } else {
            self.imgProfile.image = UIImage(named: "ic_review_splash_holder")
        }
        lblName.text = thedelegate.mainModelView.bidData?.userData?.name ?? ""
        lblDate.text = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "dd MMM yyyy", strDate: thedelegate.mainModelView.bidData?.createdAt ?? "")
        
        btnCallNow.setTitle(thedelegate.mainModelView.bidData?.userData?.mobileNo ?? "", for: .normal)
        lblCommentValue.text =  ""
    }
    
}
