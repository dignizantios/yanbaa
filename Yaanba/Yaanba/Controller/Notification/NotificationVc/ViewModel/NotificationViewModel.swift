//
//  NotificationViewModel.swift
//  Yaanba
//
//  Created by Abhay on 03/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation

class NotificationViewModel{
    
    //MARK: - View life cycle
    fileprivate weak var theController:NotificationVc!
    
    init(theController:NotificationVc) {
        self.theController = theController
    }
    var notificationListdata = [Notificationdata]()
    var notificationdata: NotificationListdata?    
    var isNewDataLoading = false
    var refreshControl = UIRefreshControl()
}
//MARK: Api setup
extension NotificationViewModel {
    func notificationListApi(urls:String,param:[String:Any],success:@escaping(_ token : NotificationListModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = urls
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        showLoader()
        WebServices().MakeGetWithOutBaseUrlAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let notificationListData = NotificationListModel(JSON: data) {
                             success(notificationListData)
                             return
                         }
                     }
                     return
                     
                 }
                failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    func notificationCountApi(urls:String,param:[String:Any],success:@escaping(_ token : LoginModel) -> Void, failed:@escaping(String) -> Void) {
        
        let url = aNotificationCount
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_key".localized)
            return
        }
        showLoader()
        WebServices().MakeGetAPI(name: url, params: param) { (result) in
            
            hideLoader()
             switch result {
             case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                     if let data = value.dictionaryObject {
                         
                         if let notificationCountData = LoginModel(JSON: data) {
                             success(notificationCountData)
                             return
                         }
                     }
                     return
                     
                 }
                failed(value["msg"].stringValue)
                 break
             case .failure(let error):
                 if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                     // no internet connection
                    failed("No_internet_connection_key".localized)
                     
                 } else if let err = error as? URLError, err.code == URLError.timedOut {
                     failed("Request_time_out_Please_try_again".localized)
                 } else {
                     // other failures
                     failed("Something_went_wrong_key".localized)
                 }
                // Utility.shared.stopActivityIndicator()
                 break
             }
        }
    }
    
}
